(function($) {
  "use strict";

$("#brand").owlCarousel({
	  autoplay: true,
	  loop: true,
	  margin: 20,
	  responsiveClass: true,
	  autoHeight: true,
	  autoplayTimeout: 7000,
	  smartSpeed: 800,
	  nav: true,
	  dots: false,
	  responsive: {
	    0: {
	      items: 1
	    },

	    600: {
	      items: 3
	    },

	    1024: {
	      items: 4
	    },

	    1366: {
	      items: 4
	    }
	  }
	});
	$("#testimonials").owlCarousel({
	  autoplay: true,
	  loop: true,
	  responsiveClass: true,
	  autoHeight: true,
	  autoplayTimeout: 7000,
	  smartSpeed: 800,
	  nav: true,
	  dots: false,
	  responsive: {
	    0: {
	      items: 1
	    },

	    600: {
	      items: 1
	    },

	    1024: {
	      items: 1
	    },

	    1366: {
	      items: 1
	    }
	  }
	});
	$("#videos").owlCarousel({
	  autoplay: true,
	  loop: true,
	  responsiveClass: true,
	  autoHeight: true,
	  autoplayTimeout: 7000,
	  smartSpeed: 800,
	  nav: true,
	  dots: false,
	  responsive: {
	    0: {
	      items: 1
	    },

	    600: {
	      items: 1
	    },

	    1024: {
	      items: 1
	    },

	    1366: {
	      items: 1
	    }
	  }
	});

	$(document).ready(function() {

    setTimeout(function() {
      $(".fancybox").on("click", function() {
        $.fancybox({
          'titleShow': true,
          'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
          'type': 'swf',
          'swf': {
            'wmode': 'transparent',
            'allowfullscreen': 'true'
          }
          // href: this.href,
          // type: $(this).data("type")
        }); // fancybox
        return false
      });
    }, 1000);
  });



})(jQuery);
