<?php
/*
Template Name: Page: Find Member
*/
?>

<?php get_header(); ?>

<SCRIPT LANGUAGE="JavaScript">
<!-- Idea by:  Nic Wolfe (Nic@TimelapseProductions.com) -->
<!-- Web URL:  http://fineline.xs.mw -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=600,height=300');");
}
// End -->
</script>

	<?php get_template_part( 'framework/inc/titlebar' ); ?>

	<div id="page-wrap" class="container">
	
		<div id="content" class="sixteen columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry">
	
					<?php the_content(); ?>
                    
                    
<form action="http://vcra.org/directory/FindAPR3.asp" method="post">
  <p align="center"><strong><font color="#99cc00" size="6">Find an APRA Member</font></strong></p>
  <p align="center">To begin your 
  search, you will need to fill in one or more of the fields below.</b><br>
  The categories &quot;Member Type&quot;, &quot;Division Code&quot;, and &quot;Product Code&quot; all have 
  specific <br>
  abbreviations, which you can find by clicking on the categories to the left.
  <br>
  For example, if you are looking for a company that remanufactures alternators, 
  you would type <b>&quot;EL&quot;</b> <br>
  in the <b>&quot;Division Code&quot;</b> field for Electrical, and <b>&quot;AL&quot;</b> in the <b>
  &quot;Product Code&quot;</b> field for Alternators. Then hit &quot;Submit Query.&quot;<br>
  Once you have your list, you can then click on the Company ID# to get more 
  in-depth information <br>
  about a particular company. The search is not case sensitive, but it is punctuation sensitive.<br>
  For best results, on company name searches, use just the beginning of you 
  company name without any punctuation.</p>
  <div align="center">
  <table border="1" style="border-collapse: collapse" bordercolor="#111111" cellpadding="5" cellspacing="0">
    <tr>
      <td width="43%" height="23" align="right">
        <br />
        <a href="javascript:popUp('/popup/apra_member_codes.htm')">
        Member
      Type</a>&nbsp;&nbsp;
      </td>
      
      <td width="61%" height="23" align="left">
        <br /><input type="text" size="10" name="MTYPE"> 
 </td>
    </tr>
   
    <tr>
      <td width="43%" height="23" align="right">
        <br />
        <a href="javascript:popUp('/popup/CompanyHelp.htm')">
        Company
      Name</a>&nbsp;&nbsp;
      </td>
      <td width="61%" height="23" align="left">
       <br /><input type="text" size="20" name="COMPANY">
      </td>
    </tr>
    <tr>
      <td align="right">
        <br />
        <a href="javascript:popUp('/popup/CompanyHelp.htm')">
        Primary Contact</a>&nbsp;&nbsp;
      </td>
      <td align="left">
        <br /><input type="text" size="20" name="CONTACT">
      </td>
    </tr>
    <tr>
      <td align="right">
        <br />
        <a href="javascript:popUp('/popup/CompanyHelp.htm')">
        City</a>&nbsp;&nbsp;
      </td>
      <td align="left">
        <br /><input type="text" size="20" name="CITY">
      </td>
    </tr>
    <tr>
      <td width="43%" height="23" align="right">
        <br />
        <a href="javascript:popUp('/popup/apra_state_codes.htm')">
        State</font></a>&nbsp;&nbsp;
      </td>
      <td width="61%" height="23" align="left">
        <br /><input type="text" size="2" maxlength="2" name="STATE">
 </td>
    </tr>
    <tr>
      <td width="43%" height="23" align="right">
        <br />
        <a href="javascript:popUp('/popup/apra_country_codes.htm')">
        Country</a>&nbsp;&nbsp;
      </td>
      <td width="61%" height="23" align="left">
        <br /><input type="text" size="20" name="COUNTRY">
 </td>
    </tr>
    <tr>
      <td width="43%" height="28" align="right">
        <br />
        <a href="javascript:popUp('/popup/apra_division_codes.htm')">
        Division Code</a>&nbsp;&nbsp;
      </td>
      <td width="61%" height="28" align="left">
        <br /><input type="text" size="2" maxlength="2" name="DIVISION">
 </td>
    </tr>
    <tr>
      <td width="43%" height="28" align="right">
        <br />
        <a href="javascript:popUp('/popup/apra_product_codes.htm')">
        Product Code</a>&nbsp;&nbsp;
        
 </td>
      <td width="61%" height="28" align="left"><br /><input type="text" size="2" maxlength="2" name="PRODUCT"> </td>
    </tr>
    <tr>
      <td width="43%" height="23" align="right"><br /><a href="javascript:popUp('/popup/CompanyHelp.htm')">Keyword from notes</a>&nbsp;&nbsp;</td>
      <td width="61%" height="23" align="left"><br /><input type="text" size="20" name="KEYWORD"></td>
    </tr>
  </table>
  </div>
  <p align="center"><input type="reset" name="Clear1" value="Reset">&nbsp;&nbsp;<input type="submit" value="Search"></p>
</form>                    
                    
                    
	
					<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
	
				</div>
	
			</article>
			
			<?php if(!$data['check_disablecomments']) { ?>
				<?php comments_template(); ?>
			<?php } ?>
	
			<?php endwhile; endif; ?>
		</div> <!-- end content -->
	
	</div> <!-- end page-wrap -->
	
<?php get_footer(); ?>
