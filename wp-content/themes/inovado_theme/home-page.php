<?php
/*
Template Name: Page: Home Page
*/
?>

<?php get_header(); ?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/css/home/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/fonts/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/home/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/css/home/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/css/home/style.css">

	<?php get_template_part( 'framework/inc/titlebar' ); ?>

	<div id="page-wrap" class="container">
	
		<div id="content" class="sixteen columns">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry">
	
					<?php the_content(); ?>
					<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
	
				</div>
	
			</article>
			
			<?php if(!$data['check_disablecomments']) { ?>
				<?php comments_template(); ?>
			<?php } ?>
	
			<?php endwhile; endif; ?>
		</div> <!-- end content -->
	
	</div> <!-- end page-wrap -->
	
<?php get_footer(); ?>


  <!-- <script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/js/home/jquery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/js/home/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/js/home/jquery.fancybox.pack.js"></script>
  <script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/js/home/jquery.fancybox-media.js"></script>
  <script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/inovado_theme/framework/js/home/script.js"></script>