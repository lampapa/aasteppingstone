=== Do Shortcodes for All in One SEO Pack ===
Contributors: denra, itinchev
Donate link: https://www.paypal.me/itinchev
Tags: all in one, seo, pack, aioseop, shortcode, shortcodes, display, execute, do, plugins, code, source, function, title, description, keywords, facebook, twitter, open graph
Requires at least: 4.0
Tested up to: 5.6
Stable tag: 2.3.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display shortcodes in title, description, keywords, Facebook and Twitter fields, and other locations for All in One SEO Pack.

== IMPORTANT!!! IMPORTANT!!! IMPORTANT!!! ===

This plugin works *ONLY* with All in One SEO Pack versions before 4.0. It does *NOT* work with version 4.0 and above since the code of version 4.* was fully re-worked. We are *NOT* planning to release a new version of this plugin and it will be removed from wordpress.org soon.

== Description ==

The All in One SEO Pack plugin does not provide the feature to do/display/show/execute WordPress shortcodes which are entered in the fields for title, description, keywords, Open Graph and others for the posts, pages, and other custom post types. This plugin converts all those shortcodes in the mentioned fields to their corresponding values as expected.

Please check the plugin settings page `Do Shortcodes for AISEOP` menu under the `Denra Plugins` admin menu.

*It takes lots of efforts to develop and support a plugin for free. Please send us your feedback and questions to fix your issue before leaving a bad review.*

Please [contact us](mailto:support@denra.com) by e-mail if you need more information or support.

== Features ==

* Do shortcodes in the post SEO Title, Description and Keywords.
* Do shortcodes in the Open Graph (Facebook and Twitter) fields.
* Do shortcodes in the Schema.
* Do shortcodes in the post/page titles.
* Do shortcodes in the navigation menus.
* Do shortcodes in the widgets.
* Preview shortcodes execution in real-time on a separate pane while editing their values.

== Installation ==

= From WordPress Dashboard =

1. Navigate to `Plugins` -> `Add New` from your WordPress dashboard.
2. Search for `Do Shortcodes for All in One SEO Pack` and install it.
3. Activate the plugin from the Plugins menu.

= Manual Installation =

1. Download the plugin file: `do-shortcodes-for-all-in-one-seo-pack.zip`
2. Unzip the file
3. Upload the `do-shortcodes-for-all-in-one-seo-pack` folder to your `/wp-content/plugins` directory (do not rename the folder).
4. Activate the plugin from the Plugins menu.

It will start doing it's job. That's all, folks!

== Screenshots ==
1. Sample title, description, and keywords with shortcodes in All in One SEO Pack post/page fields.
2. Plugin settings page under the 'Settings' menu.

== Changelog ==

= 2.3.1 =
* Fixed: Restored Settings page design and missing button to Save settings.
* Compatibility with WordPress version 5.5.3.

= 2.3 =
* Added Schema support.
* Improved framework.

= 2.2 =
* Added a separate preview pane with the shortcodes execution results for the SEO fields.
* Settings bug fix.

= 2.1.1 =
* Settings resetting bug fix. Please check that your settings have not been changed by the previous update.

= 2.1 =
* Added Page/Post Content support for shortcodes (if turned off for some reason).
* Added possibility to change filters' priority for cases when another plugin has turned the shortcodes off.
* Framework update and bug fixes.

= 2.0 =
* Added Denra Plugins Framework 1.0.
* Added Widget Title and Content support for shortcodes.

= 1.2.1 =
* Plugin settings page moved under the general 'Settings' page.
* Added Settings, E-mail support, Plugin's website and Donate! links on the Plugins page.

= 1.2 =
* Added post/page title and navigation menu support for shortcodes.
* Added new Do Shortcodes settings page under the All in One SEO menu.

= 1.1 =
* Added Open Graph title and description support (for Facebook and Twitter).
* Added fix for shortcodes with attributes.

= 1.0 =
* Initial release
