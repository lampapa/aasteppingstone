<?php

/**
 * DoShortcodesAISEOPFactory Class
 *
 * The factory class that works with the DoShortcodesAISEOP class and objects
 *
 * @author     Denra.com aka SoftShop Ltd <support@denra.com>
 * @copyright  2019 Denra.com aka SoftShop Ltd
 * @license    GPLv2 or later
 * @version    1.0.1
 * @link       https://www.denra.com/
 */

namespace Denra\Plugins;

class DoShortcodesAIOSEOPHooks extends PluginHooks {
    
    public static $plugin_id;
    public static $plugin_class;
    
    public static function activate() {
        
        parent::activate();
        
        // Change old settings used in version 1.2
        // until all old versions are upgraded to version 2.0 or newer
        $settings_id_u = str_replace('-', '_', static::$plugin_id) . '_settings';
        $settings = get_option($settings_id_u);
        if ($settings && is_array($settings)) {
            foreach ($settings['do_shortcodes'] as $key => $value) {
                $settings['do_shortcodes'][$key] = intval($value);
            }
            update_option($settings_id_u, $settings, FALSE);
        }
        
    }
    
    public static function deactivate() {
        
        parent::deactivate();
        
    }
    
    public static function uninstall() {
        
        parent::uninstall();
        
    }
    
}
