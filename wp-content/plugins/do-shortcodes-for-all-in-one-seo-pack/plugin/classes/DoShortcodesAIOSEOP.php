<?php

/**
 * DoShortcodesAISEOP Class
 *
 * The main class for the plugin Do Shortcodes for All in Once SEO Pack
 *
 * @author     Denra.com aka SoftShop Ltd <support@denra.com>
 * @copyright  2019 Denra.com aka SoftShop Ltd
 * @license    GPLv2 or later
 * @version    1.3
 * @link       https://www.denra.com/
 */

namespace Denra\Plugins;

class DoShortcodesAIOSEOP extends Plugin {
    
    public function __construct($id, $data = []) {
        
        // Set text_domain for the framework
        $this->text_domain = 'denra-do-sc-aioseop';
        
        // Set admin menus texts
        $this->admin_title_menu = __('Do Shortcodes for AIOSEOP', 'denra-do-sc-aioseop');
        
        $this->settings_default['do_shortcodes'] = [
            'aioseop_title' => 1,
            'aioseop_description' => 1,
            'aioseop_schema' => 1,
            'aioseop_keywords' => 1,
            'aioseop_open_graph_title' => 1,
            'aioseop_open_graph_desc' => 1,
            'wp_title' => 0,
            'the_content' => 0,
            'nav_menu' => 0,
            'widget' => 0,
        ];
        $this->settings_default['filters_priority'] = 10;
        $this->settings_default['use_preview_pane'] = 1;
        
        parent::__construct($id, $data);
        
        if (defined('AIOSEOP_VERSION')) { // Checking for active AIOSEOP installation
            $this->addFilters();
            if (is_admin() && $this->settings['use_preview_pane']) {
                add_action('add_meta_boxes', [&$this, 'addMetaBoxes']);
                add_action('wp_ajax_previewDoShortcodes', [&$this, 'previewDoShortcodes'], 1, 1);
            }
        }

    }
    
    public function addFilters() {
        
        if (isset($this->settings['do_shortcodes']['aioseop_title'])) {
            add_filter('aioseop_title', 'do_shortcode', $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['aioseop_description']) {
            add_filter('aioseop_description', 'do_shortcode', $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['aioseop_keywords']) {
            add_filter('aioseop_keywords', [&$this, 'doShortcodesKeywords'], $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['aioseop_schema']) {
            add_filter('aioseop_register_schema_objects', [&$this, 'doSchemaObjects'], $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['aioseop_open_graph_title']) {
            add_filter('aiosp_opengraph_meta', [&$this, 'doShortcodesOpenGraph'], $this->settings['filters_priority'], 5);
        }
        
        if ($this->settings['do_shortcodes']['aioseop_open_graph_desc']) {
            add_filter('aiosp_opengraph_meta', [&$this, 'doShortcodesOpenGraph'], $this->settings['filters_priority'], 5);
        }
        
        if ($this->settings['do_shortcodes']['wp_title']) {
            add_filter('wp_title', 'do_shortcode', $this->settings['filters_priority']);
            add_filter('the_title', 'do_shortcode', $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['the_content']) {
            add_filter('the_content', 'do_shortcode', $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['nav_menu']) {
            add_filter('walker_nav_menu_start_el', 'do_shortcode', $this->settings['filters_priority']);
        }
        
        if ($this->settings['do_shortcodes']['widget']) {
            add_filter('widget_title', [&$this, 'doShortcodes'], $this->settings['filters_priority'], 3);
            add_filter('widget_text', 'do_shortcode', $this->settings['filters_priority'], 3);
            add_filter('widget_custom_html_content', 'do_shortcode', $this->settings['filters_priority'], 3);
        }
        
    }
    
    public function enqueueAdminCSSandJS() {
        
        parent::enqueueAdminCSSandJS();
        wp_enqueue_script('jquery');
        //wp_enqueue_style('bootstrap');
        
    }
    
    public function addMetaBoxes() {
        
        if (!function_exists('get_current_screen')) {
            require_once ABSPATH . 'wp-admin/includes/screen.php';
        }   
        if (isset($GLOBALS['post']) && get_current_screen() && $GLOBALS['post']->ID) {
            wp_enqueue_script('jquery');
            add_meta_box('do-shortcodes-preview-postbox', __('Preview of Do Shortcodes for All in One SEO Pack Results', 'denra-do-sc-aioseop'), [&$this, 'previewDoShortcodesInit']);
            add_action('admin_head', [&$this, 'scriptDoShortcodesPreview']);
        }
        
    }
    
    public function previewDoShortcodesInit () {
        
        global $aioseop_options;
        
        // SEO Title, SEO Decription and SEO Keywords Preview
        echo '<div class="do-shortcodes-preview">';
        echo '<div class="col">';
        echo '<div id="do-shortcodes-preview-seo-title"><strong>' . __('SEO Title', 'denra-do-sc-aioseop') . ':</strong><br><span></span></div>';
        echo '<div id="do-shortcodes-preview-seo-description"><strong>' . __('SEO Description', 'denra-do-sc-aioseop') . ':</strong><br><span></span></div>';
        if (!$aioseop_options['aiosp_togglekeywords']) {
            echo '<div id="do-shortcodes-preview-seo-keywords"><strong>' . __('SEO Keywords', 'denra-do-sc-aioseop') . ':</strong><br><span></span></div>';
        }
        echo '</div>';
        echo '<div class="col">';
        echo '<div id="do-shortcodes-preview-opengraph-title"><strong>' . __('Open Graph Title', 'denra-do-sc-aioseop') . ':</strong><br><span></span></div>';
        echo '<div id="do-shortcodes-preview-opengraph-description"><strong>' . __('Open Graph Description', 'denra-do-sc-aioseop') . ':</strong><br><span></span></div>';
        echo '</div>';
        echo '</div>';
        
    }
    
    public function scriptDoShortcodesPreview() {
        
        $seo_post_id = filter_input(INPUT_POST, 'do_sc_post_id', FILTER_SANITIZE_NUMBER_INT);
        if (!$seo_post_id && isset($GLOBALS['post']) && $GLOBALS['post']->ID) {
            $seo_post_id = $GLOBALS['post']->ID;
        }
        if ($seo_post_id) {
    ?>
<script>
jQuery(document).ready(function ($) {
    "use strict";
    var do_sc_aioseop = {
        seo_title: {
            field: $('input[name="aiosp_title"]'),
            preview: $('#do-shortcodes-preview-seo-title span'),
            update: 1
        },
        seo_description: {
            field: $('textarea[name="aiosp_description"]'),
            preview: $('#do-shortcodes-preview-seo-description span'),
            update: 1
        },
        seo_keywords: {
            field: $('input[name="aiosp_keywords"]'),
            preview: $('#do-shortcodes-preview-seo-keywords span'),
            update: 1
        },
        opengraph_title: {
            field: $('input[name="aioseop_opengraph_settings_title"]'),
            preview: $('#do-shortcodes-preview-opengraph-title span'),
            update: 1
        },
        opengraph_description: {
            field: $('textarea[name="aioseop_opengraph_settings_desc"]'),
            preview: $('#do-shortcodes-preview-opengraph-description span'),
            update: 1
        }
    };
    var do_sc_update_interval = 2000;
    var do_sc_change = 1;
    var do_sc_ajaxurl = "<?php echo admin_url('admin-ajax.php') . '?page=' . $this->id; ?>";
    function do_sc_preview (do_sc_item_name) {
        $.post(do_sc_ajaxurl, {
            action: 'previewDoShortcodes',
            do_sc_item_name: do_sc_item_name,
            do_sc_item_value: do_sc_aioseop[do_sc_item_name]['field'].val()
        }, function (data) {
            do_sc_aioseop[do_sc_item_name]['preview'].html(data);
        });
        do_sc_aioseop[do_sc_item_name]['update'] = 0;
    }
    function do_sc_update() {
        for (var key in do_sc_aioseop) {
            var do_sc_item = do_sc_aioseop[key];
            if (do_sc_item['update'] === 1) {
                do_sc_preview(key);
            }
        }
        do_sc_change = 0;
    }
    do_sc_update();
    do_sc_aioseop['seo_title']['field'].on("keyup", function(){do_sc_aioseop['seo_title']['update']=1;do_sc_change=1;});
    do_sc_aioseop['seo_description']['field'].on("keyup", function(){do_sc_aioseop['seo_description']['update']=1;do_sc_change=1;});
    do_sc_aioseop['seo_keywords']['field'].on("keyup", function(){do_sc_aioseop['seo_keywords']['update']=1;do_sc_change=1;});
    do_sc_aioseop['opengraph_title']['field'].on("keyup", function(){do_sc_aioseop['opengraph_title']['update']=1;do_sc_change=1;});
    do_sc_aioseop['opengraph_description']['field'].on("keyup", function(){do_sc_aioseop['opengraph_description']['update']=1;do_sc_change=1;});
    setInterval(function(){if(do_sc_change===1){do_sc_update();}}, do_sc_update_interval);
});
</script>
    <?php
        }
        
    }
    
    public function previewDoShortcodes() {
        
        global $aioseop_options;
        
        $do_sc_item_name = filter_input(INPUT_POST, 'do_sc_item_name', FILTER_SANITIZE_STRING);
        $do_sc_item_value = filter_input(INPUT_POST, 'do_sc_item_value', FILTER_SANITIZE_STRING);
        $do_sc_return = '';
        
        switch ($do_sc_item_name) {
            case 'seo_title':
            case 'seo_description':
            case 'opengraph_title':
            case 'opengraph_description':    
                $do_sc_return = $this->doShortcodes($do_sc_item_value);
                break;
            
            case 'seo_keywords':
                $aiosp_togglekeywords = $aioseop_options['aiosp_togglekeywords'];
                if (!$aiosp_togglekeywords) {
                    $do_sc_return = $this->doShortcodesKeywords($do_sc_item_value);
                }
                break;
        }
        echo $do_sc_return . ' (' . mb_strlen($do_sc_return) . ')';
        exit;
        
    }
    
    public function doShortcodes($content) {
        
        $is_url = filter_var( $content, FILTER_VALIDATE_URL );
        if ( ! $is_url ) {
            $content = html_entity_decode($content);
        }
        $content = do_shortcode($content);
        if ( ! $is_url ) {
            $content = htmlentities($content);
        }
        return $content;
        
    }
    
    public function doSchemaObjects ( $graphs ) {
        foreach($graphs as $graph) {
            add_filter('aioseop_schema_class_data_'.get_class($graph), [&$this, 'doShortcodesSchema'], $this->settings['filters_priority']);
        }
        
    }
    
    public function doShortcodesSchema($entity) {
        if (is_array($entity)) {
            foreach($entity as $key => $value) {
                if (is_array($value)) {
                    $entity[$key] = $this->doShortcodesSchema($value);
                }
                else {
                    $entity[$key] = $this->doShortcodes($value);
                }
            }
            return $entity;
        }
        
    }
    
    public function doShortcodesKeywords($content) {
        
        $content = do_shortcode(html_entity_decode($content));
        
        return htmlentities(implode(',', array_unique(explode(',', $content))));
        
    }
    
    public function doShortcodesOpenGraph($value, $type, $field, $v, $extra_params) {
        
        unset($type, $field, $extra_params); // we do not need these
        $opengraph = get_post_meta(get_the_ID(), '_aioseop_opengraph_settings');
        if ($this->settings['do_shortcodes']['aioseop_open_graph_title'] &&
            in_array($v, ['og:title', 'twitter:title']) &&
                isset($opengraph[0]['aioseop_opengraph_settings_title'])) {
            $value = $this->doShortcodes($opengraph[0]['aioseop_opengraph_settings_title']);
        }
        elseif ($this->settings['do_shortcodes']['aioseop_open_graph_desc'] &&
            in_array($v, ['og:description', 'twitter:description']) && 
                isset($opengraph[0]['aioseop_opengraph_settings_desc'])) {
            $value = $this->doShortcodes($opengraph[0]['aioseop_opengraph_settings_desc']);
        }
        return $value;
        
    }
    
    public function adminSettingsContent() {
        
        echo '<fieldset>';
        echo '<legend>' . __('Do shortcodes for SEO title, description, keywords, and schema', 'denra-do-sc-aioseop') . '</legend>';
        echo '<label for="aioseop_title"><input id="aioseop_title" name="aioseop_title" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_title'] ? ' checked' : '') . ' /> ' . __('SEO Title', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="aioseop_description"><input id="aioseop_description" name="aioseop_description" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_description'] ? ' checked' : '') . ' /> ' . __('SEO Description', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="aioseop_keywords"><input id="aioseop_keywords" name="aioseop_keywords" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_keywords'] ? ' checked' : '') . ' /> ' . __('SEO Keywords', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="aioseop_schema"><input id="aioseop_schema" name="aioseop_schema" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_schema'] ? ' checked' : '') . ' /> ' . __('In Schema', 'denra-do-sc-aioseop') . '</label>';
        echo '</fieldset>';
        
        echo '<fieldset>';
        echo '<legend>'  . __('Do shortcodes for Open Graph (Facebook/Twitter) fields', 'denra-do-sc-aioseop') . '</legend>';
        echo '<label for="aioseop_open_graph_title"><input id="aioseop_open_graph_title" name="aioseop_open_graph_title" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_open_graph_title'] ? ' checked' : '') . ' /> ' . __('Open Graph Title', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="aioseop_open_graph_desc"><input id="aioseop_open_graph_desc" name="aioseop_open_graph_desc" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['aioseop_open_graph_desc'] ? ' checked' : '') . ' /> ' . __('Open Graph Description', 'denra-do-sc-aioseop') . '</label>';
        echo '</fieldset>';
        
        echo '<fieldset>';
        echo '<legend>'  . __('Do shortcodes in other locations', 'denra-do-sc-aioseop') . '<sup>1<sup></legend>';
        echo '<label for="wp_title"><input id="wp_title" name="wp_title" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['wp_title'] ? ' checked' : '') . ' /> ' . __('Post/Page Title', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="the_content"><input id="the_content" name="the_content" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['the_content'] ? ' checked' : '') . ' /> ' . __('Post/Page Content', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="nav_menu"><input id="nav_menu" name="nav_menu" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['nav_menu'] ? ' checked' : '') . ' /> ' . __('Navigation Menu', 'denra-do-sc-aioseop') . '</label>';
        echo '<label for="widget"><input id="widget" name="widget" type="checkbox" value="1"' . ($this->settings['do_shortcodes']['widget'] ? ' checked' : '') . ' /> ' . __('Widget Title and Content', 'denra-do-sc-aioseop') . '</label>';
        echo '</fieldset>';
        
        echo '<label for="filters_priority">' . __('Change filters\' priority:', 'denra-do-sc-aioseop') . '<sup>2</sup> <input id="filters_priority" name="filters_priority" type="number" min="1" max="999999999" size="12" maxlength="9" value="' . $this->settings['filters_priority']. '" /> (1 - 999999999)</label>';
        
        echo '<p><sup>1</sup> '. __('In case they do not work already.', 'denra-do-sc-aioseop');
        echo '<br><sup>2</sup> '. __('In case the filters do not work. Higher is better.', 'denra-do-sc-aioseop') . '</p>';
        
        echo '<label for="use_preview_pane"><input id="use_preview_pane" name="use_preview_pane" type="checkbox" value="1"' . ($this->settings['use_preview_pane'] ? ' checked' : '') . ' /> ' . __('Use preview pane of Do Shortcodes for All in One SEO Pack Results', 'denra-do-sc-aioseop') . '</label>';
        
        parent::adminSettingsContent();

    }
    
    public function adminSettingsProcessing() {
        
        parent::adminSettingsProcessing();
        
        foreach (array_keys($this->settings_default['do_shortcodes']) as $key) {
            $this->settings['do_shortcodes'][$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_NUMBER_INT);
        }
        $this->settings['filters_priority'] = abs(filter_input(INPUT_POST, 'filters_priority', FILTER_SANITIZE_NUMBER_INT));
        if ($this->settings['filters_priority'] < 1) {
            $this->settings['filters_priority'] = 10;
        }
        if ($this->settings['filters_priority'] > 999999999) {
            $this->settings['filters_priority'] = 999999999;
        }
        $this->settings['use_preview_pane'] = filter_input(INPUT_POST, 'use_preview_pane', FILTER_SANITIZE_NUMBER_INT);

    }
}
