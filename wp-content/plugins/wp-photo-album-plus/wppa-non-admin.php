<?php
/* wppa-non-admin.php
* Package: wp-photo-album-plus
*
* Contains all the non admin stuff
* Version 7.6.04.003
*
*/

if ( ! defined( 'ABSPATH' ) ) die( "Can't load this file directly" );

/* API FILTER and FUNCTIONS */
require_once 'wppa-filter.php';
require_once 'wppa-breadcrumb.php';
require_once 'wppa-album-covers.php';
require_once 'wppa-cart.php';
if ( ! is_admin() ) {
	require_once 'wppa-tinymce-photo-front.php';
}

/* LOAD STYLESHEET */
add_action('wp_enqueue_scripts', 'wppa_add_style');

function wppa_add_style() {
global $wppa_api_version;

	// Are we allowed to look in theme?
	if ( get_option( 'wppa_use_custom_style_file', 'no' ) == 'yes' ) {

		// In child theme?
		$userstyle = get_theme_root() . '/' . wppa_get_option('stylesheet') . '/wppa-style.css';
		if ( is_file($userstyle) ) {
			wp_register_style('wppa_style', get_theme_root_uri() . '/' . wppa_get_option('stylesheet')  . '/wppa-style.css', array(), $wppa_api_version);
			wp_enqueue_style('wppa_style');
			return;
		}

		// In theme?
		$userstyle = get_theme_root() . '/' . wppa_get_option('template') . '/wppa-style.css';
		if ( is_file($userstyle) ) {
			wp_register_style('wppa_style', get_theme_root_uri() . '/' . wppa_get_option('template')  . '/wppa-style.css', array(), $wppa_api_version);
			wp_enqueue_style('wppa_style');
			return;
		}
	}

	// Use standard
	$style_file = dirname( __FILE__ ) . '/theme/wppa-style.css';
	if ( wppa_is_file( $style_file ) ) {
		$ver = date( "ymd-Gis", filemtime( $style_file ) );
	}
	else {
		$ver = $wppa_api_version;
	}
	wp_register_style('wppa_style', WPPA_URL.'/theme/wppa-style.css', array(), $ver);
	wp_enqueue_style('wppa_style');

	// Dynamic css
	if ( get_option( 'wppa_inline_css', 'yes' ) == 'no' ) {
		if ( ! file_exists( WPPA_UPLOAD_PATH.'/dynamic/wppa-dynamic.css' ) ) {
			wppa_create_wppa_dynamic_css();
			update_option( 'wppa_dynamic_css_version', wppa_get_option( 'wppa_dynamic_css_version', '0' ) + '1' );
		}
		if ( file_exists( WPPA_UPLOAD_PATH.'/dynamic/wppa-dynamic.css' ) ) {
			wp_enqueue_style( 'wppa-dynamic', WPPA_UPLOAD_URL.'/dynamic/wppa-dynamic.css', array('wppa_style'), wppa_get_option( 'wppa_dynamic_css_version' ) );
		}
	}
}

/* SEO META TAGS AND SM SHARE DATA */
add_action('wp_head', 'wppa_add_metatags', 5);

function wppa_add_metatags() {
global $wpdb;

	// Share info for sm that uses og
	$id = wppa_get_get( 'photo' );
	if ( ! wppa_photo_exists( $id ) ) {
		$id = false;
	}
	if ( $id ) {

		// SM may not accept images from the cloud.
		wppa( 'for_sm', true );

		// SM does not want version numbers
		wppa( 'no_ver', true );

		$imgurl = wppa_get_photo_url( $id );
		wppa( 'no_ver', false );
		wppa( 'for_sm', false );
	}
	else {
		$imgurl = '';
//		echo '<!-- WPPA+ No Photo id -->';
	}

	if ( $id ) {

		if ( wppa_switch( 'share_twitter' ) ) {
			$thumb = wppa_cache_thumb( $id );

			// Twitter wants at least 280px in width, and at least 150px in height
			if ( $thumb ) {
				$x = wppa_get_photo_item( $id, 'photox' );
				$y = wppa_get_photo_item( $id, 'photoy' );
			}
			if ( $thumb && $x >= 280 && $y >= 150 ) {
				$card = 'summary_large_image';
			}
			else {
				$card = 'summary';
			}
			$title  = wppa_get_photo_name( $id );
			$desc 	= wppa_get_og_desc( $id, 'short' );
			$url 	= ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$site   = get_bloginfo( 'name' );
			$creat 	= wppa_opt( 'twitter_account' );

				echo '
<!-- WPPA+ Twitter Share data -->
<meta name="twitter:card" content="' . $card . '">
<meta name="twitter:site" content="' . esc_attr( $site ) . '">
<meta name="twitter:title" content="' . esc_attr( sanitize_text_field( $title ) ) . '">
<meta name="twitter:text:description" content="' . esc_attr( sanitize_text_field( $desc ) ) . '">
<meta name="twitter:image" content="' . esc_url( $imgurl ) . '">';
if ( $creat ) {
	echo '
<meta name="twitter:creator" content="' . $creat . '">';
}
echo '
<!-- WPPA+ End Twitter Share data -->
';
		}

		if ( wppa_switch( 'og_tags_on' ) ) {
			$thumb = wppa_cache_thumb( $id );
			if ( $thumb ) {
				$title  = wppa_get_photo_name( $id );
				$desc 	= wppa_get_og_desc( $id );
				$url 	= ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				$url 	= wppa_convert_to_pretty( $url, false, true );
				$site   = get_bloginfo('name');
				$mime 	= wppa_get_mime_type( $id );
				echo '
<!-- WPPA+ Og Share data -->
<meta property="og:site_name" content="' . esc_attr( sanitize_text_field( $site ) ) . '" />
<meta property="og:type" content="article" />
<meta property="og:url" content="' . $url . '" />
<meta property="og:title" content="' . esc_attr( sanitize_text_field( $title ) ) . '" />';
if ( $mime ) {
	echo '
<meta property="og:image" content="' . esc_url( sanitize_text_field( $imgurl ) ) . '" />
<meta property="og:image:type" content="' . $mime . '" />
<meta property="og:image:width" content="' . wppa_get_photox( $id ) . '" />
<meta property="og:image:height" content="' . wppa_get_photoy( $id ) . '" />';
}
if ( $desc ) {
	echo '
<meta property="og:description" content="' . esc_attr( sanitize_text_field( $desc ) ) . '" />';
}
echo '
<!-- WPPA+ End Og Share data -->
';
			}
		}

	}

	// To make sure we are on a page that contains at least [wppa] we check for Get var 'wppa-album'.
	// This also narrows the selection of featured photos to those that exist in the current album.
	$done = array();
	$album = wppa_get_get( 'album' );

	if ( $album ) {
		if ( wppa_switch( 'meta_page' ) ) {
			$photos = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->wppa_photos WHERE album = %s AND status = 'featured'", $album ), ARRAY_A );
			wppa_cache_photo( 'add', $photos );
			if ( $photos ) {
				echo("\n<!-- WPPA+ BEGIN Featured photos on this page -->");
				foreach ( $photos as $photo ) {
					$id 		= $photo['id'];
					$content 	= esc_attr( sanitize_text_field( wppa_get_keywords( $id ) ) );
					if ( $content && ! in_array( $content, $done ) ) {
						echo'
<meta name="keywords" content="'.$content.'" >';
						$done[] = $content;
					}
				}
				echo("\n<!-- WPPA+ END Featured photos on this page -->\n");
			}
		}
	}

	// No photo and no album, give the plain photo links of all featured photos
	elseif ( wppa_switch( 'meta_all' ) ) {
		$photos = $wpdb->get_results( "SELECT * FROM $wpdb->wppa_photos WHERE status = 'featured'", ARRAY_A);
		wppa_cache_photo( 'add', $photos );
		if ( $photos ) {
			echo("\n<!-- WPPA+ BEGIN Featured photos on this site -->");
			foreach ( $photos as $photo ) {
				$thumb 		= $photo;	// Set to global to reduce queries when getting the name
				$id 		= $photo['id'];
				$content 	= esc_attr( sanitize_text_field( wppa_get_keywords( $id ) ) );
				if ( $content && ! in_array( $content, $done ) ) {
					echo '
<meta name="keywords" content="'.$content.'" >';
					$done[] = $content;
				}
			}
			echo("\n<!-- WPPA+ END Featured photos on this site -->\n");
		}
	}

	// Facebook Admin and App
	if ( ( wppa_switch( 'share_on' ) ||  wppa_switch( 'share_on_widget' ) ) &&
		( wppa_switch( 'facebook_comments' ) || wppa_switch( 'facebook_like' ) || wppa_switch( 'share_facebook' ) ) ) {
		echo("\n<!-- WPPA+ BEGIN Facebook meta tags -->");
		if ( wppa_opt( 'facebook_admin_id' ) ) {
			echo ("\n\t<meta property=\"fb:admins\" content=\"".wppa_opt( 'facebook_admin_id' )."\" />");
		}
		if ( wppa_opt( 'facebook_app_id' ) ) {
			echo ("\n\t<meta property=\"fb:app_id\" content=\"".wppa_opt( 'facebook_app_id' )."\" />");
		}
		if ( $imgurl ) {
			echo '
<link rel="image_src" href="'.esc_url( $imgurl ).'" />';
		}
		echo '
<!-- WPPA+ END Facebook meta tags -->
';
	}
}

/* LOAD JAVASCRIPT */
add_action( 'init', 'wppa_add_javascript' );
function wppa_add_javascript() {

	$in_footer = wppa_js_in_footer();

	// If in footer...
	if ( $in_footer ) {
		add_action( 'wp_footer', 'wppa_add_javascripts' );
	}
	else {
		add_action( 'wp_enqueue_scripts', 'wppa_add_javascripts' );
	}
}

// Decide if we want js in the footer
function wppa_js_in_footer() {

	$result = wppa_switch( 'defer_javascript' ) && ! wppa( 'ajax' ) && ! is_admin() && ! wppa( 'cron' );

	return $result;
}

// This function does the actual js enqueueing
function wppa_add_javascripts() {
global $wppa_api_version;
global $wppa_lang;
global $wppa_opt;

	$footer = wppa_js_in_footer();

	// If in footer and no wppa on the page found? Quit
	if ( $footer && ! wppa( 'mocc' ) && ! wppa_switch( 'load_nicescroller' ) && ! wppa_opt( 'fs_policy' ) == 'global' ) {
		return;
	}

	// WPPA+ Javascript files.
	// All wppa+ js files come in 2 flavours: the normal version and a minified version.
	// If the minified version is available, it will be loaded, else the normal version.
	// If you want to debug js, just delete the minified version; this will cause the normal
	// - readable - version to be loaded.

	// The js dependancies
	$js_depts = array(	'jquery',
						'jquery-form',
						'jquery-masonry',
						'jquery-ui-dialog',
					);

	// First see if an 'all' file is present. This is to save http requests
	$all_file = dirname( __FILE__ ) . '/js/wppa-all.js';
	if ( wppa_is_file( $all_file ) ) {
		$js_ver = date( "ymd-Gis", filemtime( $all_file ) );
		wp_enqueue_script( 'wppa', WPPA_URL . '/js/wppa-all.js', $js_depts, $js_ver, $footer );
	}

	// No all file, do them one by one
	else {
		$js_files = array(
							'wppa-utils',
							'wppa',
							'wppa-slideshow',
							'wppa-ajax-front',
							'wppa-lightbox',
							'wppa-popup',
							'wppa-touch',
							'wppa-zoom',
							'wppa-spheric',
						);


		foreach ( $js_files as $file ) {
			if ( is_file( dirname( __FILE__ ) . '/js/' . $file . '.min.js' ) ) {
				$js_ver = date( "ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'js/' . $file . '.min.js' ) );
				wp_enqueue_script( $file, WPPA_URL . '/js/' . $file . '.min.js', $js_depts, $js_ver, $footer );
			}
			else {
				$js_ver = date( "ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'js/' . $file . '.js' ) );
				wp_enqueue_script( $file, WPPA_URL . '/js/' . $file . '.js', $js_depts, $js_ver, $footer );
			}
		}
	}

	// Start other (vendor) js files
	// google maps
	if ( ( wppa_switch( 'load_map_api' ) || wppa_opt( 'gpx_implementation' ) == 'wppa-plus-embedded' ) && strpos( wppa_opt( 'custom_content' ), 'w#location' ) !== false ) {
		$key = wppa_opt( 'map_apikey' );
		wp_enqueue_script( 	'wppa-geo',
							'https://maps.googleapis.com/maps/api/js?' . ( $key ? 'key=' . $key : 'v=3.exp' ),
							'',
							$wppa_api_version,
							$footer );
	}

	// Nicescroller
	if ( wppa_switch( 'nicescroll' ) || wppa_switch( 'nicescroll_window' ) || wppa_switch( 'load_nicescroller' ) ) {
		$nice_url = WPPA_URL . '/vendor/nicescroll/jquery.nicescroll.min.js';
		wp_enqueue_script( 'nicescrollr-inc-nicescroll-min-js', $nice_url, array( 'jquery', 'nicescrollr-easing-min-js' ), 'all' );
		$easing_url = WPPA_URL . '/vendor/jquery-easing/jquery.easing.min.js';
		wp_enqueue_script( 'nicescrollr-easing-min-js', $easing_url, array( 'jquery' ), 'all' );
	}

	// Panorama
	if ( wppa_switch( 'enable_panorama' ) ) {

		// Only if either in header or in footer and panorama's on the page found or ajax enabled
		if ( ! wppa_switch( 'defer_javascript' ) || ( wppa( 'has_panorama' ) || wppa_switch( 'allow_ajax' ) ) ) {

			$three_url = WPPA_URL . '/vendor/three/three.min.js';
			$ver = '122';

			wp_enqueue_script( 'wppa-three-min-js', $three_url, array(), $ver );
		}
	}

	// End other (vendor) js files

	// wppa-init
	if ( ! file_exists( WPPA_UPLOAD_PATH.'/dynamic/wppa-init.'.$wppa_lang.'.js' ) ) {
		wppa_create_wppa_init_js();
		update_option( 'wppa_ini_js_version_'.$wppa_lang, wppa_get_option( 'wppa_ini_js_version_'.$wppa_lang, '0' ) + '1' );
	}
	if ( file_exists( WPPA_UPLOAD_PATH.'/dynamic/wppa-init.'.$wppa_lang.'.js' ) ) {
		wp_enqueue_script( 'wppa-init', WPPA_UPLOAD_URL.'/dynamic/wppa-init.'.$wppa_lang.'.js', array( 'wppa' ), wppa_get_option( 'wppa_ini_js_version_'.$wppa_lang, $footer ) );
	}

	// wppa-pagedata
	if ( $footer ) {
		add_action( 'wp_footer', 'wppa_print_psjs', 99 );
	}
}

function wppa_print_psjs() {
global $wppa_js_page_data;

	if ( $wppa_js_page_data ) {
		echo
		"\n<!-- wppa page data -->" .
		"\n<script>\n" .
		$wppa_js_page_data .
		"\n</script>\n";
	}
}

/* LOAD WPPA+ THEME */
add_action( 'init', 'wppa_load_theme', 100 );

function wppa_load_theme() {

	// Are we allowed to look in theme?
	if ( wppa_switch( 'use_custom_theme_file' ) ) {

		$usertheme = get_theme_root() . '/' . wppa_get_option( 'template' ) . '/wppa-theme.php';
		if ( is_file( $usertheme ) ) {
			require_once $usertheme;
			return;
		}
	}
	require_once 'theme/wppa-theme.php';
}

/* LOAD FOOTER REQD DATA */
add_action('wp_footer', 'wppa_load_footer');

function wppa_load_footer() {
global $wpdb;
global $wppa_session;

	// If no wppa on the page, quit
	if ( ! wppa( 'mocc' ) && ! wppa_switch( 'lightbox_global' ) && ! wppa_switch( 'nicescroll_window' ) && ! wppa_opt( 'fs_policy' ) == 'global' ) {
		return;
	}

	if ( wppa( 'mocc' ) || wppa_switch( 'lightbox_global' ) ) {
		echo '
			<!-- start WPPA+ Footer data -->
			';

		// Do they use our lightbox?
		if ( wppa_opt( 'lightbox_name' ) == 'wppa' ) {
			$fontsize_lightbox = wppa_opt( 'fontsize_lightbox' ) ? wppa_opt( 'fontsize_lightbox' ) : '10';
			$d = wppa_switch( 'ovl_show_counter') ? 1 : 0;
			$ovlh = wppa_opt( 'ovl_txt_lines' ) == 'auto' ? 'auto' : ((wppa_opt( 'ovl_txt_lines' ) + $d) * ($fontsize_lightbox + 2));
			$txtcol = wppa_opt( 'ovl_theme' ) == 'black' ? '#a7a7a7' : '#272727';
			$dark = wppa( 'is_mobile' ) ? '0.1' : '0.1';

			// The lightbox overlay background
			echo
			'<div' .
				' id="wppa-overlay-bg"' .
				' style="' .
					'text-align:center;' .
					'display:none;' .
					'position:fixed;' .
					'top:0;' .
					'left:0;' .
					'width:100%;' .
					'height:100%;' .
					'background-color:'.wppa_opt( 'ovl_bgcolor' ).';' .
					'"' .
				' onclick="wppaOvlOnclick(event)"' .
				' onwheel="return false;"' .
				' onscroll="return false;"' .
				' >' .
			'</div>';

			// The Lightbox Wrapper and Image container
			echo
			'<div'.
				' id="wppa-overlay-ic"'.
				' style="' .
					'position:fixed;' .
					'top:50%;' .
					'left:50%;' .
					'opacity:1;' .
					'box-shadow:none;' .
					'box-sizing:content-box;' .
					'text-align:center;' .  		// for panorama
					'background-color:transparent;' .
					'"' .
				' onwheel="return false;"' .
				' onscroll="return false;"' .
				' >' .
			'</div>';

			// The preload images
			echo '
			<img
				id="wppa-pre-prev"
				style="position:fixed;left:0;top:50%;width:100px;z-index:200011;visibility:hidden;"
				class="wppa-preload"
				title="Preload preveious image"
				alt="dummy"
			/>
			<img
				id="wppa-pre-next"
				style="position:fixed;right:0;top:50%;width:100px;z-index:200011;visibility:hidden;"
				class="wppa-preload"
				title="Preload next image"
				alt="dummy"
			/>
			<img
				id="wppa-pre-curr"
				style="position:fixed;left:0;top:0;z-index:200011;visibility:hidden;"
				class="wppa-preload-curr"
				title="Preload current image"
				alt="dummy"
			/>';

			// The Spinner image
			echo wppa_get_spinner_svg_html( array( 	'id' 		=> 'wppa-ovl-spin',
													'position' 	=> 'fixed',
													'lightbox' 	=> true,
												) );

			// The init vars
			$data = '
<script type="text/javascript" >

	/* START Lightbox vars */
	wppaOvlTxtHeight = "'.$ovlh.'";
	wppaOvlOpacity = '.(wppa_opt( 'ovl_opacity' )/100).';
	wppaOvlOnclickType = "'.wppa_opt( 'ovl_onclick' ).'";
	wppaOvlTheme = "'.wppa_opt( 'ovl_theme' ).'";
	wppaOvlAnimSpeed = '.wppa_opt( 'ovl_anim' ).';
	wppaOvlSlideSpeed = '.wppa_opt( 'ovl_slide' ).';
	wppaVer4WindowWidth = 800;
	wppaVer4WindowHeight = 600;
	wppaOvlShowCounter = '.( wppa_switch( 'ovl_show_counter') ? 'true' : 'false' ).';
	'.( wppa_opt( 'fontfamily_lightbox' ) ? 'wppaOvlFontFamily = "'.wppa_opt( 'fontfamily_lightbox' ).'"' : '').'
	wppaOvlFontSize = "'.$fontsize_lightbox.'";
	'.( wppa_opt( 'fontcolor_lightbox' ) ? 'wppaOvlFontColor = "'.wppa_opt( 'fontcolor_lightbox' ).'"' : '').'
	'.( wppa_opt( 'fontweight_lightbox' ) ? 'wppaOvlFontWeight = "'.wppa_opt( 'fontweight_lightbox' ).'"' : '').'
	'.( wppa_opt( 'fontsize_lightbox' ) ? 'wppaOvlLineHeight = "'.(wppa_opt( 'fontsize_lightbox' ) + '2').'"' : '').'
	wppaOvlVideoStart = '.( wppa_switch( 'ovl_video_start' ) ? 'true' : 'false' ).';
	wppaOvlAudioStart = '.( wppa_switch( 'ovl_audio_start' ) ? 'true' : 'false' ).';
	wppaOvlShowStartStop = '.( wppa_switch( 'ovl_show_startstop' ) ? 'true' : 'false' ).';
	wppaIsMobile = '.( wppa_is_mobile() ? 'true' : 'false' ).';
	wppaIsIpad = '.( wppa_is_ipad() ? 'true' : 'false' ).';
	wppaOvlIconSize = "'.wppa_opt( 'nav_icon_size_lightbox' ).'px";
	wppaOvlBrowseOnClick = '.( wppa_switch( 'ovl_browse_on_click' ) ? 'true' : 'false' ).';
	/* END Lightbox vars */
</script>
';
			echo wppa_js( $data );
		}

		// The photo views cache
		$data = '
<script type="text/javascript" >

/* START Viewcounts */';
			if ( isset( $wppa_session['photo'] ) ) {
				foreach ( array_keys( $wppa_session['photo'] ) as $p ) {
					$data .= '
					wppaPhotoView[' . $p . '] = true;';
				}
			}
			$data .= '
/* END Vieuwcounts */
</script>';
		echo wppa_js( $data );

		// Debugging, show queries
		wppa_dbg_cachecounts('print');

		// Debugging, show active plugins
		if ( wppa( 'debug' ) ) {
			$plugins = wppa_get_option('active_plugins');
			wppa_dbg_msg('Active Plugins');
			foreach ( $plugins as $plugin ) {
				wppa_dbg_msg($plugin);
			}
			wppa_dbg_msg('End Active Plugins');
		}

		echo '
<!-- Nonce for various wppa actions -->';
		// Nonce field for Ajax bump view counter from lightbox, and rating
		wp_nonce_field( 'wppa-check', 'wppa-nonce', false, true );
		wp_nonce_field( 'wppa-qr-nonce', 'wppa-qr-nonce', false, true );

		echo '
<!-- Do user upload -->';
		// Do the upload if required and not yet done
		wppa_user_upload();

		// Done
		echo '
<!-- Done user upload -->';
	}

	// Window nicescroller
	if ( wppa_switch( 'nicescroll_window' ) ) {
		echo '
<!-- Nice scroller on window, by wppa -->
' . wppa_js(
'<script type="text/javascript" >
	jQuery("document").ready(function(){
		jQuery("body").niceScroll({' . wppa_opt( 'nicescroll_opts' ) . '});
	});
</script>' );
	}
}

/* FACEBOOK COMMENTS */
add_action( 'wp_footer', 'wppa_fbc_setup', 100 );

function wppa_fbc_setup() {
global $wppa_locale;

	if ( wppa_switch( 'load_facebook_sdk' ) &&  			// Facebook sdk requested
		( 	wppa_switch( 'share_on' ) ||
			wppa_switch( 'share_on_widget' ) ||
			wppa_switch( 'share_on_thumbs' ) ||
			wppa_switch( 'share_on_lightbox' ) ||
			wppa_switch( 'share_on_mphoto' ) ) &&
		(	wppa_switch( 'share_facebook' ) ||
			wppa_switch( 'facebook_like' ) ||
			wppa_switch( 'facebook_comments' ) )			// But is it used by wppa?
	) {
		?>
		<!-- Facebook Comments for WPPA+ -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/<?php echo $wppa_locale; ?>/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	<?php
	}
}

/* CHECK REDIRECTION */
add_action( 'plugins_loaded', 'wppa_redirect', '1' );

function wppa_redirect() {

	if ( ! isset( $_SERVER["REQUEST_URI"] ) ) return;

	$uri = $_SERVER["REQUEST_URI"];
	$wppapos = stripos( $uri, '/wppaspec/' );
	if ( $wppapos === false ) {

		$wppapos = strpos( $uri, '/-/' );
		if ( wppa_get_option( 'wppa_use_pretty_links' ) != 'compressed' ) {
			$wppapos = false;
		}
	}

	if ( $wppapos !== false && wppa_get_option( 'permalink_structure' ) ) {

		// old style solution, still required when qTranslate is active
		$plugins = implode( ',', wppa_get_option( 'active_plugins' ) );
		if ( stripos( $plugins, 'qtranslate' ) !== false ) {

			$newuri = wppa_convert_from_pretty( $uri );
			if ( $newuri == $uri ) return;

			// Although the url is urlencoded it is damaged by wp_redirect when it contains chars like �, so we do a header() call
			header( 'Location: '.$newuri, true, 302 );
			exit;
		}

		// New style solution
		$newuri = wppa_convert_from_pretty( $uri );
		if ( $newuri == $uri ) return;
		$_SERVER["REQUEST_URI"] = $newuri;
		wppa_convert_uri_to_get( $newuri );
	}
}

/* ADD PAGE SPECIFIC ( http or https ) URLS */
add_action( 'wp_head', 'wppa_add_page_specific_urls_and_language', '99' );

function wppa_add_page_specific_urls_and_language() {
global $wppa_js_page_data;
global $wppa_lang;

	$result = '
wppaImageDirectory = "' . wppa_get_imgdir() . '";
wppaWppaUrl = "' . wppa_get_wppa_url() . '";
wppaIncludeUrl = "' . trim( includes_url(), '/' ) . '";
wppaAjaxUrl = "' . ( wppa_switch( 'ajax_non_admin' ) ? wppa_url( 'wppa-ajax-front.php' ) : admin_url( 'admin-ajax.php' ) ) . '";
wppaUploadUrl = "' . WPPA_UPLOAD_URL . '";
wppaIsIe = ' . ( wppa_is_ie() ? 'true' : 'false' ) . ';
wppaIsSafari = ' . ( wppa_is_safari() ? 'true' : 'false' ) . ';
wppaSlideshowNavigationType = "' . wppa_get_navigation_type() . '";
wppaAudioHeight = '.wppa_get_audio_control_height().';
wppaFilmThumbTitle = "'.( wppa_opt( 'film_linktype' ) == 'lightbox' ? wppa_zoom_in( false ) : __('Double click to start/stop slideshow running', 'wp-photo-album-plus') ).'";
wppaClickToView = "'.( wppa_opt( 'film_linktype' ) == 'lightbox' ? wppa_zoom_in( false ) : __('Click to view', 'wp-photo-album-plus') ).'";
wppaLang = "'.$wppa_lang.'";
wppaVoteForMe = "'.__(wppa_opt( 'vote_button_text' ), 'wp-photo-album-plus').'";
wppaVotedForMe = "'.__(wppa_opt( 'voted_button_text' ), 'wp-photo-album-plus').'";
wppaDownLoad = "'.__('Download', 'wp-photo-album-plus').'";
wppaSlideShow = "'.__('Slideshow', 'wp-photo-album-plus').'";
wppaPhoto = "'.__('Photo', 'wp-photo-album-plus' ).'";
wppaOf = "'.__('of', 'wp-photo-album-plus' ).'";
wppaNextPhoto = "'.__('Next photo', 'wp-photo-album-plus').'";
wppaPreviousPhoto = "'.__('Previous photo', 'wp-photo-album-plus').'";
wppaNextP = "'.__('Next', 'wp-photo-album-plus' ).'";
wppaPrevP = "'.__('Prev.', 'wp-photo-album-plus').'";
wppaAvgRating = "'.__('Average&nbsp;rating', 'wp-photo-album-plus').'";
wppaMyRating = "'.__('My&nbsp;rating', 'wp-photo-album-plus').'";
wppaAvgRat = "'.__('Avg.', 'wp-photo-album-plus').'";
wppaMyRat = "'.__('Mine', 'wp-photo-album-plus').'";
wppaDislikeMsg = "'.__('You marked this image as inappropriate.', 'wp-photo-album-plus').'";
wppaStart = "'.__('Start', 'wp-photo-album-plus').'";
wppaStop = "'.__('Stop', 'wp-photo-album-plus').'";
wppaPleaseName = "'.__('Please enter your name', 'wp-photo-album-plus').'";
wppaPleaseEmail = "'.__('Please enter a valid email address', 'wp-photo-album-plus').'";
wppaPleaseComment = "'.__('Please enter a comment', 'wp-photo-album-plus').'";
wppaProcessing = "'.__('Processing...', 'wp-photo-album-plus').'";
wppaDone = "'.__('Done!', 'wp-photo-album-plus').'";
wppaUploadFailed = "'.__('Upload failed', 'wp-photo-album-plus').'";
wppaServerError = "'.__('Server error.', 'wp-photo-album-plus').'";
wppaGlobalFsIconSize = "'.wppa_opt( 'nav_icon_size_global_fs' ).'";
wppaFsFillcolor = "'.wppa_opt( 'fs_svg_color' ).'";
wppaFsBgcolor = "'.wppa_opt( 'fs_svg_bg_color' ).'";
wppaFsPolicy = "'.wppa_opt( 'fs_policy' ).'";
';

	// Relative urls?
	$result = wppa_make_relative( $result );

	if ( wppa_js_in_footer() ) {
		$wppa_js_page_data .=
		"\n/* WPPA+ START Page/language dependant data */" .
		$result .
		"\n/* WPPA+ END Page/language dependant data */\n";
	}
	else {
	echo '
<!-- WPPA+ START Page/language dependant data -->
<script type="text/javascript" >' . $result . '</script>
<!-- WPPA+ END Page/language dependant data -->';
	}
}

/* ENABLE RENDERING */
add_action( 'wp_head', 'wppa_kickoff', '100' );

function wppa_kickoff() {
global $wppa_lang;
global $wppa_api_version;
global $wppa_init_js_data;
global $wppa_dynamic_css_data;

	// init.css failed?
	if ( $wppa_dynamic_css_data ) echo $wppa_dynamic_css_data;

	// init.js failed?
	if ( $wppa_init_js_data ) echo $wppa_init_js_data;

	// Inline styles?
	if ( wppa_switch( 'inline_css') ) {
		echo '
<!-- WPPA+ Custom styles -->
<style type="text/css" >';
		echo wppa_opt( 'custom_style' ) . '
</style>';
	}

	// Browser dependant css
	if ( wppa_is_edge() ) {
		if ( wppa_opt( 'custom_style_edge' ) ) {
			echo '
<!-- WPPA+ Custom styles Edge -->
<style type="text/css" >
' . wppa_opt( 'custom_style_edge' ) . '
</style>';
		}
	}
	elseif ( wppa_is_chrome() ) {
		if ( wppa_opt( 'custom_style_chrome' ) ) {
			echo '
<!-- WPPA+ Custom styles Chrome -->
<style type="text/css" >
' . wppa_opt( 'custom_style_chrome' ) . '
</style>';
		}
	}
	elseif ( wppa_is_firefox() ) {
		if ( wppa_opt( 'custom_style_firefox' ) ) {
			echo '
<!-- WPPA+ Custom styles Firefox -->
<style type="text/css" >
' . wppa_opt( 'custom_style_firefox' ) . '
</style>';
		}
	}
	elseif ( wppa_is_safari() ) {
		if ( wppa_opt( 'custom_style_safari' ) ) {
			echo '
<!-- WPPA+ Custom styles Safari -->
<style type="text/css" >
' . wppa_opt( 'custom_style_safari' ) . '
</style>';
		}
	}
	elseif ( wppa_is_ie() ) {
		if ( wppa_opt( 'custom_style_ie' ) ) {
			echo '
<!-- WPPA+ Custom styles IE -->
<style type="text/css" >
' . wppa_opt( 'custom_style_ie' ) . '
</style>';
		}
	}
	elseif ( wppa_is_opera() ) {
		if ( wppa_opt( 'custom_style_opera' ) ) {
			echo '
<!-- WPPA+ Custom styles Opera -->
<style type="text/css" >
' . wppa_opt( 'custom_style_opera' ) . '
</style>';
		}
	}

	// Pinterest js
	if ( ( wppa_switch( 'share_on') || wppa_switch( 'share_on_widget') ) && wppa_switch( 'share_pinterest') ) {
		echo '
<!-- Pinterest share -->
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>';
	}

	if ( wppa( 'debug' ) ) {
		error_reporting( wppa( 'debug' ) );

		echo '
<script type="text/javascript" >
	wppaDebug = true;
</script>';
	}

	wppa( 'rendering_enabled', true );
	echo '
<!-- Rendering enabled -->
<!-- /WPPA Kickoff -->

	';

}

/* SKIP JETPACK FOTON ON WPPA+ IMAGES */
add_filter('jetpack_photon_skip_image', 'wppa_skip_photon', 10, 3);
function wppa_skip_photon($val, $src, $tag) {
	$result = $val;
	if ( strpos($src, WPPA_UPLOAD_URL) !== false ) $result = true;
	return $result;
}

/* Create dynamic js init file */
function wppa_create_wppa_init_js() {
global $wppa_api_version;
global $wppa_lang;
global $wppa_init_js_data;

	// Init
	if ( is_numeric(wppa_opt( 'fullimage_border_width' )) ) $fbw = wppa_opt( 'fullimage_border_width' ) + '1'; else $fbw = '0';

	// Make content
	$content =
'/* -- WPPA+ Runtime parameters
/*
/* Dynamicly Created on '.date('c').'
/*
*/
';
	if ( ( WPPA_DEBUG || wppa_get_get( 'debug' ) || WP_DEBUG ) && ! wppa_switch( 'defer_javascript' ) ) {
	$content .= '
/* Check if wppa.js and jQuery are present */
if (typeof(_wppaSlides) == \'undefined\') alert(\'There is a problem with your theme. The file wppa.js is not loaded when it is expected (Errloc = wppa_kickoff).\');
if (typeof(jQuery) == \'undefined\') alert(\'There is a problem with your theme. The jQuery library is not loaded when it is expected (Errloc = wppa_kickoff).\');
';	}
	/* This goes into wppa.js */
	$content .= '
wppaVersion = "'.$wppa_api_version.'";
wppaDebug = '.( wppa_switch( 'allow_debug' ) ? 'true' : 'false' ).';
wppaBackgroundColorImage = "'.wppa_opt( 'bgcolor_img' ).'";
wppaPopupLinkType = "'.wppa_opt( 'thumb_linktype' ).'";
wppaAnimationType = "'.wppa_opt( 'animation_type' ).'";
wppaAnimationSpeed = '.wppa_opt( 'animation_speed' ).';
wppaThumbnailAreaDelta = '.wppa_get_thumbnail_area_delta().';
wppaTextFrameDelta = '.wppa_get_textframe_delta().';
wppaBoxDelta = '.wppa_get_box_delta().';
wppaSlideShowTimeOut = '.wppa_opt( 'slideshow_timeout' ).';
wppaFilmShowGlue = '.( wppa_switch( 'film_show_glue') ? 'true' : 'false' ).';
wppaMiniTreshold = '.( wppa_opt( 'mini_treshold' ) ? wppa_opt( 'mini_treshold' ) : '0' ).';
wppaRatingOnce = '.( wppa_switch( 'rating_change') || wppa_switch( 'rating_multi') ? 'false' : 'true' ).';
wppaHideWhenEmpty = '.( wppa_switch( 'hide_when_empty') ? 'true' : 'false' ).';
wppaBGcolorNumbar = "'.wppa_opt( 'bgcolor_numbar' ).'";
wppaBcolorNumbar = "'.wppa_opt( 'bcolor_numbar' ).'";
wppaBGcolorNumbarActive = "'.wppa_opt( 'bgcolor_numbar_active' ).'";
wppaBcolorNumbarActive = "'.wppa_opt( 'bcolor_numbar_active' ).'";
wppaFontFamilyNumbar = "'.wppa_opt( 'fontfamily_numbar' ).'";
wppaFontSizeNumbar = "'.wppa_opt( 'fontsize_numbar' ).'px";
wppaFontColorNumbar = "'.wppa_opt( 'fontcolor_numbar' ).'";
wppaFontWeightNumbar = "'.wppa_opt( 'fontweight_numbar' ).'";
wppaFontFamilyNumbarActive = "'.wppa_opt( 'fontfamily_numbar_active' ).'";
wppaFontSizeNumbarActive = "'.wppa_opt( 'fontsize_numbar_active' ).'px";
wppaFontColorNumbarActive = "'.wppa_opt( 'fontcolor_numbar_active' ).'";
wppaFontWeightNumbarActive = "'.wppa_opt( 'fontweight_numbar_active' ).'";
wppaNumbarMax = "'.wppa_opt( 'numbar_max' ).'";
wppaNextOnCallback = '.( wppa_switch( 'next_on_callback') ? 'true' : 'false' ).';
wppaStarOpacity = '.str_replace(',', '.',( wppa_opt( 'star_opacity' )/'100' )).';
wppaEmailRequired = "'.wppa_opt( 'comment_email_required').'";
wppaSlideBorderWidth = '.$fbw.';
wppaAllowAjax = '.( wppa_switch( 'allow_ajax') ? 'true' : 'false' ).';
wppaUsePhotoNamesInUrls = '.( wppa_switch( 'use_photo_names_in_urls') ? 'true' : 'false' ).';
wppaThumbTargetBlank = '.( wppa_switch( 'thumb_blank') ? 'true' : 'false' ).';
wppaRatingMax = '.wppa_opt( 'rating_max' ).';
wppaRatingDisplayType = "'.wppa_opt( 'rating_display_type' ).'";
wppaRatingPrec = '.wppa_opt( 'rating_prec' ).';
wppaStretch = '.( wppa_switch( 'enlarge') ? 'true' : 'false' ).';
wppaMinThumbSpace = '.wppa_opt( 'tn_margin' ).';
wppaThumbSpaceAuto = '.( wppa_switch( 'thumb_auto') ? 'true' : 'false' ).';
wppaMagnifierCursor = "'.wppa_opt( 'magnifier' ).'";
wppaArtMonkyLink = "'.wppa_opt( 'art_monkey_link' ).'";
wppaAutoOpenComments = '.( wppa_switch( 'auto_open_comments') ? 'true' : 'false' ).';
wppaUpdateAddressLine = '.( wppa_switch( 'update_addressline') ? 'true' : 'false' ).';
wppaSlideSwipe = '.( wppa_switch( 'slide_swipe') ? 'true' : 'false' ).';
wppaMaxCoverWidth = '.wppa_opt( 'max_cover_width' ).';
wppaSlideToFullpopup = '.( wppa_opt( 'slideshow_linktype' ) == 'fullpopup' ? 'true' : 'false' ).';
wppaComAltSize = '.wppa_opt( 'comten_alt_thumbsize' ).';
wppaBumpViewCount = '.( wppa_switch( 'track_viewcounts') ? 'true' : 'false' ).';
wppaBumpClickCount = '.( wppa_switch( 'track_clickcounts') ? 'true' : 'false' ).';
wppaShareHideWhenRunning = '.( wppa_switch( 'share_hide_when_running') ? 'true' : 'false' ).';
wppaFotomoto = '.( wppa_switch( 'fotomoto_on') ? 'true' : 'false' ).';
wppaArtMonkeyButton = '.( wppa_opt( 'art_monkey_display' ) == 'button' ? 'true' : 'false' ).';
wppaFotomotoHideWhenRunning = '.( wppa_switch( 'fotomoto_hide_when_running') ? 'true' : 'false' ).';
wppaCommentRequiredAfterVote = '.( wppa_switch( 'vote_needs_comment') ? 'true' : 'false' ).';
wppaFotomotoMinWidth = '.wppa_opt( 'fotomoto_min_width' ).';
wppaShortQargs = '.( wppa_switch( 'use_short_qargs') ? 'true' : 'false' ).';
wppaOvlHires = '.( wppa_switch( 'lb_hres' ) ? 'true' : 'false' ).';
wppaSlideVideoStart = '.( wppa_switch( 'start_slide_video' ) ? 'true' : 'false' ).';
wppaSlideAudioStart = '.( wppa_switch( 'start_slide_audio' ) ? 'true' : 'false' ).';
wppaRel = "'.( wppa_opt( 'lightbox_name' ) == 'wppa' ? 'data-rel' : 'rel' ).'";
wppaOvlRadius = '.wppa_opt( 'ovl_border_radius' ).';
wppaOvlBorderWidth = '.wppa_opt( 'ovl_border_width' ).';
wppaEditPhotoWidth = "'.(wppa_opt( 'upload_edit' ) == 'new' ? 500 : 960).'";
wppaThemeStyles = "'.(wppa_switch( 'upload_edit_theme_css') ? get_stylesheet_uri() : '' ).'";
wppaStickyHeaderHeight = '.wppa_opt( 'sticky_header_size' ).';
wppaRenderModal = ' . ( wppa_switch( 'ajax_render_modal' ) ? 'true' : 'false' ) . ';
wppaModalQuitImg = "url(' . wppa_get_imgdir( 'smallcross-' . wppa_opt( 'ovl_theme') . '.gif' ) . ')";
wppaBoxRadius = "' . wppa_opt( 'bradius' ) . '";
wppaModalBgColor = "' . wppa_opt( 'bgcolor_modal' ) . '";
wppaUploadEdit = "' . wppa_opt( 'upload_edit' ) . '";
wppaSvgFillcolor = "' . wppa_opt( 'svg_color' ) . '";
wppaSvgBgcolor = "' . wppa_opt( 'svg_bg_color' ) . '";
wppaOvlSvgFillcolor = "' . wppa_opt( 'ovl_svg_color' ) . '";
wppaOvlSvgBgcolor = "' . wppa_opt( 'ovl_svg_bg_color' ) . '";
wppaSvgCornerStyle = "' . wppa_opt( 'icon_corner_style' ) . '";
wppaHideRightClick = ' . ( wppa_switch( 'no_rightclick' ) ? 'true' : 'false' ) . ';
wppaGeoZoom = ' . wppa_opt( 'geo_zoom' ) . ';
wppaLazyLoad = ' . ( wppa_switch( 'lazy' ) ? 'true' : 'false' ) . ';
wppaAreaMaxFrac = ' . ( wppa_opt( 'area_size' ) < 1 ? wppa_opt( 'area_size' ) : 1.0 ) . ';
wppaNiceScroll = ' . ( wppa_switch( 'nicescroll' ) ? 'true' : 'false' ) . ';
wppaIconSizeNormal = "' . wppa_opt( 'nav_icon_size' ) . '";
wppaIconSizeSlide = "' . wppa_opt( 'nav_icon_size_slide' ) . '";
wppaResponseSpeed = ' . wppa_opt( 'response_speed' ) . ';
wppaExtendedResizeCount = ' . wppa_opt( 'extended_resize_count' ) . ';
wppaExtendedResizeDelay = ' . wppa_opt( 'extended_resize_delay' ) . ';
wppaCoverSpacing = ' . wppa_opt( 'cover_spacing' ) . ';
wppaFilmonlyContinuous = ' . ( wppa_switch( 'filmonly_continuous' ) ? 'true' : 'false' ) . ';
wppaNoAnimateOnMobile = ' . ( wppa_switch( 'no_animate_on_mobile' ) ? 'true' : 'false' ) . ';
wppaAjaxScroll = ' . ( wppa_switch( 'ajax_scroll' ) ? 'true' : 'false' ) . ';
wppaThumbSize = ' . wppa_opt( 'thumbsize' ) . ';
wppaTfMargin = ' . wppa_opt( 'tn_margin' ) . ';
wppaArtmonkeyFileNotSource = ' . ( 	wppa_opt( 'art_monkey_link' ) == 'file' &&
									wppa_opt( 'art_monkey_display' ) == 'text' &&
									! wppa_switch( 'artmonkey_use_source' ) ? 'true' : 'false' ) . '
wppaRequestInfoDialogText = "' . wppa_opt( 'request_info_text' ) . '";';

	// Thumbnail aspect (for real calendar)
	$aspect = 1;
	if ( wppa_opt( 'thumb_aspect' ) != '0:0:none' ) {
		$t = explode( ':', wppa_opt( 'thumb_aspect' ) );
		$aspect = $t[0] / $t[1];
	}
	elseif ( wppa_opt( 'resize_to' ) ) {
		$t = explode( 'x', wppa_opt( 'resize_to' ) );
		$aspect = $t[1] / $t[0];
	}
	else {
		$aspect = wppa_opt( 'maxheight' ) / wppa_opt( 'fullsize' );
	}
$content .= '
wppaThumbAspect = ' . $aspect . ';';

	// Create file
	$path = WPPA_UPLOAD_PATH.'/dynamic/wppa-init.'.$wppa_lang.'.js';
	if ( wppa_put_contents( $path, $content ) ) {
		$wppa_init_js_data = '';
	}
	else {
		$wppa_init_js_data =
'<script type="text/javascript" >
/* Warning: file wppa-init.'.$wppa_lang.'.js could not be created */
/* The content is therefor output here */

'.$content.'
</script>
';
	}
}

add_action( 'init', 'wppa_set_shortcode_priority', 100 );

function wppa_set_shortcode_priority() {

	$newpri = wppa_opt( 'shortcode_priority' );
	if ( $newpri == '11' ) return;	// Default, do not change

	$oldpri = has_filter( 'the_content', 'do_shortcode' );
	if ( $oldpri ) {
		remove_filter( 'the_content', 'do_shortcode', $oldpri );
		add_filter( 'the_content', 'do_shortcode', $newpri );
	}
}


/* We use bbPress */
// editor bbpress in tinymce mode
function wppa_enable_visual_editor_in_bbpress( $args = array() ) {

	if ( wppa_switch( 'photo_on_bbpress' ) ) {
		$args['tinymce'] = true;
		$args['teeny'] = false;
	}
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'wppa_enable_visual_editor_in_bbpress' );

// remove insert wp image button
function wppa_remove_image_button_in_bbpress( $buttons ) {

	if ( wppa_switch( 'photo_on_bbpress' ) ) {
		if ( ( $key = array_search( 'image', $buttons ) ) !== false ) {
			unset( $buttons[$key] );
		}
	}
	return $buttons ;
}
add_filter( 'bbp_get_teeny_mce_buttons', 'wppa_remove_image_button_in_bbpress' );

// enable processing shortcodes
function wppa_enable_shortcodes_in_bbpress( $content ) {

	if ( wppa_switch( 'photo_on_bbpress' ) ) {
		$content = do_shortcode( $content );
	}
	return $content;
}
add_filter( 'bbp_get_topic_content', 'wppa_enable_shortcodes_in_bbpress', 1000 );
add_filter( 'bbp_get_reply_content', 'wppa_enable_shortcodes_in_bbpress', 1000 );

// Disable Autoptimize from optimizing our javascript
add_filter( 'autoptimize_filter_js_noptimize', 'wppa_nopti_js', 10, 2 );
function wppa_nopti_js( $nopt_in, $html_in ) {
	if ( strpos( $html_in, 'data-wppa="yes"' ) !== false ) {
		return true;
	}
	else {
		return false;
	}
}

// This function contains strings for i18n from files not included
// in the search for frontend required translatable strings
// Mainly from widgets
function wppa_dummy() {

	// Commet widget
	__( 'wrote' , 'wp-photo-album-plus' );
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'There are no commented photos (yet)', 'wp-photo-album-plus' );

	// Featen widget
	__( 'View the featured photos', 'wp-photo-album-plus' );
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'There are no featured photos (yet)', 'wp-photo-album-plus' );

	// Lasten widget
	__( 'View the most recent uploaded photos', 'wp-photo-album-plus' );
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'There are no uploaded photos (yet)', 'wp-photo-album-plus' );

	// Potd widget
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'By:', 'wp-photo-album-plus' );

	// Slideshow widget
	__( 'No album defined (yet)', 'wp-photo-album-plus' );

	// Thumbnail widget
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'There are no photos (yet)', 'wp-photo-album-plus' );

	// Upldr widget
	__( 'There are too many registered users in the system for this widget' , 'wp-photo-album-plus' );
	__( 'Photos uploaded by', 'wp-photo-album-plus' );

	// Topten widget
	_n( '%d vote', '%d votes', $n, 'wp-photo-album-plus' );
	_n( '%d view', '%d views', $n, 'wp-photo-album-plus' );
	__( 'Photo not found', 'wp-photo-album-plus' );
	__( 'There are no rated photos (yet)', 'wp-photo-album-plus' );

	// From wppa-filter.php
	__( 'delay', 'wp-photo-album-plus' );
	__( 'cache', 'wp-photo-album-plus' );
	__( 'single image', 'wp-photo-album-plus' );

}