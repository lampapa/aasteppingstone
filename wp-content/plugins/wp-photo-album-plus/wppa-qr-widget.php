<?php
/* wppa-qr-widget.php
* Package: wp-photo-album-plus
*
* display qr code
* Version 7.3.03
*/

class wppaQRWidget extends WP_Widget {

    /** constructor */
    function __construct() {
		$widget_ops = array( 'classname' => 'qr_widget', 'description' => __( 'Display the QR code of the current url' , 'wp-photo-album-plus' ) );
		parent::__construct( 'qr_widget', __( 'WPPA+ QR Widget', 'wp-photo-album-plus' ), $widget_ops );
    }

	/** @see WP_Widget::widget */
    function widget( $args, $instance ) {
		global $wpdb;

		// Initialize
		wppa_widget_timer( 'init' );
		wppa_reset_occurrance();
        wppa( 'in_widget', 'qr' );
		wppa_bump_mocc();
        extract( $args );
		$instance 		= wppa_parse_args( (array) $instance, $this->get_defaults() );
		$widget_title 	= apply_filters( 'widget_title', $instance['title'] );
		$cache 			= $instance['cache'];
		$cachefile 		= wppa_get_widget_cache_path( $this->id );

		// Logged in only and logged out?
		if ( wppa_checked( $instance['logonly'] ) && ! is_user_logged_in() ) {
			return;
		}

		// Cache?
		if ( $cache ) {

			if ( wppa_is_file( $cachefile ) ) {

				// Cache expired?
				if ( $cache != 'inf' && wppa_filetime( $cachefile ) < time() - 60 * $cache ) {
					wppa_remove_widget_cache_path( $this->id );
				}

				// No, use it
				else {
					echo wppa_get_contents( $cachefile );
					echo wppa_widget_timer( 'show', $widget_title, true );
					wppa( 'in_widget', false );
					return;
				}
			}
		}

		// Other inits
		$qrsrc 	= 	'http' . ( is_ssl() ? 's' : '' ) . '://api.qrserver.com/v1/create-qr-code/' .
					'?format=svg' .
					'&size='. wppa_opt( 'qr_size' ).'x'.wppa_opt( 'qr_size' ) .
					'&color='.trim( wppa_opt( 'qr_color' ), '#' ) .
					'&bgcolor='.trim( wppa_opt( 'qr_bgcolor' ), '#' ) .
					'&data=' . urlencode( $_SERVER['SCRIPT_URI'] );

		// Get the qrcode
		$qrsrc = wppa_create_qrcode_cache( $_SERVER['SCRIPT_URI'], wppa_opt( 'qr_size' ) );

		// Make the html
		$widget_content = '
		<div
			style="text-align:center;"
			data-wppa="yes"
			>
			<img
				id="wppa-qr-img"
				src="' . $qrsrc . '"
				title="' . esc_attr( wppa_convert_to_pretty( $_SERVER['SCRIPT_URI'] ) ) . '"
				alt="' . __( 'QR code', 'wp-photo-album-plus' ) . '"
			/>
		</div>
		<div style="clear:both" ></div>';

		$widget_content .=
		'<script type="text/javascript" >
			var wppaQRUrl = document.location.href;
			function wppaQRUpdate( arg ) {
				if ( arg ) {
					wppaQRUrl = arg;
				}
				wppaAjaxSetQrCodeSrc( wppaQRUrl, "wppa-qr-img" );
				document.getElementById( "wppa-qr-img" ).title = wppaQRUrl;
				return;
			}
			' . wppa_js( '
			jQuery(document).ready(function(){
				wppaQRUpdate();
			});' ) . '
		</script>';

		// Output
		$result = "\n" . $before_widget;
		if ( ! empty( $widget_title ) ) {
			$result .= $before_title . $widget_title . $after_title;
		}
		$result .= $widget_content . $after_widget;

		echo $result;
		echo wppa_widget_timer( 'show', $widget_title );

		// Cache?
		if ( $cache ) {
			wppa_put_contents( $cachefile, $result );
		}

		wppa( 'in_widget', false );

    }

    /** @see WP_Widget::update */
    function update( $new_instance, $old_instance ) {

		// Completize all parms
		$instance = wppa_parse_args( $new_instance, $this->get_defaults() );

		// Sanitize certain args
		$instance['title'] 		= strip_tags( $instance['title'] );

		wppa_remove_widget_cache_path( $this->id );
	}

    /** @see WP_Widget::form */
    function form( $instance ) {

		// Defaults
		$instance = wppa_parse_args( (array) $instance, $this->get_defaults() );

		// Title
		echo
		wppa_widget_input( $this, 'title', $instance['title'], __( 'Title', 'wp-photo-album-plus' ) );

		// Loggedin only
		echo
		wppa_widget_checkbox( $this, 'logonly', $instance['logonly'], __( 'Show to logged in visitors only', 'wp-photo-album-plus' ) );

		// Cache
// Do not cache QR widget
//		echo
//		wppa_widget_cache( $this, $instance['cache'] );

		// Explanation
		echo
		'<p>' .
			__( 'You can set the sizes and colors in this widget in the <b>Photo Albums -> Settings</b> admin page Table IX-K1.x.', 'wp-photo-album-plus' ) .
		'</p>';

	}

	// Set defaults
	function get_defaults() {

		$defaults = array( 	'title' 	=> __( 'Sidebar Slideshow', 'wp-photo-album-plus' ),
							'logonly' 	=> 'no',
							'cache' 	=> '0',
							);
		return $defaults;
	}

} // class wppaQRWidget

// register wppaQRWidget widget
add_action('widgets_init', 'wppa_register_QRWidget' );

function wppa_register_QRWidget() {
	register_widget( "wppaQRWidget" );
}
