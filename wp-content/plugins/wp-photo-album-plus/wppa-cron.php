<?php
/* wppa-cron.php
* Package: wp-photo-album-plus
*
* Contains all cron functions
* Version 7.5.07
*
*
*/

// Are we in a cron job?
function wppa_is_cron() {

	if ( isset( $_GET['doing_wp_cron'] ) ) {
		return $_GET['doing_wp_cron'];
	}
	if ( defined( 'DOING_CRON' ) ) {
		return DOING_CRON;
	}
	return false;
}

// Activate our maintenance hook
add_action( 'wppa_cron_event', 'wppa_do_maintenance_proc', 10, 1 );

// Schedule maintenance proc
function wppa_schedule_maintenance_proc( $slug, $from_settings_page = false ) {
global $is_reschedule;

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	// Schedule cron job
	if ( ! wp_next_scheduled( 'wppa_cron_event', array( $slug ) ) ) {
		if ( $is_reschedule || $from_settings_page ) {
			$delay = 5;
		}
		else switch ( $slug ) {
			case 'wppa_cleanup_index':			// 1 hour
				$delay = 3600;
				break;
			case 'wppa_remake_index_albums':
				$delay = 180;
				break;
			default:
				$delay = 10;
		}
		wp_schedule_single_event( time() + $delay, 'wppa_cron_event', array( $slug ) );
		$backtrace = debug_backtrace();
		$args = '';
		if ( is_array( $backtrace[1]['args'] ) ) {
			foreach( $backtrace[1]['args'] as $arg ) {
				if ( $args ) {
					$args .= ', ';
				}
				$args .= str_replace( "\n", '', var_export( $arg, true ) );
			}
			$args = trim( $args );
			if ( $args ) {
				$args = ' ' . str_replace( ',)', ', )', $args ) . ' ';
			}
		}
		elseif ( $backtrace[1]['args'] ) {
			$args = " '" . $backtrace[1]['args'] . "' ";
		}

		$re = $is_reschedule ? 're-' : '';
		wppa_log( 'Cron', '{b}' . $slug . '{/b} ' . $re . 'scheduled by {b}' . $backtrace[1]['function'] . '(' . $args . '){/b} on line {b}' . $backtrace[0]['line'] . '{/b} of ' . basename( $backtrace[0]['file'] ) . ' called by ' . $backtrace[2]['function'] );
	}

	// Update appropriate options
	update_option( $slug . '_status', 'Cron job' );
	update_option( $slug . '_user', 'cron-job' );

	// Inform calling Ajax proc about the results
	if ( $from_settings_page ) {
		echo '||' . $slug . '||' . 'Cron job' . '||0||reload';
	}

}

// Is cronjob crashed?
function wppa_is_maintenance_cron_job_crashed( $slug ) {

	// Asume not
	$result = false;

	// If there is a last timestamp longer than 5 minutes ago...
	$lasttime = wppa_get_option( $slug.'_lasttimestamp', '0' );
	if ( $lasttime && $lasttime < ( time() - 300 ) ) {

		// And the user is cron
		if ( wppa_get_option( $slug . '_user' ) == 'cron-job' ) {

			// And proc is not scheduled
			if ( ! wp_next_scheduled( 'wppa_cron_event', array( $slug ) ) ) {

				// It is crashed
				$result = true;
			}
		}
	}

	// No last timestamp, maybe never started?
	elseif ( ! $lasttime ) {

		// Nothing done yet
		if ( wppa_get_option( $slug . 'last' ) == '0' ) {

			// Togo not calculated yet
			if ( wppa_get_option( $slug . 'togo' ) == '' ) {

				// If the user is cron
				if ( wppa_get_option( $slug . '_user' ) == 'cron-job' ) {

					// And proc is not scheduled
					if ( ! wp_next_scheduled( 'wppa_cron_event', array( $slug ) ) ) {

						// It is crashed
						$result = true;
					}
				}
			}
		}
	}

	return $result;
}

// Activate our cleanup session hook
add_action( 'wppa_cleanup', 'wppa_do_cleanup' );

// Schedule cleanup session database table
function wppa_schedule_cleanup( $now = false ) {

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	// Immediate action requested?
	if ( $now ) {
		wp_schedule_single_event( time() + 1, 'wppa_cleanup' );
	}
	// Schedule cron job
	if ( ! wp_next_scheduled( 'wppa_cleanup' ) ) {
		wp_schedule_event( time(), 'hourly', 'wppa_cleanup' );
	}
}

// The actual cleaner
function wppa_do_cleanup() {
global $wpdb;
global $wppa_endtime;

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	ob_start();

	wppa_log( 'Cron', '{b}wppa_cleanup{/b} started.' );

	// Start renew crypt processes if configured
	if ( wppa_opt( 'crypt_albums_every' ) ) {
		wppa_log( 'Cron', '{b}wppa_cleanup{/b} renew albumcrypt.' );
		$last = wppa_get_option( 'wppa_crypt_albums_lasttimestamp', '0' );
		if ( $last + wppa_opt( 'crypt_albums_every' ) * 3600 < time() ) {
			wppa_schedule_maintenance_proc( 'wppa_crypt_albums' );
			update_option( 'wppa_crypt_albums_lasttimestamp', time() );
		}
	}
	wppa_log( 'Cron', 'Phase 1, time left = '.($wppa_endtime-time()) );
	if ( wppa_opt( 'crypt_photos_every' ) ) {
		wppa_log( 'Cron', '{b}wppa_cleanup{/b} renew photocrypt.' );
		$last = wppa_get_option( 'wppa_crypt_photos_lasttimestamp', '0' );
		if ( $last + wppa_opt( 'crypt_photos_every' ) * 3600 < time() ) {
			wppa_schedule_maintenance_proc( 'wppa_crypt_photos' );
			update_option( 'wppa_crypt_photos_lasttimestamp', time() );
		}
	}
	wppa_log( 'Cron', 'Phase 2, time left = '.($wppa_endtime-time()) );

	// Cleanup session db table
	wppa_log( 'Cron', '{b}wppa_cleanup{/b} cleanup sessions.' );
	$lifetime 	= 3600;			// Sessions expire after one hour
	$savetime 	= 86400;		// Save session data for 24 hour
	$expire 	= time() - $lifetime;
	$purge 		= time() - $savetime;
	$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->wppa_session SET status = 'expired' WHERE timestamp < %s", $expire ) );
	$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->wppa_session WHERE timestamp < %s", $purge ) );
	wppa_log( 'Cron', 'Phase 3, time left = '.($wppa_endtime-time()) );

	// Delete obsolete spam
	$spammaxage = wppa_opt( 'spam_maxage' );
	if ( $spammaxage != 'none' ) {
		wppa_log( 'Cron', '{b}wppa_cleanup{/b} cleanup spam.' );
		$time = time();
		$obsolete = $time - $spammaxage;
		$iret = $wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->wppa_comments WHERE status = 'spam' AND timestamp < %s", $obsolete ) );
		if ( $iret ) wppa_update_option( 'wppa_spam_auto_delcount', wppa_get_option( 'wppa_spam_auto_delcount', '0' ) + $iret );
	}
	wppa_log( 'Cron', 'Phase 4, time left = '.($wppa_endtime-time()) );

	// Re-animate crashed cronjobs
	wppa_log( 'Cron', '{b}wppa_cleanup{/b} reanimate cron.' );
	wppa_re_animate_cron();
	wppa_log( 'Cron', 'Phase 5, time left = '.($wppa_endtime-time()) );

	// Remove 'deleted' photos from system
	$dels = $wpdb->get_col( "SELECT id FROM $wpdb->wppa_photos WHERE album <= '-9' AND modified < " . ( time() - 3600 ) );
	if ( !empty( $dels ) ) foreach( $dels as $del ) {
		wppa_delete_photo( $del );
		wppa_log( 'Cron', 'Removed photo {b}' . $del . '{/b} from system' );
	}
	wppa_log( 'Cron', 'Phase 6, time left = '.($wppa_endtime-time()) );

	// Re-create permalink htaccess file
	wppa_log( 'Cron', '{b}wppa_cleanup{/b} creating pl htaccess.' );
	wppa_create_pl_htaccess();
	wppa_log( 'Cron', 'Phase 7, time left = '.($wppa_endtime-time()) );

	// Retry failed mails
	if ( wppa_opt( 'retry_mails' ) ) {

		$failed_mails = wppa_get_option( 'wppa_failed_mails' );
		if ( is_array( $failed_mails ) && count( $failed_mails ) ) {
			wppa_log( 'Cron', '{b}wppa_cleanup{/b} retrying failed mails.' );

			foreach( array_keys( $failed_mails ) as $key ) {

				$mail = $failed_mails[$key];
				$mess = $mail['message'] . '(retried mail)';

				// Retry
				if ( wp_mail( $mail['to'], $mail['subj'], $mess, $mail['headers'], $mail['att'] ) ) {

					wppa_log( 'Eml', 'Retried mail to ' . $mail['to'] . ' succeeded.' );

					// Set counter to 0
					$failed_mails[$key]['retry'] = '0';
				}
				else {

					// Decrease retry counter
					$failed_mails[$key]['retry']--;
					wppa_log( 'Eml', 'Retried mail to ' . $mail['to'] . ' failed. Tries to go = ' . $failed_mails[$key]['retry'] );

					// If no tries left, add to permanently failed
					if ( $failed_mails[$key]['retry'] < '1' ) {
						$perm_fail = get_option( 'wppa_perm_failed_mails', array() );
						$perm_fail[] = $failed_mails[$key];
						update_option( 'wppa_perm_failed_mails', $perm_fail );
					}
				}
			}

			// Cleanup
			foreach( array_keys( $failed_mails ) as $key ) {
				if ( $failed_mails[$key]['retry'] < '1' ) {
					unset( $failed_mails[$key] );
				}
			}
		}

		// Store updated failed mails
		update_option( 'wppa_failed_mails', $failed_mails );
	}
	wppa_log( 'Cron', 'Phase 8, time left = '.($wppa_endtime-time()) );


	/*
	// Cleanup iptc and exif
	wppa_iptc_clean_garbage();
	wppa_exif_clean_garbage();
	*/

	// Cleanup qr cache
	if ( wppa_is_dir( WPPA_UPLOAD_PATH . '/qr' ) ) {
		$qrs = wppa_glob( WPPA_UPLOAD_PATH . '/qr/*.svg' );
		if ( ! empty( $qrs ) ) {
			$count = count( $qrs );
			if ( $count > 250 ) {
				foreach( $qrs as $qr ) @ unlink( $qr );
				wppa_log( 'Cron', $count . ' qr cache files removed' );
			}
		}
	}
	wppa_log( 'Cron', 'Phase 9, time left = '.($wppa_endtime-time()) );

	// Add url-sanitized names to new albums
	$albs = $wpdb->get_results( "SELECT id, name FROM $wpdb->wppa_albums WHERE sname = ''", ARRAY_A );
	if ( ! empty( $albs ) ) {
		foreach( $albs as $alb ) {
			wppa_update_album( array( 'id' => $alb['id'], 'sname' => wppa_sanitize_album_photo_name( $alb['name'] ) ) );
			wppa_log( 'dbg', 'Set sname from ' . $alb['name'] . ' to ' . wppa_sanitize_album_photo_name( $alb['name'] ) . ' for album ' . $alb['id'] );
			if ( wppa_is_time_up() ) {
				wppa_log( 'Cron', 'Reschedule cleanup' );
				wppa_schedule_cleanup( true );
				wppa_exit();
			}
		}
	}
	wppa_log( 'Cron', 'Phase 10, time left = '.($wppa_endtime-time()) );

	// Add url-sanitized names to new photos
	$photos = $wpdb->get_results( "SELECT id, name FROM $wpdb->wppa_photos WHERE sname = '' AND name <> '' LIMIT 10000", ARRAY_A );
	if ( ! empty( $photos ) ) {
		foreach( $photos as $photo ) {
			wppa_update_photo( array( 'id' => $photo['id'], 'sname' => wppa_sanitize_album_photo_name( $photo['name'] ) ) );
			wppa_log( 'dbg', 'Set sname from ' . $photo['name'] . ' to ' . wppa_sanitize_album_photo_name( $photo['name'] ) . ' for photo ' . $photo['id'] );
			if ( wppa_is_time_up() ) {
				wppa_log( 'Cron', 'Reschedule cleanup' );
				wppa_schedule_cleanup( true );
				wppa_exit();
			}
		}
	}
	wppa_log( 'Cron', 'Phase 11, time left = '.($wppa_endtime-time()) );

	// Cleanup tempfiles
	wppa_delete_obsolete_tempfiles();
	wppa_log( 'Cron', 'Phase 12, time left = '.($wppa_endtime-time()) );

	// Cleanup unused depot dirs
	$root 	= is_user_logged_in() ? dirname( WPPA_DEPOT_PATH ) : WPPA_DEPOT_PATH;
	$depot 	= dir( $root );
	if ( substr( $root, -10 ) != 'wppa-depot' ) $depot = false; // Just to be sure we are in the right dir
	if ( $depot ) {
		while ( false !== ( $entry = $depot->read() ) && ! wppa_is_time_up() ) {
			if ( $entry != '.' && $entry != '..' && is_dir( $root . '/' . $entry ) ) {
				$user = get_user_by( 'login', $entry );
				if ( ! $user || ! user_can( $user, 'wppa_import' ) ) {
					wppa_rmdir( $root . '/' . $entry );
					wppa_log( 'Fso', 'Removed unused depot dir for' . ( $user ? '': ' non existent' ) . ' user {b}' . $entry . '{/b}' );
				}
			}
		}
	}
	else {
		wppa_log( 'err', 'No depot found ' . $root );
	}

	wppa_log( 'Cron', 'Phase 13, time left = '.($wppa_endtime-time()) );

	// Cleanup empty source dirs
	$dirs = wppa_glob( wppa_opt( 'source_dir' ) . '/*', WPPA_ONLYDIRS );
	if ( $dirs ) foreach( $dirs as $dir ) {
		wppa_rmdir( $dir, true ); // when empty
	}

	wppa_log( 'Cron', 'Phase 14, time left = '.($wppa_endtime-time()) );

	// Clear widget cache
	wppa_remove_widget_cache_path();

	// Done?
	wppa_log( 'Cron', 'Phase 15, time left = '.($wppa_endtime-time()) );

	wppa_log( 'Cron', '{b}wppa_cleanup{/b} completed.' );

	$outbuf = ob_get_clean();
	if ( $outbuf ) {
		wppa_log( 'dbg', 'Cron unexpected output: ' . $outbuf );
	}
}

// Selectively clear caches
add_action( 'wppa_clear_cache', 'wppa_do_clear_cache' );

function wppa_schedule_clear_cache( $time = 10 ) {
static $done;

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	// If a cron job is scheduled cancel the request
	$next_scheduled = wp_next_scheduled( 'wppa_clear_cache' );

	if ( $next_scheduled ) {
		return;
	}
	/*
	if ( $time == 10 && is_numeric( $next_scheduled ) && $next_scheduled > ( time() + $time ) ) {

		wp_unschedule_event( $next_scheduled, 'wppa_clear_cache' );
		$did_unschedule = true;
	}
	else {
		$did_unschedule = false;
	}
	*/

	// Schedule new event
	if ( ! wp_next_scheduled( 'wppa_clear_cache' ) && ! $done ) {

		wp_schedule_single_event( time() + $time, 'wppa_clear_cache' );
		$done = true;

		wppa_log( 'Cron', '{b}wppa_clear_cache{/b} scheduled for run in ' . $time . ' sec.' );
	}
}

// call the actusl cache deleting proc, and indicate only delete cache files with text 'data-wppa="yes"'
function wppa_do_clear_cache() {

	$relroot = trim( wppa_opt( 'cache_root' ), '/' );
	if ( ! $relroot ) {
		$relroot = 'cache';
	}
	$root = WPPA_CONTENT_PATH . '/' . $relroot;
	if ( wppa_is_dir( $root ) ) {

		wppa_log( 'Cron', '{b}wppa_clear_cache{/b} started.' );
		_wppa_do_clear_cache( $root );
		wppa_log( 'Cron', '{b}wppa_clear_cache{/b} completed.' );
	}
}
function _wppa_do_clear_cache( $dir ) {
static $did_tempfiles;

	$needle = 'data-wppa="yes"';
	$fsos = wppa_glob( $dir . '/*' );
	if ( is_array( $fsos ) ) foreach ( $fsos as $fso ) {

		// If it is in ../wppa-widget/ or in ../wppa-shortcode/, wait until older than 2 hours
		$is_widget_cache 	= ( strpos( $fso, '/wppa-widget/' ) != false );
		$is_shortcode_cache = ( strpos( $fso, '/wppa-shortcode/' ) != false );
		if ( $is_widget_cache || $is_shortcode_cache ) {
			if ( wppa_filetime( $fso ) < ( time() - 24 * 3600 ) ) {
				unlink ( $fso );
				wppa_log( 'fso', 'Cron removed obsolete widget cachefile: {b}' . basename( $fso ) . '{/b}' );
			}
		}
		else {
			$name = basename( $fso );
			if ( $name != '.' && $name != '..' ) {
				if ( wppa_is_dir( $fso ) ) {
					_wppa_do_clear_cache( $fso );
				}
				else {
					$file = wppa_fopen( $fso, 'rb' );
					if ( $file ) {
						$size = filesize( $fso );
						if ( $size ) {
							$haystack = fread( $file, $size );
							if ( strpos( $haystack, $needle ) !== false ) {
								fclose( $file );
								unlink( $fso );
								wppa_log( 'fso', 'Cron removed cachefile: {b}' . str_replace( WPPA_CONTENT_PATH, '', $fso ) . '{/b}' );
							}
							else {
								fclose( $file );
							}
						}
						else {
							fclose( $file );
						}
					}
				}
			}
		}
	}

	// Also delete tempfiles
	if ( ! $did_tempfiles ) {
		wppa_delete_obsolete_tempfiles();
		$did_tempfiles = true;
	}
}

// Activate treecount update proc
add_action( 'wppa_update_treecounts', 'wppa_do_update_treecounts' );

function wppa_schedule_treecount_update() {

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	// Schedule cron job
	if ( ! wp_next_scheduled( 'wppa_update_treecounts' ) ) {
		$time = 10;
		wp_schedule_single_event( time() + $time, 'wppa_update_treecounts' );
		wppa_log( 'Cron', '{b}wppa_update_treecounts{/b} scheduled for run in ' . $time . ' sec.' );
	}
}

function wppa_do_update_treecounts() {
global $wpdb;

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	wppa_log( 'Cron', '{b}wppa_update_treecounts{/b} started.' );

	$start = time();

	$albs = $wpdb->get_col( "SELECT id FROM $wpdb->wppa_albums WHERE a_parent < '1' ORDER BY id" );

	foreach( $albs as $alb ) {
		$treecounts = wppa_get_treecounts_a( $alb );
		if ( $treecounts['needupdate'] ) {
			wppa_verify_treecounts_a( $alb );
			wppa_log( 'Cron', 'Cron fixed treecounts for ' . $alb );
		}
		if ( time() > $start + 15 ) {
			wppa_schedule_treecount_update();
			exit();
		}
	}

	wppa_log( 'Cron', '{b}wppa_update_treecounts{/b} completed.' );

	wppa_schedule_clear_cache( 600 );
}

function wppa_re_animate_cron() {
global $wppa_cron_maintenance_slugs;

	// Are we temp disbled?
	if ( wppa_switch( 'maint_ignore_cron' ) ) {
		return;
	}

	foreach ( $wppa_cron_maintenance_slugs as $slug ) {
		if ( wppa_is_maintenance_cron_job_crashed( $slug ) ) {
			$last = wppa_get_option( $slug . '_last' );
			update_option( $slug . '_last', $last + 1 );
			wppa_schedule_maintenance_proc( $slug );
			wppa_log( 'Cron', '{b}' . $slug . '{/b} re-animated at item {b}#' . $last . '{/b}' );
		}
	}
}