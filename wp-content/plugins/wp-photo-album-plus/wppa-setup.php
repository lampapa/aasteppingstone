<?php
/* wppa-setup.php
* Package: wp-photo-album-plus
*
* Contains all the setup stuff
* Version 7.6.01
*
*/

if ( ! defined( 'ABSPATH' ) ) die( "Can't load this file directly" );

/* SETUP */
// It used to be: register_activation_hook(WPPA_FILE, 'wppa_setup');
// The activation hook is useless since wp does no longer call this hook after upgrade of the plugin
// this routine is now called at action init, so also after initial install
// Additionally it can now output messages about success or failure
// Just for people that rely on the healing effect of de-activating and re-activating a plugin
// we still do a setup on activation by faking that we are not up to rev, and so invoking
// the setup on the first init event. This has the advantage that we can display messages
// instead of characters of unexpected output.
// register_activation_hook(WPPA_FILE, 'wppa_activate_plugin'); is in wppa.php
function wppa_activate_plugin() {
	$old_rev = wppa_get_option( 'wppa_revision', '100' );
	$new_rev = $old_rev - '0.01';
	wppa_update_option( 'wppa_revision', $new_rev );
}

// Set force to true to re-run it even when on rev (happens in wppa-settings.php)
// Force will NOT redefine constants
function wppa_setup( $force = false ) {
global $silent;
global $wpdb;
global $wppa_revno;
global $current_user;
global $wppa_error;

	$old_rev = wppa_get_option( 'wppa_revision', '100' );

	if ( $old_rev == $wppa_revno && ! $force ) return; // Nothing to do here

	wppa_clear_cache( $force );

	$wppa_error = false;	// Init no error

	$create_albums = "CREATE TABLE $wpdb->wppa_albums (
					id bigint(20) NOT NULL,
					name text NOT NULL,
					description text NOT NULL,
					a_order smallint(5) NOT NULL,
					main_photo bigint(20) NOT NULL,
					a_parent bigint(20) NOT NULL,
					p_order_by smallint(5) NOT NULL,
					cover_linktype tinytext NOT NULL,
					cover_linkpage bigint(20) NOT NULL,
					owner text NOT NULL,
					timestamp tinytext NOT NULL,
					modified tinytext NOT NULL,
					upload_limit tinytext NOT NULL,
					alt_thumbsize tinytext NOT NULL,
					default_tags tinytext NOT NULL,
					cover_type tinytext NOT NULL,
					suba_order_by tinytext NOT NULL,
					views bigint(20) NOT NULL default '0',
					cats text NOT NULL,
					scheduledtm tinytext NOT NULL,
					custom longtext NOT NULL,
					crypt tinytext NOT NULL,
					treecounts text NOT NULL,
					wmfile tinytext NOT NULL,
					wmpos tinytext NOT NULL,
					indexdtm tinytext NOT NULL,
					sname text NOT NULL,
					zoomable tinytext NOT NULL,
					PRIMARY KEY  (id)
					) DEFAULT CHARACTER SET utf8;";

	$create_photos = "CREATE TABLE $wpdb->wppa_photos (
					id bigint(20) NOT NULL,
					album bigint(20) NOT NULL,
					ext tinytext NOT NULL,
					name text NOT NULL,
					description longtext NOT NULL,
					p_order smallint(5) NOT NULL,
					mean_rating tinytext NOT NULL,
					linkurl text NOT NULL,
					linktitle text NOT NULL,
					linktarget tinytext NOT NULL,
					owner text NOT NULL,
					timestamp tinytext NOT NULL,
					status tinytext NOT NULL,
					rating_count bigint(20) NOT NULL default '0',
					tags text NOT NULL,
					alt tinytext NOT NULL,
					filename tinytext NOT NULL,
					modified tinytext NOT NULL,
					location tinytext NOT NULL,
					views bigint(20) NOT NULL default '0',
					clicks bigint(20) NOT NULL default '0',
					page_id bigint(20) NOT NULL default '0',
					exifdtm tinytext NOT NULL,
					videox smallint(5) NOT NULL default '0',
					videoy smallint(5) NOT NULL default '0',
					thumbx smallint(5) NOT NULL default '0',
					thumby smallint(5) NOT NULL default '0',
					photox smallint(5) NOT NULL default '0',
					photoy smallint(5) NOT NULL default '0',
					scheduledtm tinytext NOT NULL,
					scheduledel tinytext NOT NULL,
					custom longtext NOT NULL,
					stereo smallint NOT NULL default '0',
					crypt tinytext NOT NULL,
					magickstack text NOT NULL,
					indexdtm tinytext NOT NULL,
					panorama smallint(5) NOT NULL default '0',
					angle smallint(5) NOT NULL default '0',
					sname text NOT NULL,
					dlcount bigint(20) NOT NULL default '0',
					thumblock smallint(5) default '0',
					duration tinytext NOT NULL,
					PRIMARY KEY  (id),
					KEY albumkey (album),
					KEY statuskey (status(6))
					) DEFAULT CHARACTER SET utf8;";

	$create_rating = "CREATE TABLE " . WPPA_RATING . " (
					id bigint(20) NOT NULL,
					timestamp tinytext NOT NULL,
					photo bigint(20) NOT NULL,
					value smallint(5) NOT NULL,
					user text NOT NULL,
					userid int NOT NULL,
					ip tinytext NOT NULL,
					status tinytext NOT NULL,
					PRIMARY KEY  (id),
					KEY photokey (photo)
					) DEFAULT CHARACTER SET utf8;";

	$create_comments = "CREATE TABLE " . WPPA_COMMENTS . " (
					id bigint(20) NOT NULL,
					timestamp tinytext NOT NULL,
					photo bigint(20) NOT NULL,
					user text NOT NULL,
					userid int NOT NULL,
					ip tinytext NOT NULL,
					email text NOT NULL,
					comment text NOT NULL,
					status tinytext NOT NULL,
					PRIMARY KEY  (id),
					KEY photokey (photo)
					) DEFAULT CHARACTER SET utf8;";

	$create_iptc = "CREATE TABLE " . WPPA_IPTC . " (
					id bigint(20) NOT NULL AUTO_INCREMENT,
					photo bigint(20) NOT NULL,
					tag tinytext NOT NULL,
					description text NOT NULL,
					status tinytext NOT NULL,
					PRIMARY KEY  (id),
					KEY photokey (photo)
					) DEFAULT CHARACTER SET utf8;";

	$create_exif = "CREATE TABLE " . WPPA_EXIF . " (
					id bigint(20) NOT NULL AUTO_INCREMENT,
					photo bigint(20) NOT NULL,
					tag tinytext NOT NULL,
					description text NOT NULL,
					status tinytext NOT NULL,
					f_description text NOT NULL,
					brand tinytext NOT NULL,
					PRIMARY KEY  (id),
					KEY photokey (photo)
					) DEFAULT CHARACTER SET utf8;";

	$create_index = "CREATE TABLE " . WPPA_INDEX . " (
					id bigint(20) NOT NULL AUTO_INCREMENT,
					slug tinytext NOT NULL,
					albums text NOT NULL,
					photos mediumtext NOT NULL,
					PRIMARY KEY  (id),
					KEY slugkey (slug(20))
					) DEFAULT CHARACTER SET utf8;";

	$create_session = "CREATE TABLE " . WPPA_SESSION . " (
					id bigint(20) NOT NULL AUTO_INCREMENT,
					session tinytext NOT NULL,
					timestamp tinytext NOT NULL,
					user tinytext NOT NULL,
					ip tinytext NOT NULL,
					status tinytext NOT NULL,
					data text NOT NULL,
					count bigint(20) NOT NULL default '0',
					PRIMARY KEY  (id),
					KEY sessionkey (session(20))
					) DEFAULT CHARACTER SET utf8;";

	require_once ABSPATH . 'wp-admin/includes/upgrade.php';

	// Create or update db tables
	$tn = array( WPPA_ALBUMS, WPPA_PHOTOS, WPPA_RATING, WPPA_COMMENTS, WPPA_IPTC, WPPA_EXIF, WPPA_INDEX, WPPA_SESSION );
	$tc = array( $create_albums, $create_photos, $create_rating, $create_comments, $create_iptc, $create_exif, $create_index, $create_session );
	$idx = 0;
	while ( $idx < 8 ) {
		dbDelta( $tc[$idx] );
		$idx++;
	}

	// Convert any changed and remove obsolete setting options
	if ( $old_rev > '100' ) {	// On update only
		if ( $old_rev <= '402' ) {
			wppa_convert_setting('wppa_coverphoto_left', 'no', 'wppa_coverphoto_pos', 'right');
			wppa_convert_setting('wppa_coverphoto_left', 'yes', 'wppa_coverphoto_pos', 'left');
		}
		if ( $old_rev <= '440' ) {
			wppa_convert_setting('wppa_fadein_after_fadeout', 'yes', 'wppa_animation_type', 'fadeafter');
			wppa_convert_setting('wppa_fadein_after_fadeout', 'no', 'wppa_animation_type', 'fadeover');
		}
		if ( $old_rev <= '450' ) {
			wppa_remove_setting('wppa_fadein_after_fadeout');
			wppa_copy_setting('wppa_show_bbb', 'wppa_show_bbb_widget');
			wppa_convert_setting('wppa_comment_use_gravatar', 'yes', 'wppa_comment_gravatar', 'mm');
			wppa_convert_setting('wppa_comment_use_gravatar', 'no', 'wppa_comment_gravatar', 'none');
			wppa_remove_setting('wppa_comment_use_gravatar');
			wppa_revalue_setting('wppa_start_slide', 'yes', 'run');
			wppa_revalue_setting('wppa_start_slide', 'no', 'still');
			wppa_rename_setting('wppa_accesslevel', 'wppa_accesslevel_admin');
			wppa_remove_setting('wppa_charset');
			wppa_remove_setting('wppa_chmod');
			wppa_remove_setting('wppa_coverphoto_left');
			wppa_remove_setting('wppa_2col_treshold');
			wppa_remove_setting('wppa_album_admin_autosave');
			wppa_remove_setting('wppa_doublethevotes');
			wppa_remove_setting('wppa_halvethevotes');
			wppa_remove_setting('wppa_lightbox_overlaycolor');
			wppa_remove_setting('wppa_lightbox_overlayopacity');
			wppa_remove_setting('wppa_multisite');
			wppa_remove_setting('wppa_set_access_by');
			wppa_remove_setting('wppa_accesslevel_admin');
			wppa_remove_setting('wppa_accesslevel_upload');
			wppa_remove_setting('wppa_accesslevel_sidebar');
		}
		if ( $old_rev <= '452') {
			wppa_copy_setting('wppa_fontfamily_numbar', 'wppa_fontfamily_numbar_active');
			wppa_copy_setting('wppa_fontsize_numbar', 'wppa_fontsize_numbar_active');
			wppa_copy_setting('wppa_fontcolor_numbar', 'wppa_fontcolor_numbar_active');
			wppa_copy_setting('wppa_fontweight_numbar', 'wppa_fontweight_numbar_active');
		}
		if ( $old_rev <= '455') {	// rating_count added to WPPA_PHOTOS
			$phs = $wpdb->get_results( "SELECT id FROM $wpdb->wppa_photos", ARRAY_A );
			if ($phs) foreach ($phs as $ph) {
				$cnt = $wpdb->get_var($wpdb->prepare('SELECT COUNT(*) FROM '.WPPA_RATING.' WHERE photo = %s', $ph['id']));
				$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->wppa_photos
											   SET rating_count = %d
											   WHERE id = %d", $cnt, $ph['id'] ) );
			}
		}
		if ( $old_rev < '470' ) {	// single photo re-impl. has its own links, clone from slideshow
			wppa_copy_setting('wppa_slideshow_linktype', 'wppa_sphoto_linktype');
			wppa_copy_setting('wppa_slideshow_blank', 'wppa_sphoto_blank');
			wppa_copy_setting('wppa_slideshow_overrule', 'wppa_sphoto_overrule');
		}
		if ( $old_rev <= '474' ) {	// Convert album and photo descriptions to contain html instead of htmlspecialchars. Allowing html is assumed, if not permitted, wppa_html will convert to specialcars.
			// html
			$at = 0; $ah = 0; $pt = 0; $ph = 0;
			$albs = $wpdb->get_results('SELECT id, description FROM '.WPPA_ALBUMS, ARRAY_A);
			if ($albs) {
				foreach($albs as $alb) {
					$at++;
					if (html_entity_decode($alb['description']) != $alb['description']) {
						$wpdb->query($wpdb->prepare('UPDATE '.WPPA_ALBUMS.' SET description = %s WHERE id = %s', html_entity_decode($alb['description']), $alb['id']));
						$ah++;
					}
				}
			}
			$phots = $wpdb->get_results( "SELECT id, description FROM $wpdb->wppa_photos", ARRAY_A);
			if ($phots) {
				foreach($phots as $phot) {
					$pt++;
					if (html_entity_decode($phot['description']) != $phot['description']) {
						$wpdb->query($wpdb->prepare( "UPDATE $wpdb->wppa_photos
													  SET description = %s
													  WHERE id = %d", html_entity_decode( $phot['description'] ), $phot['id'] ) );
						$ph++;
					}
				}
			}
			if ( WPPA_DEBUG ) if ($ah || $ph) wppa_ok_message($ah.' out of '.$at.' albums and '.$ph.' out of '.$pt.' photos html converted');
		}
		if ( $old_rev <= '482' ) {	// Share box added
			$so = wppa_get_option('wppa_slide_order', '0,1,2,3,4,5,6,7,8,9');
			if ( strlen($so) == '19' ) {
				wppa_update_option('wppa_slide_order', $so.',10');
			}
			$so = wppa_get_option('wppa_slide_order_split', '0,1,2,3,4,5,6,7,8,9,10');
			if ( strlen($so) == '22' ) {
				wppa_update_option('wppa_slide_order_split', $so.',11');
			}
			wppa_remove_setting('wppa_sharetype');
			wppa_copy_setting('wppa_bgcolor_namedesc', 'wppa_bgcolor_share');
			wppa_copy_setting('wppa_bcolor_namedesc', 'wppa_bcolor_share');

		}
		if ( $old_rev <= '4811' ) {
			wppa_rename_setting('wppa_comment_count', 'wppa_comten_count');
			wppa_rename_setting('wppa_comment_size', 'wppa_comten_size');
		}
		if ( $old_rev <= '4910' ) {
			wppa_copy_setting('wppa_show_bread', 'wppa_show_bread_posts');
			wppa_copy_setting('wppa_show_bread', 'wppa_show_bread_pages');
			wppa_remove_setting('wppa_show_bread');
		}
		if ( $old_rev <= '5000' ) {
			wppa_remove_setting('wppa_autoclean');
		}
		if ( $old_rev <= '5010' ) {
			wppa_copy_setting('wppa_apply_newphoto_desc', 'wppa_apply_newphoto_desc_user');
		}
		if ( $old_rev <= '5107' ) {
			delete_option('wppa_taglist'); 	// Forces recreation
		}
		if ( $old_rev <= '5205' ) {
			if ( wppa_get_option('wppa_list_albums_desc', 'nil') == 'yes' ) {
				$value = wppa_get_option('wppa_list_albums_by', '0') * '-1';
				wppa_update_option('wppa_list_albums_by', $value);
				wppa_remove_setting('wppa_list_albums_desc');
			}
			if ( wppa_get_option('wppa_list_photos_desc', 'nil') == 'yes' ) {
				$value = wppa_get_option('wppa_list_photos_by', '0') * '-1';
				wppa_update_option('wppa_list_photos_by', $value);
				wppa_remove_setting('wppa_list_photos_desc');
			}
		}

		if ( $old_rev <= '5207' ) {
			if ( wppa_get_option( 'wppa_strip_file_ext', 'nil' ) == 'yes' ) {
				wppa_update_option( 'wppa_newphoto_name_method', 'noext' );
				delete_option( 'wppa_strip_file_ext' );
			}
		}

		if ( $old_rev <= '5307' ) {
			$wpdb->query( "TRUNCATE TABLE ".WPPA_SESSION."" );
		}

		if ( $old_rev <= '5308' ) {
			wppa_invalidate_treecounts();
		}

		if ( $old_rev <= '5410' ) {
			wppa_copy_setting( 'wppa_widget_width', 'wppa_potd_widget_width' );
			wppa_flush_upldr_cache( 'all' );	// New format
		}

		if ( $old_rev == '5421' || $old_rev == '5420.99' ) { 							// The rev where the bug was
			if ( $wppa_revno >= '5422' ) {												// The rev where we fix it
				if ( wppa_get_option( 'wppa_rating_on', 'no' ) == 'yes' ) { 					// Only if rating used
					if ( wppa_get_option( 'wppa_ajax_non_admin', 'yes' ) == 'no' ) { 		// Only if backend ajax
						update_option( 'wppa_rerate_status', __('Required', 'wp-photo-album-plus') ); 	// Make sure they see the message
					}
				}
			}
		}

		if ( $old_rev <= '5500' ) {
			wppa_create_pl_htaccess( wppa_get_option( 'wppa_pl_dirname', 'wppa-pl' ) );		// Remake due to fix in wppa_sanitize_file_name()
		}

		if ( $old_rev <= '6103' ) {
			wppa_copy_setting( 'wppa_owner_only', 'wppa_upload_owner_only' );
		}

		if ( $old_rev <= '6305' ) {
			if ( wppa_get_option( 'wppa_comment_captcha' ) == 'no' ) {
				update_option( 'wppa_comment_captcha', 'none' );
			}
			if ( wppa_get_option( 'wppa_comment_captcha' ) == 'yes' ) {
				update_option( 'wppa_comment_captcha', 'all' );
			}
		}

		if ( $old_rev <= '6310' ) {
			$wpdb->query("UPDATE $wpdb->wppa_photos SET timestamp = '0' WHERE timestamp = ''");
			$wpdb->query("UPDATE $wpdb->wppa_photos SET modified = timestamp WHERE modified = '' OR modified = '0'");
		}

		if ( $old_rev <= '6312' ) {
			$wpdb->query("UPDATE $wpdb->wppa_albums SET timestamp = '0' WHERE timestamp = ''");
			$wpdb->query("UPDATE $wpdb->wppa_albums SET modified = timestamp WHERE modified = '' OR modified = '0'");
			wppa_copy_setting( 'wppa_wppa_set_shortcodes', 'wppa_set_shortcodes' );
			wppa_remove_setting( 'wppa_wppa_set_shortcodes' );
			wppa_copy_setting( 'wppa_max_album_newtime', 'wppa_max_album_modtime' );
			wppa_copy_setting( 'wppa_max_photo_newtime', 'wppa_max_photo_modtime' );
		}

		if ( $old_rev <= '6316' ) {
			wppa_remove_setting( 'wppa_start_symbol_url' );
			wppa_remove_setting( 'wppa_pause_symbol_url' );
			wppa_remove_setting( 'wppa_stop_symbol_url' );
		}

		if ( $old_rev <= '6319' ) {
			if ( wppa_get_option( 'wppa_cre_uploads_htaccess', 'no' ) == 'no' ) {
				update_option( 'wppa_cre_uploads_htaccess', 'remove' );
			}
			if ( wppa_get_option( 'wppa_cre_uploads_htaccess', 'no' ) == 'yes' ) {
				update_option( 'wppa_cre_uploads_htaccess', 'grant' );
			}
		}

		if ( $old_rev <= '6403' ) {
			wppa_copy_setting( 'wppa_thumbsize', 'wppa_film_thumbsize' );
		}

		if ( $old_rev <= '6408' ) {
			if ( wppa_get_option( 'wppa_comment_email_required', 'yes' ) ) {
				update_option( 'wppa_comment_email_required', 'required', false );
			}
			else {
				update_option( 'wppa_comment_email_required', 'none', false );
			}
		}

		if ( $old_rev <= '6411' ) {
			$old = wppa_get_option( 'wppa_upload_edit', 'no' );
			if ( $old == 'no' ) {
				update_option( 'wppa_upload_edit', '-none-', false );
			}
			if ( $old == 'yes' ) {
				update_option( 'wppa_upload_edit', 'classic', false );
			}
		}

		if ( $old_rev <= '6414' ) {
			if ( wppa_get_option( 'wppa_upload_edit', 'no' ) != 'no' ) {
				update_option( 'wppa_upload_delete', 'yes' );
			}
			if ( wppa_get_option( 'wppa_upload_edit_users' ) == 'equalname' ) {
				update_option( 'wppa_upload_edit_users', 'owner' );
			}
		}

		if ( $old_rev <= '6417' ) {
			$logfile = ABSPATH . 'wp-content/wppa-depot/admin/error.log';
			if ( is_file( $logfile ) ) {
				unlink( $logfile );
			}
			update_option( 'wppa_album_crypt_9', wppa_get_unique_album_crypt() );
		}

		if ( $old_rev <= '6504' ) {
			wppa_rename_setting( 'wppa_widgettitle', 			'wppa_potd_title' );
			wppa_rename_setting( 'wppa_widget_linkurl', 		'wppa_potd_linkurl' );
			wppa_rename_setting( 'wppa_widget_linktitle', 		'wppa_potd_linktitle' );
			wppa_rename_setting( 'wppa_widget_subtitle', 		'wppa_potd_subtitle' );
			wppa_rename_setting( 'wppa_widget_counter', 		'wppa_potd_counter' );
			wppa_rename_setting( 'wppa_widget_album', 			'wppa_potd_album' );
			wppa_rename_setting( 'wppa_widget_status_filter', 	'wppa_potd_status_filter' );
			wppa_rename_setting( 'wppa_widget_method', 			'wppa_potd_method' );
			wppa_rename_setting( 'wppa_widget_period', 			'wppa_potd_period' );
		}

		if ( $old_rev <= '6600' ) {
			wppa_create_pl_htaccess( wppa_get_option( 'wppa_pl_dirname', 'wppa-pl' ) );		// Remake due to fix in wppa_create_pl_htaccess() and wppa_get_source_pl()
			if ( wppa_get_option( 'wppa_run_wpautop_on_desc' ) == 'yes' ) {
				wppa_update_option( 'wppa_wpautop_on_desc', 'wpautop' );
			}
			if ( wppa_get_option( 'wppa_run_wpautop_on_desc' ) == 'no' ) {
				wppa_update_option( 'wppa_wpautop_on_desc', 'nil' );
			}
		}

		if ( $old_rev <= '6601' ) {
			if ( wppa_get_option( 'wppa_bc_url', 'nil' ) != 'nil' ) {
				update_option( 'wppa_bc_url', str_replace( '/images/', '/img/', wppa_get_option( 'wppa_bc_url', 'nil' ) ) );
			}
		}

		if ( $old_rev <= '6602' ) {
			if ( wppa_get_option( 'wppa_show_treecount' ) == 'yes' ) {
				wppa_update_option( 'wppa_show_treecount', 'detail' );
			}
			if ( wppa_get_option( 'wppa_show_treecount' ) == 'no' ) {
				wppa_update_option( 'wppa_show_treecount', '-none-' );
			}
			if ( wppa_get_option( 'wppa_count_on_title' ) == 'yes' ) {
				wppa_update_option( 'wppa_count_on_title', 'self' );
			}
			if ( wppa_get_option( 'wppa_count_on_title' ) == 'no' ) {
				wppa_update_option( 'wppa_count_on_title', '-none-' );
			}

		}

		if ( $old_rev <= '6606' ) {
			if ( wppa_get_option( 'wppa_rating_dayly' ) == 'no' ) {
				wppa_update_option( 'wppa_rating_dayly', '0' );
			}
		}

		if ( $old_rev <= '6609' ) {
			wppa_schedule_treecount_update();
		}

		if ( $old_rev <= '6610' ) {
			if ( wppa_get_option( 'wppa_blog_it' ) == 'yes' ) {
				wppa_update_option( 'wppa_blog_it', 'optional' );
			}
			if ( wppa_get_option( 'wppa_blog_it' ) == 'no' ) {
				wppa_update_option( 'wppa_blog_it', '-none-' );
			}
		}

		if ( $old_rev <= '6611' ) {
			delete_option( 'wppa_cached_options' );
			delete_option( 'wppa_md5_options' );
			@ $wpdb->query( "UPDATE " . $wpdb->options . " SET autoload = 'yes' WHERE option_name LIKE 'wppa_%'");
			if ( wppa_get_option( 'wppa_fe_alert' ) == 'no' ) {
				update_option( 'wppa_fe_alert', '-none-' );
			}
			if ( wppa_get_option( 'wppa_fe_alert' ) == 'yes' ) {
				update_option( 'wppa_fe_alert', 'all' );
			}
		}

		if ( $old_rev <= '6618' ) {
			wppa_schedule_maintenance_proc( 'wppa_remake_index_albums' );
			wppa_schedule_maintenance_proc( 'wppa_remake_index_photos' );
		}

		if ( $old_rev <= '6626' ) {
			wppa_rename_setting( 'wppa_upload_fronend_maxsize', 'wppa_upload_frontend_maxsize' );	// Fix typo
		}

		if ( $old_rev <= '6628' ) {
			if ( wppa_get_option( 'wppa_gpx_implementation' ) == 'wppa-plus-embedded' ) {
				update_option( 'wppa_load_map_api', 'yes' );
			}
			if ( wppa_get_option( 'wppa_gpx_implementation' ) == 'google-maps-gpx-viewer' ) {
				update_option( 'wppa_gpx_implementation', 'external-plugin' );
			}
		}

		if ( $old_rev <= '6630' ) {
			if ( wppa_get_option( 'wppa_upload_edit' ) == 'none' ) {
				update_option( 'wppa_upload_edit', '-none-' );
			}
		}

		if ( $old_rev <= '6800' ) {
			$wpdb->query( "ALTER TABLE $wpdb->wppa_iptc MODIFY id bigint(20) NOT NULL AUTO_INCREMENT" );
			$wpdb->query( "ALTER TABLE $wpdb->wppa_exif MODIFY id bigint(20) NOT NULL AUTO_INCREMENT" );
			$wpdb->query( "ALTER TABLE $wpdb->wppa_index MODIFY id bigint(20) NOT NULL AUTO_INCREMENT" );
			delete_option( 'wppa_' . WPPA_IPTC . '_lastkey' );
			delete_option( 'wppa_' . WPPA_EXIF . '_lastkey' );
			delete_option( 'wppa_' . WPPA_INDEX . '_lastkey' );

			//	wppa_schedule_maintenance_proc( 'wppa_format_exif' );
		}

		// Fix exiflables that were undefined so far but have a known description by now
		if ( $old_rev <= '6801' ) {
			if ( function_exists( 'exif_tagname' ) && function_exists( 'exif_read_data' ) ) {
				$exif_labels = $wpdb->get_results( "SELECT * FROM $wpdb->wppa_exif WHERE photo = 0 AND description LIKE 'UndefinedTag%'", ARRAY_A );
				if ( ! empty( $exif_labels ) ) foreach( $exif_labels as $label ) {
					$newdesc = wppa_exif_tagname( $label['tag'] );
					if ( $newdesc != $label['description'] ) {
						$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->wppa_exif SET description = %s WHERE photo = 0 AND tag = %s", $newdesc, $label['tag'] ) );
						wppa_log( 'obs', 'There is a new desc for '.$label['tag'].' being: '.$newdesc );
					}
				}
				wppa_schedule_maintenance_proc( 'wppa_recup' );
			}
		}

		if ( $old_rev <= '6915' ) {
			if ( ! wppa_get_option( 'wppa_album_admin_pagesize', '0' ) ) {
				update_option( 'wppa_album_admin_pagesize', '100' );
			}
			if ( ! wppa_get_option( 'wppa_photo_admin_pagesize', '0' ) ) {
				update_option( 'wppa_photo_admin_pagesize', '20' );
			}
		}

		if ( $old_rev <= '7001' ) {
			if ( wppa_get_option( 'wppa_use_pretty_links', 'nil' ) == 'no' ) {
				update_option( 'wppa_use_pretty_links', '-none-' );
			}
			if ( wppa_get_option( 'wppa_use_pretty_links', 'nil' ) == 'yes' ) {
				update_option( 'wppa_use_pretty_links', 'classic' );
			}

		}

		if ( $old_rev <= '7010' ) {
			$met = ini_get( 'max_execution_time' );
			$wppamet = wppa_get_option( 'wppa_max_execution_time', '30' );
			if ( ( $met > 5 ) && $wppamet > ( $met - '5' ) ) {
				update_option( 'wppa_max_execution_time', $met - '5' );
			}
			$wpdb->query( "UPDATE $wpdb->wppa_albums SET sname = ''" );
			$wpdb->query( "UPDATE $wpdb->wppa_photos SET sname = ''" );
			wppa_schedule_cleanup( true );
		}

		if ( $old_rev <= '7100' ) {

			// Fix audiostub.jpg, it had been overwritten
			$fromfile = WPPA_PATH . '/img/audiostub.jpg';
			$tofile = WPPA_UPLOAD_PATH . '/audiostub.jpg';
			@ wppa_copy( $fromfile, $tofile );

			// Copy documentstub.png, the default documentstub
			$fromfile = WPPA_PATH . '/img/documentstub.png';
			$tofile = WPPA_UPLOAD_PATH . '/documentstub.png';
			@ wppa_copy( $fromfile, $tofile );
		}

		if ( $old_rev <= '7102' ) {
			wppa_rename_setting( 'wppa_thumb_area_size', 'wppa_area_size' );
		}

		if ( $old_rev <= '7203' ) {
			$wpdb->query( "UPDATE $wpdb->wppa_albums SET timestamp = modified WHERE timestamp = ''" );
		}

		if ( $old_rev <= '7211' ) {

			// Fix bug because of different usernames in rating and comments
			if ( wppa_get_option( 'wppa_vote_needs_comment' ) == 'yes' || wppa_get_option( 'wppa_comment_need_vote' ) == 'yes' ) {
				$wpdb->query( "UPDATE $wpdb->wppa_rating SET status = 'publish' WHERE status = 'pending'" );
				$wpdb->query( "UPDATE $wpdb->wppa_comments SET status = 'approved' WHERE status = 'pending'" );
				wppa_schedule_maintenance_proc( 'wppa_rerate' );
			}
		}

		if ( $old_rev <= '7300' ) {
			wppa_schedule_maintenance_proc( 'wppa_fix_userids' );
		}

		if ( $old_rev <= '7400' ) {
			if ( get_option( 'wppa_enable_admins_choice', 'no' ) == 'yes' ) {
				update_option( 'wppa_admins_choice', 'admin' );
			}
		}
	}

	// Set Defaults
//	wppa_set_defaults();

	// Check required directories
	if ( ! wppa_check_dirs() ) $wppa_error = true;

	// Create .htaccess file in .../wp-content/uploads/wppa
	wppa_create_wppa_htaccess();

	// Copy factory supplied watermarks
	$frompath = WPPA_PATH . '/watermarks';
	$watermarks = wppa_glob($frompath . '/*.png');
	if ( is_array($watermarks) ) {
		foreach ($watermarks as $fromfile) {
			$tofile = WPPA_UPLOAD_PATH . '/watermarks/' . basename($fromfile);
			wppa_copy( $fromfile, $tofile );
		}
	}

	// Copy factory supplied watermark fonts
	$frompath = WPPA_PATH . '/fonts';
	$fonts = wppa_glob($frompath . '/*');
	if ( is_array($fonts) ) {
		foreach ($fonts as $fromfile) {
			if ( is_file ( $fromfile ) ) {
				$tofile = WPPA_UPLOAD_PATH . '/fonts/' . basename($fromfile);
				wppa_copy( $fromfile, $tofile );
			}
		}
	}

	// Copy audiostub.jpg, the default audiostub
	$fromfile = WPPA_PATH . '/img/audiostub.jpg';
	$tofile = WPPA_UPLOAD_PATH . '/audiostub.jpg';
	if ( ! wppa_is_file( $tofile ) ) {
		wppa_copy( $fromfile, $tofile );
	}

	// Copy documentstub.png, the default documentstub
	$fromfile = WPPA_PATH . '/img/documentstub.png';
	$tofile = WPPA_UPLOAD_PATH . '/documentstub.png';
	if ( ! wppa_is_file( $tofile ) ) {
		wppa_copy( $fromfile, $tofile );
	}

	// Copy factory supplied icons
	$fromfiles = array( 'Document-File.svg', 'Music-Note-1.svg', 'Film-Clapper.svg', 'Acrobat.jpg', 'Video-icon.png', 'Audio-icon.jpg' );
	foreach ( $fromfiles as $file ) {
		$from 	= WPPA_PATH . '/img/' . $file;
		$to  	= WPPA_UPLOAD_PATH . '/icons/' . $file;
		wppa_copy( $from, $to );
	}

	// Check if this update comes with a new wppa-theme.php and/or a new wppa-style.css
	// If so, produce message
	$key = '0';
	if ( $old_rev < '5400' ) {		// theme changed since...
		$usertheme = get_theme_root().'/'.wppa_get_option('template').'/wppa-theme.php';
		if ( is_file( $usertheme ) ) $key += '2';
	}
	if ( $old_rev < '5211' ) {		// css changed since...
		$userstyle = get_theme_root().'/'.wppa_get_option('stylesheet').'/wppa-style.css';
		if ( is_file( $userstyle ) ) {
			$key += '1';
		}
		else {
			$userstyle = get_theme_root().'/'.wppa_get_option('template').'/wppa-style.css';
			if ( is_file( $userstyle ) ) {
				$key += '1';
			}
		}
	}
	if ( $key ) {
		$msg = '<center>' . __('IMPORTANT UPGRADE NOTICE', 'wp-photo-album-plus') . '</center><br/>';
		if ($key == '1' || $key == '3') $msg .= '<br/>' . __('Please CHECK your customized WPPA-STYLE.CSS file against the newly supplied one. You may wish to add or modify some attributes. Be aware of the fact that most settings can now be set in the admin settings page.', 'wp-photo-album-plus');
		if ($key == '2' || $key == '3') $msg .= '<br/>' . __('Please REPLACE your customized WPPA-THEME.PHP file by the newly supplied one, or just remove it from your theme directory. You may modify it later if you wish. Your current customized version is NOT compatible with this version of the plugin software.', 'wp-photo-album-plus');
		wppa_ok_message($msg);
	}

	// Remove dynamic files
	$files = wppa_glob( WPPA_UPLOAD_PATH.'/dynamic/wppa-init.*.js' );
	if ( $files ) {
		foreach ( $files as $file ) {
			wppa_unlink ( $file );						// Will be auto re-created
		}
	}
	if ( is_file( WPPA_UPLOAD_PATH.'/dynamic/wppa-dynamic.css' ) ) {
		wppa_unlink ( WPPA_UPLOAD_PATH.'/dynamic/wppa-dynamic.css' );		// Will be auto re-created
	}

	// Make sure virtual album crypt exist
	$albs = array( '0', '1', '2', '3', '9' );
	foreach( $albs as $alb ) {
		if ( ! wppa_get_option( 'wppa_album_crypt_' . $alb ) ) {
			update_option( 'wppa_album_crypt_' . $alb, wppa_get_unique_album_crypt() );
		}
	}

	// Done!
	if ( ! $wppa_error ) {
		$old_rev = round($old_rev); // might be 0.01 off
		if ( $old_rev < $wppa_revno ) { 	// was a real upgrade,
			wppa_update_option('wppa_prevrev', $old_rev);	// Remember prev rev. For support purposes. They say they stay up to rev, but they come from stoneage...
		}
		wppa_update_option('wppa_revision', $wppa_revno);
		if ( WPPA_DEBUG ) {
			if ( is_multisite() ) {
				wppa_ok_message(sprintf(__('WPPA+ successfully updated in multi site mode to db version %s.', 'wp-photo-album-plus'), $wppa_revno));
			}
			else {
				wppa_ok_message(sprintf(__('WPPA+ successfully updated in single site mode to db version %s.', 'wp-photo-album-plus'), $wppa_revno));
			}
		}
	}
	else {
		if ( WPPA_DEBUG ) wppa_error_message(__('An error occurred during update', 'wp-photo-album-plus'));
	}

	wppa_schedule_cleanup();
}

// Function used during setup when existing settings are changed or removed
function wppa_convert_setting($oldname, $oldvalue, $newname, $newvalue) {
	if ( wppa_get_option($oldname, 'nil') == 'nil' ) return;	// no longer exists
	if ( wppa_get_option($oldname, 'nil') == $oldvalue ) wppa_update_option($newname, $newvalue);
}
function wppa_remove_setting($oldname) {
	if ( wppa_get_option($oldname, 'nil') != 'nil' ) delete_option($oldname);
}
function wppa_rename_setting($oldname, $newname) {
	if ( wppa_get_option($oldname, 'nil') == 'nil' ) return;	// no longer exists
	wppa_update_option($newname, wppa_get_option($oldname));
	delete_option($oldname);
}
function wppa_copy_setting($oldname, $newname) {
	if ( wppa_get_option($oldname, 'nil') == 'nil' ) return;	// no longer exists
	wppa_update_option($newname, wppa_get_option($oldname));
}
function wppa_revalue_setting($oldname, $oldvalue, $newvalue) {
	if ( wppa_get_option($oldname, 'nil') == $oldvalue ) wppa_update_option($oldname, $newvalue);
}

// Check if the required directories exist, if not, try to create them and optionally report it
function wppa_check_dirs() {

	// check if wppa dir exists
	$dir = WPPA_UPLOAD_PATH;
	if ( ! is_dir( $dir ) ) {
		mkdir( $dir );
	}
	chmod( $dir, 0755 );

	$subdirs = array( 'thumbs', 'watermarks', 'fonts', 'icons', 'temp', 'dynamic' );

	// Check all subdirs
	foreach( $subdirs as $subdir ) {
		$dir = WPPA_UPLOAD_PATH . '/' . $subdir;
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir );
		}
		chmod( $dir, 0755 );
	}

	// check if depot dir exists
	if ( ! is_multisite() ) {

		// check if master depot dir exists
		$dir = WPPA_CONTENT_PATH . '/wppa-depot';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir );
		}
		chmod( $dir, 0755 );
	}

	// check the plugin activators depot directory
	$dir = WPPA_DEPOT_PATH;
	if ( ! is_dir( $dir ) ) {
		mkdir( $dir );
	}
	chmod( $dir, 0755 );

	return true;
}

// Create grated album(s)
// @1: int album id that may be a grant parent, if so, create child for current user if not already exists
function wppa_grant_albums( $xparent = false ) {
global $wpdb;
static $grant_parents;
static $my_albs_parents;
static $owner;
static $user;

	// Feature enabled?
	if ( ! wppa_switch( 'grant_an_album' ) ) {
		return false;
	}

	// Owners only?
	if ( ! wppa_switch( 'owner_only' ) ) {
		return false;
	}

	// User logged in?
	if ( ! is_user_logged_in() ) {
		return false;
	}

	// Restrict?
	if ( wppa_switch( 'grant_restrict' ) && ! current_user_can( 'wppa_admin' ) ) {
		return false;
	}

	// Can user upload? If restricted need no upload.
	if ( ! wppa_switch( 'grant_restrict' ) && ! current_user_can( 'wppa_upload' ) && ! wppa_switch( 'user_upload_on' ) ) {
		return false;
	}

	// Init
	$albums_created = array();

	// Get required data if not done already
	// First get the grant parent album(s)
	if ( ! is_array( $grant_parents ) ) {
		switch( wppa_opt( 'grant_parent_sel_method' ) ) {

			case 'selectionbox':

				// Album ids are and expanded enumeration sep by , in the setting
				$grant_parents = explode( ',', wppa_opt( 'grant_parent' ) );
				if ( empty( $grant_parents ) ) {
					// Selection box method chosen, but no album(s) selected
					return array();
				}
				else {
					foreach( array_keys( $grant_parents ) as $key ) {
						if ( $grant_parents[$key] == 'zero' ) {
							$grant_parents[$key] = '0';
						}
					}
				}
				break;

			case 'category':

				// The option hold a category
				$grant_parents = $wpdb->get_col( 	"SELECT id " .
													"FROM $wpdb->wppa_albums " .
													"WHERE cats LIKE '%," . wppa_opt( 'grant_parent' ) . ",%'"
												);
				if ( empty( $grant_parents ) ) {
					// Selection set to category, but no albums exist with that category
					return array();
				}
				break;

			case 'indexsearch':
				$temp = $wpdb->get_var( "SELECT albums " .
										"FROM $wpdb->wppa_index " .
										"WHERE slug = '" . wppa_opt( 'grant_parent' ) . "'"
										);

				$grant_parents = explode( '.', wppa_expand_enum( $temp ) );
				if ( empty( $grant_parents ) ) {
					// Selection set to indexsearch but no albums found matching the search criteria
					return array();
				}
				break;

			default:
				wppa_log( 'err', 'Unimplemented grant_parent_sel_method: ' . wppa_opt( 'grant_parent_sel_method' ) . ' in wppa_grant_albums()' );
				break;
		}
	}

	// Retrieve the users login name if not done already
	if ( ! $owner ) {
		$owner = wppa_get_user( 'login' );	// The current users login name
	}

	// Get all the parents of the current user albums if not done already
	if ( ! is_array( $my_albs_parents ) ) {
		$query = $wpdb->prepare( "SELECT DISTINCT a_parent FROM $wpdb->wppa_albums WHERE owner = %s", $owner );
		$my_albs_parents = $wpdb->get_col( $query );
		if ( ! is_array( $my_albs_parents ) ) {
			$my_albs_parents = array();
		}
	}

	// Get the current users name as how the album should be named
	if ( ! $user ) {
		$user = wppa_get_user( wppa_opt( 'grant_name' ) );
	}

	// If a parent is given and it is not a grant parent, quit
	if ( $xparent !== false && ! in_array( $xparent, $grant_parents ) ) {
		return false;
	}

	// If a parent is given, it will now be a grant parent (see directly above), only create the granted album inside this parent.
	if ( $xparent !== false ) {
		$parents = array( $xparent );
	}
	// Else create granted albums for all grant parents
	else {
		$parents = $grant_parents;
	}

	// Parent independant album data
	$name = $user;
	$desc = __( 'Default photo album for', 'wp-photo-album-plus' ) . ' ' . $user;

	// May be multiple granted parents. Check for all parents.
	foreach( $parents as $parent ) {

		// Create only grant album if: parent is either -1 or existing
		if ( $parent == '-1' || $parent == '0' || wppa_album_exists( $parent ) ) {
			if ( ! in_array( $parent, $my_albs_parents, true ) ) {

				// make an album for this user
				$cats = wppa_opt( 'grant_cats' );
				$deftags = wppa_opt( 'grant_tags' );
				$id = wppa_create_album_entry( array ( 'name' => $name, 'description' => $desc, 'a_parent' => $parent, 'cats' => $cats, 'default_tags' => $deftags ) );
				if ( $id ) {
					wppa_log( 'Obs', 'Album ' . wppa_get_album_name( $parent ) . '(' . $parent . ')' .' -> ' . $id . ' for ' . $user . ' created.' );
					$albums_created[] = $id;

					// Add this parent to the array of my albums parents
					$my_albs_parents[] = $parent;
				}
				else {
					wppa_log( 'Err', 'Could not create subalbum of ' . $parent . ' for ' . $user );
				}
				wppa_invalidate_treecounts( $parent );
				wppa_index_add( 'album', $id );

			}
		}
	}

	// Remake permalink redirects
	if ( ! empty( $albums_created ) ) {
		wppa_create_pl_htaccess();
	}

	return $albums_created;

}