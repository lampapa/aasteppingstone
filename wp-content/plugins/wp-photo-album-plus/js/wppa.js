// wppa.js
//
// contains common vars and functions
//

wppaWppaVer = '7.6.05.008';

var wppaIsChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
var wppaIsSafari = false;
var wppaOvlActivePanorama = 0;

// Language dependant vars. i18n for js does not work. Values are filled in by wppa_add_page_specific_urls_and_language() in wppa-non-admin.php
var wppaSlideShow;
var wppaPhoto;
var wppaOf;
var wppaNextPhoto;
var wppaPreviousPhoto;
var wppaNextP;
var wppaPrevP;
var wppaAvgRating;
var wppaMyRating;
var wppaAvgRat;
var wppaMyRat;
var wppaDislikeMsg;
var wppaStart;
var wppaStop;
var wppaPleaseName;
var wppaPleaseEmail;
var wppaPleaseComment;
var wppaProcessing;
var wppaDone;
var wppaUploadFailed;
var wppaServerError;

// Important notice:
// All external vars that may be given a value in wppa-non-admin.php must be declared here and not in other front-end js files!!
// This will prevent overwriting with default values in case defereed js is activated
var wppaVersion = '0';
var wppaIsIe = false;
var wppaDebug;
var wppaFullValign = [];
var wppaFullHalign = [];
var wppaFullFrameDelta = [];
var wppaAnimationSpeed;
var wppaImageDirectory;
if ( ! wppaAutoColumnWidth ) var wppaAutoColumnWidth = [];
if ( ! wppaAutoColumnFrac ) var wppaAutoColumnFrac = [];
var wppaThumbnailAreaDelta;
var wppaSlideShowTimeOut = 2500;
var wppaFadeInAfterFadeOut = false;
var wppaTextFrameDelta = 0;
var wppaBoxDelta = 0;
var wppaPreambule = [];
var wppaHideWhenEmpty = false;
var wppaThumbnailPitch = [];
var wppaFilmStripLength = [];
var wppaFilmStripMargin = [];
var wppaFilmStripAreaDelta = [];
var wppaFilmShowGlue = false;
var wppaIsMini = [];
var wppaPortraitOnly = [];
var wppaMiniTreshold = 300;

var wppaRatingOnce = true;
var wppaBGcolorNumbar = 'transparent';
var wppaBcolorNumbar = 'transparent';
var wppaBGcolorNumbarActive = 'transparent';
var wppaBcolorNumbarActive = 'transparent';
var wppaFontFamilyNumbar = '';
var wppaFontSizeNumbar = '';
var wppaFontColorNumbar = '';
var wppaFontWeightNumbar = '';
var wppaFontFamilyNumbarActive = '';
var wppaFontSizeNumbarActive = '';
var wppaFontColorNumbarActive = '';
var wppaFontWeightNumbarActive = '';
var wppaNumbarMax = '10';
var wppaAjaxUrl = '';
var wppaLang = '';
var wppaNextOnCallback = false;
var wppaStarOpacity = 0.2;
var wppaLightBox = [];
var wppaEmailRequired = 'required';
var wppaSlideBorderWidth = 0;
var wppaSlideInitRunning = [];
var wppaAnimationType = 'fadeover';
var wppaSlidePause = [];
var wppaSlideBlank = [];
var wppaRatingMax = 5;
var wppaRatingDisplayType = 'graphic';
var wppaRatingPrec = 2;
var wppaFilmPageSize = [];
var wppaAspectRatio = [];
var wppaFullSize = [];
var wppaStretch = false;
var wppaThumbSpaceAuto = false;
var wppaMinThumbSpace = 4;
var wppaMagnifierCursor = '';
var wppaArtMonkyLink = 'none';
var wppaAutoOpenComments = false;
var wppaUpdateAddressLine = false;
var wppaFilmThumbTitle = '';
var wppaClickToView = '';
var wppaUploadUrl = '';
var wppaVoteForMe = '';
var wppaVotedForMe = '';
var wppaSlideSwipe = true;
var wppaLightboxSingle = [];
var wppaMaxCoverWidth = 300;	// For responsive multicolumn covers
var wppaDownLoad = 'Download';
var wppaSiteUrl = '';
var wppaWppaUrl = '';
var wppaIncludeUrl = '';
var wppaSlideToFullpopup = false;
var wppaComAltSize = 75;
var wppaBumpViewCount = true;
var wppaBumpClickCount = false;
var wppaFotomoto = false;
var wppaArtMonkeyButton = true;
var wppaShortQargs = false;
var wppaOvlHires = false;
var wppaMasonryCols = [];
var wppaVideoPlaying = [];
var wppaAudioPlaying = [];
var wppaSlideVideoStart = false;
var wppaSlideAudioStart = false;
var wppaAudioHeight = 28;
var wppaHis = 0;
var wppaStartHtml = [];
var wppaCanAjaxRender = false;					// Assume failure
var wppaCanPushState = false;
var wppaAllowAjax = true;						// Assume we are allowed to use ajax
var wppaMaxOccur = 0;
var wppaFirstOccur = 0;
var wppaUsePhotoNamesInUrls = false;
var wppaShareHideWhenRunning = false;
var wppaCommentRequiredAfterVote = true;
var wppaTopMoc = 0;								// Set by wppa_functions.php -> function wppa_container('open' );
if ( ! wppaColWidth ) var wppaColWidth = [];	// [mocc] Set by wppa_functions.php -> function wppa_container('open' );
if ( ! wppaMCRWidth ) var wppaMCRWidth = [];
var wppaFotomotoHideWhenRunning = false;		// Set by wppa-non-admin.php -> wppa_create_wppa_init_js();
var wppaFotomotoMinWidth = 400;					// Set by wppa-non-admin.php -> wppa_create_wppa_init_js();
var wppaPhotoView = [];							// [id] Set to true by a bump viewcount to prevent duplicate bumps.
var wppaBackgroundColorImage = '';
var wppaPopupLinkType = '';
var wppaPopupOnclick = [];
var wppaThumbTargetBlank = false;
var wppaRel = 'rel';
var wppaEditPhotoWidth = '960';
var wppaThemeStyles = '';
var wppaStickyHeaderHeight = 0;
var wppaRenderModal = false;
var wppaModalBgColor = '#ffffff';
var wppaBoxRadius = 0;
var wppaModalQuitImg;
var wppaUploadEdit = 'none';
var wppaPageArg = '';
var wppaSlideshowNavigationType = 'icons';
var wppaCoverImageResponsive = [];
var wppaSearchBoxSelItems = [];
var wppaSlideWrap = [];
var wppaHideRightClick = false;
var wppaGeoZoom = 10;
var wppaLazyLoad = true;
var wppaAreaMaxFrac = 1.0;
var wppaNiceScroll = false;
var wppaIconSizeNormal = 'default';
var wppaIconSizeSlide = 48;
var wppaIconSizeStars = 24;
var wppaResponseSpeed = 500;
var wppaExtendedResizeCount = 0;
var wppaExtendedResizeDelay = 200;
var wppaThumbAspect = 3/4;
var wppaFilmonlyContinuous = false;
var wppaNoAnimateOnMobile = false;
var wppaAjaxScroll = true;
var wppaFilmInit = [];
var wppaResizeEndDelay = 200;
var wppaScrollEndDelay = 200;
var wppaArtmonkeyFileNotSource = false;
var wppaRequestInfoDialogText = "Please specify your question";
var wppaGlobalFsIconSize = 32;
var wppaFsFillcolor = '#999999';
var wppaFsBgcolor = 'transparent';

// 'Internal' variables ( private )
var _wppaId = [];
var _wppaRealId = [];
var _wppaAvg = [];
var _wppaDisc = [];
var _wppaMyr = [];
var _wppaVRU = [];							// Vote Return Url
var _wppaLinkUrl = [];
var _wppaLinkTitle = [];
var _wppaLinkTarget = [];
var _wppaCommentHtml = [];
var _wppaIptcHtml = [];
var _wppaExifHtml = [];
var _wppaToTheSame = false;
var _wppaSlides = [];
var _wppaNames = [];
var _wppaFullNames = [];
var _wppaDsc = [];
var _wppaOgDsc = [];
var _wppaCurIdx = [];
var _wppaNxtIdx = [];
var _wppaTimeOut = [];
var _wppaSSRuns = [];
var _wppaFg = [];
var _wppaTP = [];
var _wppaIsBusy = [];
var _wppaFirst = [];
var _wppaVoteInProgress = false;
var _wppaTextDelay;
var _wppaUrl = [];
var _wppaSkipRated = [];
var _wppaLbTitle = [];
var _wppaStateCount = 0;
var _wppaDidGoto = [];
var _wppaShareUrl = [];
var _wppaShareHtml = [];
var _wppaFilmNoMove = [];
var _wppaHiresUrl = [];
var _wppaIsVideo = [];
var _wppaVideoHtml = [];
var _wppaAudioHtml = [];
var _wppaVideoNatWidth = [];
var	_wppaVideoNatHeight = [];
var _wppaWaitTexts = [];
var _wppaImageAlt = [];
var _wppaLastIdx = [];
var _wppaLazyDone = [];
var _wppaStopping = [];
var _wppaFilename = [];

var __wppaOverruleRun = false;

var wppaOvlUrls;
var wppaOvlIds;
var wppaOvlTitles;
var wppaOvlAlts;
var wppaOvlIdx = 0;
var wppaOvlFirst = true;
var wppaOvlKbHandler = '';
var wppaOvlSizeHandler = '';
var wppaOvlPadTop = 5;
var wppaOvlIsSingle;
var wppaOvlRunning = false;
var wppaOvlVideoHtmls;
var wppaOvlAudioHtmls;
var wppaOvlPdfHtmls;
var wppaOvlVideoNaturalWidths;
var wppaOvlVideoNaturalHeights;
var wppaOvlVideoPlaying = false;
var wppaOvlAudioPlaying = false;
var wppaOvlShowLegenda = true;
var wppaOvlShowStartStop = true;
var wppaOvlRadius = 0;
var wppaOvlBorderWidth = 16;
var wppaOvlOpen = false;
var wppaOvlClosing 			= false;
var wppaThumbSize = 100;
var wppaTfMargin = 4;
var wppaZoomData = [];
var wppaSphericData = [];
var wppaFsPolicy = 'lightbox';

// The next lightbox var values become overwritten in wppa-non-admin.php -> wppa_load_footer()
// Therefor they are placed here!
var wppaOvlTxtHeight = 36;	// 12 * ( n lines of text including the n/m line )
var wppaOvlOpacity = 0.8;
var wppaOvlOnclickType = 'none';
var wppaOvlTheme = 'black';
var wppaOvlAnimSpeed = 300;
var wppaOvlSlideSpeed = 3000;
var wppaVer4WindowWidth = 800;
var wppaVer4WindowHeight = 600;
var wppaOvlFontFamily = 'Helvetica';
var wppaOvlFontSize = '10';
var wppaOvlFontColor = '';
var wppaOvlFontWeight = 'bold';
var wppaOvlLineHeight = '12';
var wppaOvlShowCounter = true;
var wppaOvlIsVideo = false;
var wppaShowLegenda = '';
var wppaOvlFsPhotoId = 0;
var wppaPhotoId = 0;
var wppaOvlVideoStart = false;
var wppaOvlAudioStart = false;
var wppaLastIptc = '';
var wppaLastExif = '';
var wppaIsMobile = false;
var wppaIsIpad = false;
var wppaSvgFillcolor = 'gray';
var wppaSvgBgcolor = 'transparent';
var wppaSvgCornerStyle = 'light';
var wppaCoverSpacing = 8;

// Start debug area
/*
jQuery(document).ready(function() {
	wppaDebugFunc();
});

function wppaDebugFunc() {
	jQuery(window).on("resize",function(){alert('Window resized');});
};
*/
// End debug area

// Tabby click function
function wppaTabbyClick(){
	jQuery(window).trigger("resize");
	jQuery(document).trigger("tabbychange");
	jQuery(window).trigger("orientationchange");
	for (mocc = 1; mocc <= wppaTopMoc; mocc++) {
		_wppaAdjustFilmstrip(mocc);
	}
};

// Init at dom ready
jQuery(document).ready(function() {
	wppaDoInit();
});

// General initialisation
function wppaDoInit() {

	// Misc. init
	_wppaTextDelay = wppaAnimationSpeed;
	if ( wppaFadeInAfterFadeOut ) {
		_wppaTextDelay *= 2;
	}
	if ( wppaIsMobile && wppaNoAnimateOnMobile ) {
		_wppaTextDelay = 10;
	}

	// Make sure ajax spinners dies
	jQuery( '.wppa-ajax-spin' ).stop().fadeOut();

	// Make sure ovl spinner dies
	jQuery( '.wppa-ovl-spin' ).hide();

	// Fade ubbs out
	setTimeout( function() {
		var i = 1;
		while( i < wppaTopMoc ) {
			if ( jQuery( '#ubb-'+i+'-l') ) {
				wppaUbb(i,'l','hide');
				wppaUbb(i,'r','hide');
			}
			i++;
		}
	}, 3000 );

	// Responsive handlers
	jQuery(window).on("DOMContentLoaded load resize wppascrollend orientationchange",wppaDoAllAutocols);

	// Size scrollable areas
	jQuery(window).on('DOMContentLoaded load resize scroll wheel orientationchange',wppaSizeArea);

	// Make Lazy load images visible
	jQuery(window).on('DOMContentLoaded load resize wppascrollend orientationchange', wppaMakeLazyVisible);
	jQuery('.wppa-divnicewrap').on('DOMContentLoaded load resize wppascrollend wheelend orientationchange', wppaMakeLazyVisible);

	// Init masonryplus
	jQuery(window).on('DOMContentLoaded load resize wppascrollend orientationchange', wppaInitMasonryPlus);

	// When the window size changes, filmstrip needs immediate adjustment
	jQuery(window).on('resize', function(){
		var m = 1;
		while ( m <= wppaTopMoc ) {
			wppaFilmInit[m] = false; 					// mark as not initiated
			jQuery( '#wppa-filmstrip-'+m ).stop(); 		// stop running animations
			_wppaAdjustFilmstrip( m ); 					// re-adjust by jumping
			m++;
		}
	});

	// When resize is done, filmstrips are initilized
	jQuery(window).on('wpparesizeend', function(){
		var m = 1;
		while ( m <= wppaTopMoc ) {
			wppaFilmInit[m] = true; 					// mark as not initiated
			m++;
		}
	});

	// Resize nicescrollers, re-layout masonryplus
	jQuery(window).on("DOMContentLoaded load resize wppascrollend orientationchange", function(){

		setTimeout( function() {

			jQuery(".wppa-thumb-area").each(function(){
				if (jQuery(this).getNiceScroll) {
					jQuery(this).getNiceScroll().resize();
				}
			});

			jQuery(".albumlist").each(function(){
				if (jQuery(this).getNiceScroll) {
					jQuery(this).getNiceScroll().resize();
				}
			});

			jQuery(".wppa-div").each(function(){
				if (jQuery(this).getNiceScroll) {
					jQuery(this).getNiceScroll().resize();
				}
			});

			if (jQuery("body").getNiceScroll) {
				jQuery("body").getNiceScroll().resize();
			}

		}, 2000);
    });

	// Fake a resize
	jQuery(window).trigger('resize');

	// Protect rightclick
	wppaProtect();

	// For tabby plugin:
	setTimeout(function(){
		jQuery(".responsive-tabs__heading").on("click", wppaTabbyClick);
        jQuery(".responsive-tabs__list__item").on("click", wppaTabbyClick);
	},10);

	jQuery(document).on("tabbychange",function(){
		if (jQuery("div").getNiceScroll) {
			jQuery("div").getNiceScroll().resize();
		}
		setTimeout(function(){
			jQuery(window).trigger("resize");
			jQuery("#wppa-ovl-spin").hide();
		},500);
	});
}

// resize end listener
var wppaResizeEndTimer;
jQuery(document).ready(function(){
	jQuery(window).on( 'resize load', function () {

//		wppaConsoleLog('Resize triggered');

		clearTimeout( wppaResizeEndTimer );
		wppaResizeEndTimer = setTimeout( function () {
			jQuery(window).trigger('wpparesizeend');
//			wppaConsoleLog('triggering wpparesizeend');
		}, wppaResizeEndDelay );
	});
});

// scroll end listener
var wppaScrollEndTimer;
jQuery(document).ready(function(){
	jQuery(window).on( 'scroll wheel touchmove', function () {
		clearTimeout( wppaScrollEndTimer );
		wppaScrollEndTimer = setTimeout( function () {
			jQuery(window).trigger('wppascrollend');
		}, wppaScrollEndDelay );
	});
});

// Install auto div sizer
jQuery(document).ready(function(){
	jQuery(window).on("DOMContentLoaded load resize scroll wheel orientationchange", wppaSizeAutoDiv);
});

// Size auto wppa_div
function wppaSizeAutoDiv() {
	jQuery('.wppa-autodiv').each(function(index) {
		var totHeight = jQuery(window).height();
		var factor = jQuery(this).attr('data-max-height');
		jQuery(this).css({maxHeight:totHeight*factor});
	});
}

// Do the auto cols
function wppaDoAllAutocols(e) {

	// Report where it comes from
	setTimeout(function(){_wppaDoAllAutocols(-1)}, wppaExtendedResizeDelay);
}

function _wppaDoAllAutocols(i) {

	// Do occurrences that are responsive
	var mocc = 1;
	while ( mocc <= wppaAutoColumnWidth.length ) {
		if ( wppaAutoColumnWidth[mocc] ) {
			_wppaDoAutocol( mocc, i);
		}

		// Do lazy, just to be sure, for Ale
		wppaMakeLazyVisible(mocc);
		mocc++;
	}

	// Do retries if configured (-1 is infinite)
	if ( i < wppaExtendedResizeCount || wppaExtendedResizeCount == -1 ) {
		setTimeout(function(){_wppaDoAllAutocols(i+1)}, wppaExtendedResizeDelay);
	}

	return true;
}

// If disable right mouseclick
function wppaProtect() {
	if ( wppaHideRightClick ) {
		jQuery('img' ).bind('contextmenu', function(e) {
			return false;
		});
		jQuery('video' ).bind('contextmenu', function(e) {
			return false;
		});
	}
}

// Initialize Ajax render partial page content with history update
jQuery( document ).ready( function( e ) {

	// Are we allowed and capable to ajax?
	if ( wppaAllowAjax && jQuery.ajax ) {
		wppaCanAjaxRender = true;
	}

	// Can we do history.pushState ?
	if ( typeof( history.pushState ) != 'undefined') {

		// Save entire initial page content ( I do not know which container is going to be modified first )
		var i=1;
		while ( i <= wppaMaxOccur ) {
			wppaStartHtml[i] = jQuery( '#wppa-container-'+i ).html();
			i++;
		}

		wppaCanPushState = true;
	}
});

// On the fly init lightbox
function wppaUpdateLightboxes() {

	// Native wppa lightbox
	if ( typeof( wppaInitOverlay ) == 'function') {
		wppaInitOverlay();
	}

	// Lightbox-3
	if ( typeof( myLightbox ) != 'undefined') {
		if ( typeof( myLightbox.updateImageList ) == 'function') {
			myLightbox.updateImageList();
		}
	}

	// PrettyPhoto
	if ( jQuery().prettyPhoto ) {
		jQuery( "a[rel^='prettyPhoto']" ).prettyPhoto( {
			deeplinking: false,
		});
	}
}

// Stop video of a given occurrency
function wppaStopVideo( mocc ) {
	var id = [];
	var video;
	var i;

	id[1] = 'wppa-overlay-img';
	id[2] = 'theimg0-'+mocc;
	id[3] = 'theimg1-'+mocc;
	i = 0;

	while ( i < 3 ) {
		i++;
		if ( i != 1 || mocc == 0 ) {	// Stop video on lighbox only when mocc = 0, i.e. stop all video
			video = document.getElementById( id[i] );
			if ( video ) {
				if ( typeof( video.pause ) == 'function') {
					video.pause();
				}
			}
		}
	}
}

// Stop audio
function wppaStopAudio( mocc ) {

	// This mocc only?
	if ( typeof( mocc ) == 'number') {
		if ( jQuery( '#audio-' + mocc ).pause ) {
			jQuery( '#audio-' + mocc ).pause();
		}
	}

	// All audio
	else {

		var items = jQuery('audio' );

		if ( items.length > 0 ) {
			var i = 0;
			while ( i < items.length ) {
				if ( jQuery( items[i] ).attr('data-from') == 'wppa') {
					items[i].pause();
				}
				i++;
			}
		}
	}
}

// Convert a thumbnail url to a fs url
function wppaMakeFullsizeUrl( url ) {
var temp;
var temp2;

	url = url.replace( '/thumbs/', '/' );	// Not a thumb
	// Remove sizespec for Cloudinary
	temp = url.split( '//' );
	if ( temp[1] ) {
		temp2 = temp[1].split( '/' );
		url = temp[0]+'//';
	}
	else {
		temp2 = temp[0].split( '/' );
		url = '';
	}
	var j = 0;
	while ( j < temp2.length ) {
		var chunk = temp2[j];
		var w = chunk.split('_' );
		if ( w[0] != 'w') {
			if ( j != 0 ) url += '/';
			url += chunk;
		}
		j++;
	}
	return url;
}

// Get the width of a container
function wppaGetContainerWidth( mocc ) {
	var elm = document.getElementById('wppa-container-'+mocc );
	var w = 0;

	if ( ! wppaAutoColumnWidth[mocc] ) return elm.clientWidth;

	while ( w == 0 ) {
		elm = elm.parentNode;
		w = jQuery( elm ).width();
	}

	return parseInt( w * wppaAutoColumnFrac[mocc] );
}

// Do the responsive size adjustment
function _wppaDoAutocol( mocc, i ) {

	// Auto?
	if ( ! wppaAutoColumnWidth[mocc] ) {
		return true;
	}

	var w;
	var h;
	var old;
	var exists;

	// Container
	w = jQuery('#wppa-container-'+mocc).width();

	// Anything to do here?
	var container = document.getElementById( 'wppa-container-' + mocc );
	if ( ! container ) {
		return;
	}

	// Covers
	if ( ! wppaCoverImageResponsive[mocc] ) {
		exists = jQuery( ".wppa-asym-text-frame-"+mocc );
		if ( exists.length > 1 ) {
			old = jQuery( exists[0] ).width();

			if ( wppaResponseSpeed == 0 ) {
				jQuery( ".wppa-asym-text-frame-"+mocc ).css( {width:(w - wppaTextFrameDelta)} );
				jQuery( ".wppa-cover-box-"+mocc ).css( {width:w} );
			}
			else {
				jQuery( ".wppa-asym-text-frame-"+mocc ).stop().animate( {width:(w - wppaTextFrameDelta)}, wppaResponseSpeed );
				jQuery( ".wppa-cover-box-"+mocc ).stop().animate( {width:w}, wppaResponseSpeed );
			}
		}
	}

	// Multi Column Responsive covers
	exists = jQuery( ".wppa-cover-box-mcr-"+mocc );
	if ( exists.length > 1 ) {	// Yes there are

		var cw 			= document.getElementById('wppa-albumlist-' + mocc ).clientWidth;
		var nCovers 	= parseInt( ( cw + wppaCoverSpacing )/( wppaMaxCoverWidth+wppaCoverSpacing ) ) + 1;
		var coverMax1 	= nCovers - 1;
		var MCRWidth 	= parseInt( ( ( cw + wppaCoverSpacing )/nCovers ) - wppaCoverSpacing );

		if ( wppaColWidth[mocc] != cw || wppaMCRWidth[mocc] != MCRWidth ) {

			wppaColWidth[mocc] = cw;
			wppaMCRWidth[mocc] = MCRWidth;

			var idx = 0;
			while ( idx < exists.length ) {
				var col = idx % nCovers;
				switch ( col ) {
					case 0:	/* left */
						jQuery( exists[idx] ).css( {'marginLeft': '0px', 'clear': 'both', 'float': 'left'});
						break;
					case coverMax1:	/* right */
						jQuery( exists[idx] ).css( {'marginLeft': '0px', 'clear': 'none', 'float': 'right'});
						break;
					default:
						jQuery( exists[idx] ).css( {'marginLeft': wppaCoverSpacing, 'clear': 'none', 'float': 'left'});
				}
				idx++;
			}

			if ( wppaCoverImageResponsive[mocc] ) {
			}
			else {
				jQuery( ".wppa-asym-text-frame-mcr-"+mocc ).stop().animate( {width: (MCRWidth - wppaTextFrameDelta)}, wppaResponseSpeed );
			}
			old = jQuery( exists[0] ).width();
			jQuery( ".wppa-cover-box-mcr-"+mocc ).stop().animate( {width:MCRWidth}, wppaResponseSpeed );
		}
	}
	else if ( exists.length == 1 ) {	// One cover: full width, 0 covers don't care
		if ( wppaCoverImageResponsive[mocc] ) {
		}
		else {
			jQuery( ".wppa-asym-text-frame-mcr-"+mocc ).stop().animate( {width:(w - wppaTextFrameDelta)}, wppaResponseSpeed );
			var myCss = {
				'marginLeft': '0px',
				'float'		: 'left'
			}
			jQuery( ".wppa-cover-box-mcr-"+mocc ).css( myCss );
		}
	}

	// Grid covers. set container linheight to 0
	var isGrid = jQuery( '.wppa-album-cover-grid-'+mocc ).length;

	if ( isGrid > 0 ) {

		// Set container linheight to 0
		jQuery('#wppa-container-'+mocc).css( 'line-height', '0' );

		// Calculate width
		var nItems = parseInt( ( w / wppaMaxCoverWidth ) + 0.9999 );
		if ( nItems < 1 ) nItems = 1;

		// Set widths
		jQuery( '.wppa-album-cover-grid-'+mocc ).css( {width:(100/nItems)+'%'} );
	}

	// Thumbframes default
	if ( wppaThumbSpaceAuto ) {
		var tfw = parseInt( jQuery( ".thumbnail-frame-"+mocc ).css('width') );
		if ( tfw ) {
			var minspc = wppaMinThumbSpace;
			var weff = w - wppaThumbnailAreaDelta - 7;
			var nthumbs = Math.max( 1, parseInt( weff / ( tfw + minspc ) ) );
			var availsp = weff - nthumbs * tfw;
//			var newspc = parseInt( 0.5 + availsp / ( nthumbs+1 ) );
// This should fix blinking layout
			var newspc = parseInt( availsp / ( nthumbs+1 ) );

			jQuery( ".thumbnail-frame-"+mocc ).css( {marginLeft:newspc});
		}
	}

	// Comalt thumbmails
	jQuery( ".thumbnail-frame-comalt-"+mocc ).css('width', w - wppaThumbnailAreaDelta );
	jQuery( ".wppa-com-alt-"+mocc ).css('width', w - wppaThumbnailAreaDelta - wppaComAltSize - 16 );

	// Masonry thumbnails horizontal
	var row = 1;
	var rowHeightPix;
	var rowHeightPerc = jQuery( '#wppa-mas-h-'+row+'-'+mocc ).attr('data-height-perc' );
	while ( rowHeightPerc ) {
		rowHeightPix = rowHeightPerc * ( w - wppaThumbnailAreaDelta ) / 100;
		jQuery( '#wppa-mas-h-'+row+'-'+mocc ).css('height', rowHeightPix );
		row++;
		rowHeightPerc = jQuery( '#wppa-mas-h-'+row+'-'+mocc ).attr('data-height-perc' );
	}

	// Fix bug in ie and chrome
	wppaSetMasHorFrameWidthsForIeAndChrome(mocc);

	// Slide
	if ( document.getElementById( 'slide_frame-'+mocc ) ) {
		wppaFormatSlide( mocc );
	}

	// Audio
	jQuery( "#audio-slide-"+mocc ).css('width', w - wppaBoxDelta - 6 );

	// Comments
	jQuery( ".wppa-comment-textarea-"+mocc ).css('width',w * 0.7 );

	// Filmstrip
	wppaFilmStripLength[mocc] = w - wppaFilmStripAreaDelta[mocc];
	jQuery( "#filmwindow-"+mocc ).css('width',wppaFilmStripLength[mocc] );
	_wppaAdjustFilmstrip( mocc );	// reposition content

	// Texts in slideshow and browsebar
	if ( ! wppaIsMini[mocc] && typeof( _wppaSlides[mocc] ) != 'undefined') {	// Mini is properly initialized
		if ( wppaColWidth[mocc] < wppaMiniTreshold ) {
			jQuery( '#wppa-avg-rat-'+mocc ).html( wppaAvgRat );
			jQuery( '#wppa-my-rat-'+mocc ).html( wppaMyRat );

			jQuery( '#counter-'+mocc ).html( ( _wppaCurIdx[mocc]+1 )+' / '+_wppaSlides[mocc].length );
		}
		else {
			jQuery( '#wppa-avg-rat-'+mocc ).html( wppaAvgRating );
			jQuery( '#wppa-my-rat-'+mocc ).html( wppaMyRating );

			jQuery( '#counter-'+mocc ).html( wppaPhoto+' '+( _wppaCurIdx[mocc]+1 )+' '+wppaOf+' '+_wppaSlides[mocc].length );
		}
	}

	// Single photo
	jQuery( ".wppa-sphoto-"+mocc ).css('width',w );
	jQuery( ".wppa-simg-"+mocc ).css('width',w - 2*wppaSlideBorderWidth );
	jQuery( ".wppa-simg-"+mocc ).css('height', '' );

	// Mphoto
	jQuery( ".wppa-mphoto-"+mocc ).css('width',w + 10 );
	jQuery( ".wppa-mimg-"+mocc ).css('width',w );
	jQuery( ".wppa-mimg-"+mocc ).css('height', '' );

	// smxpdf
	jQuery( ".smxpdf-"+mocc ).css('height', 0.8 * wppaWindowHeight() );

	// Search box
	if ( wppaSearchBoxSelItems[mocc] > 0 ) {
		if ( w / wppaSearchBoxSelItems[mocc] < 125 ) {
			jQuery( ".wppa-searchsel-item-"+mocc ).css('width', '100%' );
		}
		else {
			jQuery( ".wppa-searchsel-item-"+mocc ).css('width', ( 100 / wppaSearchBoxSelItems[mocc] ) + '%' );
		}
	}

	// Upload dialog album selectionbox
	jQuery( ".wppa-upload-album-"+mocc ).css('maxWidth', 0.6 * w );

	// Real calendar
	wppaSetRealCalendarHeights( mocc );

	// wppa_div
//	jQuery('.wppa-divnicewrap').trigger('transitioned');

	return true;
}

// Set heights in real calendar
function wppaSetRealCalendarHeights( mocc ) {

	var w = jQuery('#wppa-real-calendar-'+mocc).width();

	if ( w > 0 ) {

		var ready = true;

		var h = w*wppaThumbAspect/7;
		jQuery('.wppa-real-calendar-day-'+mocc).css({height:h});

		var f = (w/50+2);
		jQuery('#wppa-real-calendar-'+mocc).css({fontSize:f});

		var m = f/4;
		jQuery('.wppa-real-calendar-head-td-'+mocc).css({marginTop:m,marginBottom:m});

		var b = h/2;
		jQuery('.wppa-realcalimg-'+mocc).each(function(){
			if ( this.height == 0 ) {
				ready = false;
			}
			else {
				var day = jQuery(this).attr('data-day');
				thisb = b - (h-this.height)/2;
				jQuery('.wppa-real-calendar-day-content-'+day+'-'+mocc).css({bottom:thisb});
			}
		});

		if ( ! ready ) {
			setTimeout(function(){wppaSetRealCalendarHeights( mocc );},100);
		}
	}
}

// Fix bug in IE and Chrome
function wppaSetMasHorFrameWidthsForIeAndChrome(mocc) {
	// For IE and Chrome there is the class .wppa-mas-h-{mocc}
	// Set widths of frames for IE and Chrome. These browsers interprete width:auto
	// sometimes not in relation to the specified height, but to the available space.
	var frames = jQuery( '.wppa-mas-h-'+mocc );
	var tnm = wppaMinThumbSpace;
	for ( var i=0;i<frames.length;i++ ) {

		var img = wppaGetChildI( frames[i] );
		if ( img ) {
			if ( img.nodeName == 'IMG') {
				if ( ! img.complete ) {
					setTimeout('wppaSetMasHorFrameWidthsForIeAndChrome( '+mocc+' )', 400 ); // Try again after 400 ms.
					return;
				}
			}
			var wd = ( ( img.naturalWidth ) / ( img.naturalHeight ) * ( img.height ) ) + tnm;
			jQuery( frames[i] ).css( {'width': wd } );
		}
	}
}
// get (grand)child of parent with id like i-...
function wppaGetChildI( parent ) {

	var children = parent.childNodes;
	var img = false;
	var i;

	for ( i=0; i<children.length; i++ ) {
		var child = children[i];
		if ( child.id ) {
			if ( child.id.substr( 0, 2 ) == 'i-' ) {
				return child;
			}
		}
		var grandChild = wppaGetChildI( child );
		if ( grandChild ) {
			return grandChild;
		}
	}
	return false;
}

// Fotomoto
var wppaFotomotoLoaded = false;
var wppaFotomotoToolbarIds = [];
function fotomoto_loaded() {
	wppaFotomotoLoaded = true;
}
function wppaFotomotoToolbar( mocc, url ) {
	if ( wppaColWidth[mocc] >= wppaFotomotoMinWidth ) {	// Space enough to show the toolbar
		jQuery( '#wppa-fotomoto-container-'+mocc ).css('display','inline' );
		jQuery( '#wppa-fotomoto-checkout-'+mocc ).css('display','inline' );
	}
	else {
		jQuery( '#wppa-fotomoto-container-'+mocc ).css('display','none' );
		jQuery( '#wppa-fotomoto-checkout-'+mocc ).css('display','none' );
		return;	// Too small
	}
	if ( wppaFotomoto && document.getElementById('wppa-fotomoto-container-'+mocc ) ) { // Configured and container present
		if ( wppaFotomotoLoaded ) {
			FOTOMOTO.API.checkinImage( url );
			wppaFotomotoToolbarIds[mocc] = FOTOMOTO.API.showToolbar('wppa-fotomoto-container-'+mocc, url );
		}
		else { // Not loaded yet, retry after 200 ms
			setTimeout('wppaFotomotoToolbar( '+mocc+',"'+url+'" )', 200 );
		}
	}
}
function wppaFotomotoHide( mocc ) {
	jQuery( '#wppa-fotomoto-container-'+mocc ).css('display','none' );
	jQuery( '#wppa-fotomoto-checkout-'+mocc ).css('display','none' );
}

// Sanitize utility
function wppaStringContainsForbiddenChars( str ) {
var forbidden = [ '?', '&', '#', '/', '"', "'" ];
var i=0;

	while ( i < forbidden.length ) {
		if ( str.indexOf( forbidden[i] ) != -1 ) {
			return true;
		}
		i++;
	}
	return false;
}

// Setup an event handler for popstate events
window.onpopstate = function( event ) {
	var occ = 0;
	if ( wppaCanPushState ) {
		if ( event.state ) {
			occ = event.state.occur;
			switch ( event.state.type ) {
				case 'html':

					// Restore wppa container content
					jQuery( '#wppa-container-'+occ ).html( event.state.html );
					break;
				case 'slide':

					// Go to specified slide without the didgoto switch to avoid a stackpush here
					_wppaGoto( occ, event.state.slide );
					break;
			}
		}
		else if ( wppaUpdateAddressLine ) {

			occ = wppaFirstOccur;

			// Restore first modified occurrences content
			jQuery( '#wppa-container-'+occ ).html( wppaStartHtml[occ] );

			// Now we are back to the initial page
			wppaFirstOccur = 0;

			// If a photo number given goto that photo
			if ( occ == 0 ) {	// Find current occur if not yet known
				var url = document.location.href;
				var urls = url.split( "&wppa-occur=" );
				occ = parseInt( urls[1] );
			}
			var url = document.location.href;
			var urls = url.split( "&wppa-photo=" );
			var photo = parseInt( urls[1] );
			if ( photo > 0 ) {
				var idx = 0;
				while ( idx < _wppaId[occ].length ) {
					if ( _wppaId[occ][idx] == photo ) break;
					idx++;
				}
				if ( idx < _wppaId[occ].length ) _wppaGoto( occ, idx );
			}
		}
		// If it is a slideshow, stop it
		if ( document.getElementById('theslide0-'+occ ) ) {
			_wppaStop( occ );
		}
	}
	if ( typeof( wppaQRUpdate ) != 'undefined') wppaQRUpdate( document.location.href );
};

// Push history stack for a slideshow
function wppaPushStateSlide( mocc, slide, url ) {

	if ( ! wppaIsMini[mocc] ) {	// Not from a widget
		if ( wppaCanPushState && wppaUpdateAddressLine ) {
			if ( url != '' ) {
				try {
					history.pushState( {page: wppaHis, occur: mocc, type: 'slide', slide: slide}, "---", url );
				}
				catch( err ) {
					wppaConsoleLog( 'Slide history stack update failed' );
				}
			}
		}
	}
}

// Filter enables the use of <script> tags inside a script
function wppaRepairScriptTags( text ) {
var temp;
var newtext;

	// Just to be sure we do not run into undefined error
	if ( typeof( text ) == 'undefined') return '';

	while ( text.indexOf( '[script') != -1 ) {
		text = text.replace( '[script', '<script' );
	}
	while ( text.indexOf( '[/script') != -1 ) {
		text = text.replace( '[/script', '</script' );
	}

	return text;

}

// Filter enables the use of a <br> tag while they are removed with strip_tags
// Also fixes [a ] and [/a], and [img  and /]
function wppaRepairBrTags( text ) {
var newtext;

	// Just to be sure we do not run into undefined error
	if ( typeof(text) == 'undefined') return '';

	newtext = text;
	newtext = newtext.replace( '[br /]', '<br />' );
	newtext = newtext.replace( '[a', '<a' );
	newtext = newtext.replace( /&quot;/g, '"' );
	newtext = newtext.replace( '"]', '">' );
	newtext = newtext.replace( '[/a]', '</a>' );
	newtext = newtext.replace( '[img', '<img' );
	newtext = newtext.replace( '/]', '/>' );

	return newtext;
}

// Replace text that is too long by ellipses
function wppaTrimAlt( text ) {
var newtext;

	// Just to be sure we do not run into undefined error
	if ( typeof(text) == 'undefined') return '';

	if ( text.length > 13 ) {
		newtext = text.substr( 0,10 ) + '...';
	}
	else newtext = text;
	return newtext;
}

// Initialize FaceBook sdk
var wppaFbInitBusy = false;
function wppaFbInit() {
	if ( ! wppaFbInitBusy ) {
		if ( typeof( FB ) != 'undefined') {
			wppaFbInitBusy = true;				// set busy
			setTimeout('_wppaFbInit()', 10 ); 	// do it async over 10 ms
		}
		else {
			setTimeout('wppaFbInit()', 200 );
		}
	}
}

function _wppaFbInit() {
	FB.init( {status : true, xfbml : true } );
	wppaFbInitBusy = false;
}

// Insert ( an emoticon ) in a comment text
function wppaInsertAtCursor( elm, value ) {

    //IE support
    if ( document.selection ) {
        elm.focus();
        sel = document.selection.createRange();
        sel.text = value;
    }

    //MOZILLA and others
    else if ( elm.selectionStart || elm.selectionStart == '0') {
        var startPos = elm.selectionStart;
        var endPos = elm.selectionEnd;
        elm.value = elm.value.substring( 0, startPos )
            + value
            + elm.value.substring( endPos, elm.value.length );
        elm.selectionStart = startPos + value.length;
        elm.selectionEnd = startPos + value.length;
    } else {
        elm.value += value;
    }
}

// Initialize Google Maps
function wppaGeoInit( mocc, lat, lon ) {
	var myLatLng = new google.maps.LatLng( lat, lon );
	var mapOptions = {
		disableDefaultUI: false,
		panControl: false,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: true,
		streetViewControl: true,
		overviewMapControl: true,
		zoom: wppaGeoZoom,
		center: myLatLng,
//			mapTypeId: google.maps.MapTypeId.TERRAIN,
//			mapTypeControlOptions: {
//				mapTypeIds: [ google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID ],
//				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
//			},
	};
	var map = new google.maps.Map( document.getElementById( "map-canvas-"+mocc ), mapOptions );
	var marker = new google.maps.Marker( {
		position: myLatLng,
		map: map,
		title:""
	});

	google.maps.event.addListener( map, "center_changed", function() {
		// 1 second after the center of the map has changed, pan back to the
		// marker.
		window.setTimeout(function() {
		  map.panTo( marker.getPosition() );
		}, 1000 );
	});
}

// Encode funny chars
function wppaEncode( xtext ) {
	var text, result;

	if ( typeof( xtext )=='undefined') return;

	text = String(xtext);
	result = text.replace( /#/g, '||HASH||' );
	text = result;
	result = text.replace( /&/g, '||AMP||' );
	text = result;
//	result = text.replace( /+/g, '||PLUS||' );
	var temp = text.split( '+' );
	var idx = 0;
	result = '';
	while ( idx < temp.length ) {
		result += temp[idx];
		idx++;
		if ( idx < temp.length ) result += '||PLUS||';
	}

	return result;
}

// Compute photo id out of an url
function wppaUrlToId( url ) {
	var temp = url.split( '/wppa/' );		// if '/wppa/' found, a wppa image
	if ( temp.length == 1 ) {
		temp = url.split( '/upload/' );	// if '/upload/' found, a cloudinary image
	}
	if ( temp.length == 1 ) {
		return 0;	// Still nothing, not a wppa image or ahires image, return 0
	}
	// Find image id
	temp = temp[1];
	temp = temp.split( '.' );
	temp = temp[0].replace( '/', '' );
	temp = temp.replace( '/', '' );
	temp = temp.replace( '/', '' );
	temp = temp.replace( '/', '' );
	temp = temp.replace( '/', '' );

	return temp;
}

// Opens/closes selection boxes in supersearch html
function wppaSuperSearchSelect( mocc, go ) {

	// Init
	jQuery( '#wppa-ss-albumopt-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-albumcat-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-albumname-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-albumtext-'+mocc ).css('display', 'none');
	jQuery( '#wppa-ss-photoopt-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-photoname-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-photoowner-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-phototag-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-phototext-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-photoexif-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-photoiptc-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-exifopts-'+mocc ).css('display', 'none');
		jQuery( '#wppa-ss-iptcopts-'+mocc ).css('display', 'none');

	jQuery( '#wppa-ss-spinner-'+mocc ).css('display', 'none');
	jQuery( '#wppa-ss-button-'+mocc ).css('display', 'none');

	var s1 = jQuery( '#wppa-ss-pa-'+mocc ).val();
	var s2 = '';
	var s3 = '';
	var data = '';
	switch ( s1 ) {
		case 'a':
			jQuery( '#wppa-ss-albumopt-'+mocc ).css('display', '');
			s2 = jQuery( '#wppa-ss-albumopt-'+mocc ).val();
			switch ( s2 ) {
				case 'c':
					jQuery( '#wppa-ss-albumcat-'+mocc ).css('display', '');
					var set = jQuery( '.wppa-ss-albumcat-'+mocc );
					data = '';
					var i;
					for ( i = 0; i < set.length; i++ ) {
						if ( jQuery( set[i] ).prop('selected')) {
							data += '.' + jQuery( set[i] ).val();
						}
					}
					data = data.substr( 1 );

					if ( data != '' ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 'n':
					jQuery( '#wppa-ss-albumname-'+mocc ).css('display', '');
					data = jQuery( '#wppa-ss-albumname-'+mocc ).val();
					if ( data != null ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 't':
					jQuery( '#wppa-ss-albumtext-'+mocc ).css('display', '');
					var set = jQuery( '.wppa-ss-albumtext-'+mocc );
					data = '';
					var i;
					for ( i = 0; i < set.length; i++ ) {
						if ( jQuery( set[i] ).prop('selected')) {
							data += '.' + jQuery( set[i] ).val();
						}
					}
					data = data.substr( 1 );

					if ( data != '' ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
			}
			break;

		case 'p':
			jQuery( '#wppa-ss-photoopt-'+mocc ).css('display', '');
			s2 = jQuery( '#wppa-ss-photoopt-'+mocc ).val();
			switch ( s2 ) {
				case 'n':
					jQuery( '#wppa-ss-photoname-'+mocc ).css('display', '');
					data = jQuery( '#wppa-ss-photoname-'+mocc ).val();
					if ( data != null ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 'o':
					jQuery( '#wppa-ss-photoowner-'+mocc ).css('display', '');
					data = jQuery( '#wppa-ss-photoowner-'+mocc ).val();
					if ( data != null ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 'g':
					jQuery( '#wppa-ss-phototag-'+mocc ).css('display', '');
					var set = jQuery( '.wppa-ss-phototag-'+mocc );
					data = '';
					var i;
					for ( i=0; i<set.length; i++ ) {
						if ( jQuery( set[i] ).prop('selected')) {
							data += '.' + jQuery( set[i] ).val();
						}
					}
					data = data.substr( 1 );

					if ( data != '' ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 't':
					jQuery( '#wppa-ss-phototext-'+mocc ).css('display', '');
					var set = jQuery( '.wppa-ss-phototext-'+mocc );
					data = '';
					var i;
					for ( i=0; i<set.length; i++ ) {
						if ( jQuery( set[i] ).prop('selected')) {
							data += '.' + jQuery( set[i] ).val();
						}
					}
					data = data.substr( 1 );

					if ( data != '' ) {
						jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
					}
					break;
				case 'i':
					jQuery( '#wppa-ss-photoiptc-'+mocc ).css('display', '');
					s3 = jQuery( '#wppa-ss-photoiptc-'+mocc ).val();
					if ( s3 ) {
						if ( s3.length > 2 ) {
							s3 = s3.replace( '#', 'H' );	// Replace # by H
						}
						if ( s3 != '' ) {
							jQuery( '#wppa-ss-iptcopts-'+mocc ).css('display', '');
							if ( wppaLastIptc != s3 ) {
								wppaAjaxGetSsIptcList( mocc, s3, 'wppa-ss-iptcopts-'+mocc );
								wppaLastIptc = s3;
							}
							else {
								data = jQuery( '#wppa-ss-iptcopts-'+mocc ).val();
								if ( data != null && data != '' ) {
									jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
								}
							}
						}
					}
					break;
				case 'e':
					jQuery( '#wppa-ss-photoexif-'+mocc ).css('display', '');
					s3 = jQuery( '#wppa-ss-photoexif-'+mocc ).val();
					if ( s3 ) {
						if ( s3.length > 2 ) {
							s3 = s3.replace( '#', 'H' );	// Replace # by H
						}
						if ( s3 != '' ) {
							jQuery( '#wppa-ss-exifopts-'+mocc ).css('display', '');
							if ( wppaLastExif != s3 ) {
								wppaAjaxGetSsExifList( mocc, s3, 'wppa-ss-exifopts-'+mocc );
								wppaLastExif = s3;
							}
							else {
								data = jQuery( '#wppa-ss-exifopts-'+mocc ).val();
								if ( data != null && data != '' ) {
									jQuery( '#wppa-ss-button-'+mocc ).css('display', '');
								}
							}
						}
					}
					break;
			}
			break;
	}

	// Find results
	if ( go ) {
		var url = jQuery( '#wppa-ss-pageurl-'+mocc ).val();
		if ( url.indexOf( '?' ) == -1 ) {
			url += '?';
		}
		else {
			url += '&';
		}
		url += 'occur=1&wppa-supersearch='+s1+','+s2+','+s3+','+data;
		document.location.href = url;
	}
}

// Supersearch function get iptc list
// These functions may not be moved to wppa-ajax-front.js because they MUST be loaded in the header!
function wppaAjaxGetSsIptcList( mocc, s3, selid ) {

	jQuery.ajax( { 	url: 		wppaAjaxUrl,
					data: 		'action=wppa' +
								'&wppa-action=getssiptclist' +
								'&tag=' + s3 +
								'&moccur=' + mocc,
					async: 		true,
					type: 		'GET',
					timeout: 	10000,
					beforeSend: function( xhr ) {
									jQuery( '#wppa-ss-spinner-'+mocc ).css('display', '');
								},
					success: 	function( result, status, xhr ) {
									jQuery( '#'+selid ).html( result );
									jQuery( '#wppa-ss-iptcopts-'+mocc ).css('display', '');
									wppaSuperSearchSelect( mocc );
									setTimeout('wppaSetIptcExifSize( ".wppa-iptclist-'+mocc+'", "#'+selid+'" )', 10 );
								},
					error: 		function( xhr, status, error ) {
									wppaConsoleLog('wppaAjaxGetSsIptcList failed. Error = ' + error + ', status = ' + status, 'force' );
								},
					complete: 	function( xhr, status, newurl ) {
									jQuery( '#wppa-ss-spinner-'+mocc ).css('display', 'none');
								}
				} );

}

// Supersearch function get exif list
function wppaAjaxGetSsExifList( mocc, s3, selid ) {

	jQuery.ajax( { 	url: 		wppaAjaxUrl,
					data: 		'action=wppa' +
								'&wppa-action=getssexiflist' +
								'&tag=' + s3 +
								'&moccur=' + mocc,
					async: 		true,
					type: 		'GET',
					timeout: 	10000,
					beforeSend: function( xhr ) {
									jQuery( '#wppa-ss-spinner-'+mocc ).css('display', '');
								},
					success: 	function( result, status, xhr ) {
									jQuery( '#'+selid ).html( result );
									jQuery( '#wppa-ss-exifopts-'+mocc ).css('display', '');
									wppaSuperSearchSelect( mocc );
									setTimeout('wppaSetIptcExifSize( ".wppa-exiflist-'+mocc+'", "#'+selid+'" )', 10 );
								},
					error: 		function( xhr, status, error ) {
									wppaConsoleLog('wppaAjaxGetSsExifList failed. Error = ' + error + ', status = ' + status, 'force' );
								},
					complete: 	function( xhr, status, newurl ) {
									jQuery( '#wppa-ss-spinner-'+mocc ).css('display', 'none');
								}
				} );

}

// Supersearch function set size of exif/iptc itemlist
function wppaSetIptcExifSize( clas, selid ) {
	var t = jQuery( clas );
	var n = t.length;
	if ( n > 6 ) n = 6;
	if ( n < 2 ) n = 2;
	jQuery( selid ).attr('size', n );
}

function wppaUpdateSearchRoot( text, root ) {
	var items = jQuery( ".wppa-search-root" );
	var i = 0;
	while ( i < items.length ) {
		jQuery( items[i] ).html( text );
		i++;
	}
	items = jQuery( ".wppa-rootbox" );
	i = 0;
	while ( i < items.length ) {
		if ( root ) {
			jQuery( items[i] ).prop('checked', false );
			jQuery( items[i] ).prop('disabled', false );
		}
		else {
			jQuery( items[i] ).prop('checked', true );
			jQuery( items[i] ).prop('disabled', true );
		}
		i++;
	}
	items = jQuery( ".wppa-search-root-id" );
	i = 0;
	while ( i < items.length ) {
		jQuery( items[i] ).val( root );
		i++;
	}
}

function wppaSubboxChange( elm ) {
	if ( jQuery( elm ).prop('checked') ) {
		jQuery( ".wppa-rootbox" ).each(function(index) {
			jQuery(this).prop('checked',true);
		});

		/*
		var items = jQuery( ".wppa-rootbox" );
		var i = 0;
		while ( i < items.length ) {
			jQuery( items[i] ).prop('checked', true );
			i++;
		}
		*/
	}
}

function wppaClearSubsearch() {
	var items = jQuery( ".wppa-display-searchstring" );
	var i = 0;
	while ( i < items.length ) {
		jQuery( items[i] ).html( '' );
		i++;
	}
	items = jQuery( ".wppa-search-sub-box" );
	i = 0;
	while ( i < items.length ) {
		jQuery( items[i] ).prop('disabled', true );
		i++;
	}
}

function wppaEnableSubsearch() {
	var items = jQuery( ".wppa-search-sub-box" );
	var i = 0;
	while ( i < items.length ) {
		jQuery( items[i] ).removeAttr('disabled' );
		i++;
	}
}

function wppaDisplaySelectedFiles(id) {
	var theFiles = jQuery('#'+id);
	var i = 0;
	var result = '';

	while ( i<theFiles[0].files.length ) {
		result += theFiles[0].files[i].name+' ';
		i++;
	}

	jQuery('#'+id+'-display').val(result);
}

function wppaIsEmpty( str ) {
	if ( str == null ) return true;
	if ( typeof( str ) == 'undefined') return true;
	if ( str == '' ) return true;
	if ( str == false ) return true;
	if ( str == 0 ) return true;
}

function wppaGetUploadOptions( yalb, mocc, where, onComplete ) {

	var options = {
		beforeSend: function() {
			jQuery('#progress-'+yalb+'-'+mocc).show();
			jQuery('#bar-'+yalb+'-'+mocc).width('0%');
			jQuery('#message-'+yalb+'-'+mocc).html('');
			jQuery('#percent-'+yalb+'-'+mocc).html('');
		},
		uploadProgress: function(event, position, total, percentComplete) {
			jQuery('#bar-'+yalb+'-'+mocc).css('backgroundColor','#7F7');
			jQuery('#bar-'+yalb+'-'+mocc).width(percentComplete+'%');
			if ( percentComplete < 95 ) {
				jQuery('#percent-'+yalb+'-'+mocc).html(percentComplete+'%');
			}
			else {
				jQuery('#percent-'+yalb+'-'+mocc).html(wppaProcessing);
			}
		},
		success: function() {
			jQuery('#bar-'+yalb+'-'+mocc).width('100%');
			jQuery('#percent-'+yalb+'-'+mocc).html(wppaDone);
			jQuery('.wppa-upload-button').val(wppaUploadButtonText);
		},
		complete: function(response) {
			if (response.responseText.indexOf(wppaUploadFailed)!=-1) {
				jQuery('#bar-'+yalb+'-'+mocc).css('backgroundColor','#F77');
				jQuery('#percent-'+yalb+'-'+mocc).html(wppaUploadFailed);
				jQuery('#message-'+yalb+'-'+mocc).html( '<span style="font-size: 10px;" >'+response.responseText+'</span>' );
			}
			else {
				jQuery('#message-'+yalb+'-'+mocc).html( '<span style="font-size: 10px;" >'+response.responseText+'</span>' );
				if ( where == 'thumb' || where == 'cover') {
					eval(onComplete);
				}
			}
		},
		error: function() {
			jQuery('#message-'+yalb+'-'+mocc).html( '<span style="color: red;" >'+wppaServerError+'</span>' );
			jQuery('#bar-'+yalb+'-'+mocc).css('backgroundColor','#F77');
			jQuery('#percent-'+yalb+'-'+mocc).html(wppaUploadFailed);
		}
	};

	return options;
}

function wppaInitMasonryPlus( mocc ) {

	var fm, to, m;
	if ( parseInt( mocc ) > 0 ) {
		fm = mocc;
		to = mocc;
	}
	else {
		fm = 1;
		to = wppaTopMoc;
	}
	m = fm;

	while ( m <= to ) {

		if ( document.getElementById( 'grid-' + m ) ) {

			var w = jQuery( '#wppa-container-' + m ).width() - wppaThumbnailAreaDelta;
			var cnt = parseInt( ( w + wppaTfMargin ) / ( wppaThumbSize * 0.75 + wppaTfMargin ) );
			var colWidth = w / cnt - wppaTfMargin;

			jQuery(".grid-item").css('visibility', 'visible');
			jQuery(".grid-item-" + m).css('width', colWidth + 'px');
			jQuery('#grid-' + m ).masonry({
					itemSelector: '.grid-item-' + m,
					columnWidth: colWidth,
					gutter: wppaTfMargin,
					fitWidth: true
				});
		}
		m++;
	}
}

/* Start fullscreen functions */
// Initialize
jQuery(document).ready(function(){

	// Create global buttons if required
	if ( wppaFsPolicy == 'global' ) {
		wppaGlobalFS();
	}

	// Install handlers
	jQuery(window).on("DOMContentLoaded load", wppaFsShow);
	jQuery(document).on("fullscreenchange mozfullscreenchange webkitfullscreenchange msfullscreenchange", wppaFsChange);

});

// Handle fullscreen change event
function wppaFsChange() {

	// Show the right buttons
	wppaFsShow();

	// Re-display lightbox
	wppaOvlShowSame();
}

// Create global fullsize buttons
function wppaGlobalFS() {

	if ( wppaIsIpad ) return false;
	if ( wppaIsSafari ) return false;

	var top = parseInt( wppaGlobalFsIconSize / 4 );
	var rgt = top;
	if ( ! wppaIsMobile && jQuery( '#wpadminbar' ).length > 0 ) {
		top += jQuery( '#wpadminbar' ).height();
	}

	jQuery('body').append(
		'<div' +
			' id="wppa-fulls-btn-1"' +
			' class="wppa-fulls-btn"' +
			' style="position:fixed;top:' + top + 'px;right:' + rgt + 'px;display:none;"' +
			' title="Enter fullscreen"' +
			' onclick="wppaFsOn()"' +
			' >' +
			wppaSvgHtml( 'Full-Screen', wppaGlobalFsIconSize + 'px', true, false, '0', '0', '0', '0' ) +
		'</div>');

	jQuery('body').append(
		'<div' +
			' id="wppa-exit-fulls-btn-1"' +
			' class="wppa-exit-fulls-btn"' +
			' style="position:fixed;top:' + top + 'px;right:' + rgt + 'px;display:none;"' +
			' title="Leave fullscreen"' +
			' onclick="wppaFsOff()"' +
			' >' +
			wppaSvgHtml( 'Exit-Full-Screen', wppaGlobalFsIconSize + 'px', true, false, '0', '0', '0', '0' ) +
		'</div>');

	wppaFsShow();

}

// Switch fullscreen on
function wppaFsOn() {

	var docElm = document.documentElement;
	if (docElm.requestFullscreen) {
		docElm.requestFullscreen();
	}
	else if (docElm.mozRequestFullScreen) {
		docElm.mozRequestFullScreen();
	}
	else if (docElm.webkitRequestFullScreen) {
		docElm.webkitRequestFullScreen();
	}

}

// Switch fullscreen off
function wppaFsOff() {

	if (document.exitFullscreen) {
		document.exitFullscreen();
	}
	else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	}
	else if (document.webkitCancelFullScreen) {
		document.webkitCancelFullScreen();
	}

}

// Is fullscreen on?
function wppaIsFs() {

	if ( wppaIsIpad ) return false;
	if ( wppaIsSafari ) return false;
	return ( document.fullscreenElement !== null );
}

// Show / hide the right buttons
function wppaFsShow() {

	if ( wppaIsFs() ) {
		jQuery( '.wppa-fulls-btn' ).hide();
		jQuery( '.wppa-exit-fulls-btn' ).show();
	}
	else {
		jQuery( '.wppa-fulls-btn' ).show();
		jQuery( '.wppa-exit-fulls-btn' ).hide();
	}

}
