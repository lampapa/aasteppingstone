<?php
/* wppa-admin.php
* Package: wp-photo-album-plus
*
* Contains the admin menu and startups the admin pages
* Version 7.5.10
*
*/

if ( ! defined( 'ABSPATH' ) ) die( "Can't load this file directly (2)" );

/* CHECK INSTALLATION */
// Check setup
add_action ( 'init', 'wppa_setup', '8' );	// admin_init

/* ADMIN MENU */
add_action( 'admin_menu', 'wppa_add_admin' );

function wppa_add_admin() {
	global $wp_roles;
	global $wpdb;

	// Make sure admin has access rights
	if ( wppa_user_is( 'administrator' ) ) {
		$wp_roles->add_cap( 'administrator', 'wppa_admin' );
		$wp_roles->add_cap( 'administrator', 'wppa_upload' );
		$wp_roles->add_cap( 'administrator', 'wppa_import' );
		$wp_roles->add_cap( 'administrator', 'wppa_moderate' );
		$wp_roles->add_cap( 'administrator', 'wppa_export' );
		$wp_roles->add_cap( 'administrator', 'wppa_settings' );
		$wp_roles->add_cap( 'administrator', 'wppa_potd' );
		$wp_roles->add_cap( 'administrator', 'wppa_comments' );
		$wp_roles->add_cap( 'administrator', 'wppa_help' );
	}

	// See if there are comments pending moderation
	$com_pending = '';
	$com_pending_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->wppa_comments WHERE status = 'pending' OR status = 'spam' OR status = ''" );
	if ( $com_pending_count ) $com_pending = '<span class="update-plugins"><span class="plugin-count">'.$com_pending_count.'</span></span>';

	// See if there are uploads pending moderation
	$upl_pending = '';
	$upl_pending_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->wppa_photos WHERE status = 'pending' AND album > 0" );
	if ( $upl_pending_count ) $upl_pending = '<span class="update-plugins"><span class="plugin-count">'.$upl_pending_count.'</span></span>';

	// Compute total pending moderation
	$tot_pending = '';
	$tot_pending_count = '0';
	if ( current_user_can('wppa_comments') || current_user_can('wppa_moderate') ) $tot_pending_count += $com_pending_count;
	if ( current_user_can('wppa_admin') || current_user_can('wppa_moderate') ) $tot_pending_count+= $upl_pending_count;
	if ( $tot_pending_count ) $tot_pending = '<span class="update-plugins"><span class="plugin-count">'.'<b>'.$tot_pending_count.'</b>'.'</span></span>';

	$icon_url = WPPA_URL . '/img/camera16.png';

	// 				page_title        menu_title                                      capability    menu_slug          function      icon_url    position
	add_menu_page( 'WP Photo Album', __('Photo&thinsp;Albums', 'wp-photo-album-plus').$tot_pending, 'wppa_admin', 'wppa_admin_menu', 'wppa_admin', $icon_url ); //,'10' );

	//                 parent_slug        page_title                             			menu_title                             					capability            menu_slug               function
	add_submenu_page( 'wppa_admin_menu',  __('Album Admin', 'wp-photo-album-plus'),			 __('Album Admin', 'wp-photo-album-plus').$upl_pending,'wppa_admin',        	'wppa_admin_menu',      'wppa_admin' );
    add_submenu_page( 'wppa_admin_menu',  __('Upload Photos', 'wp-photo-album-plus'),           __('Upload Photos', 'wp-photo-album-plus'),          'wppa_upload',        	'wppa_upload_photos',   'wppa_page_upload' );
	// Uploader without album admin rights, but when the upload_edit switch set, may edit his own photos
	if ( ! current_user_can('wppa_admin') && wppa_opt( 'upload_edit') != '-none-' ) {
		add_submenu_page( 'wppa_admin_menu',  __('Edit Photos', 'wp-photo-album-plus'), 		 __('Edit Photos', 'wp-photo-album-plus'), 		   'wppa_upload', 		 	'wppa_edit_photo', 	 'wppa_edit_photo' );
	}
	add_submenu_page( 'wppa_admin_menu',  __('Import Photos', 'wp-photo-album-plus'),           __('Import Photos', 'wp-photo-album-plus'),          'wppa_import',        'wppa_import_photos',   'wppa_page_import' );
	add_submenu_page( 'wppa_admin_menu',  __('Moderate Photos', 'wp-photo-album-plus'),		 __('Moderate Photos', 'wp-photo-album-plus').(wppa_switch('moderate_bulk')?$upl_pending:$tot_pending), 'wppa_moderate', 	 'wppa_moderate_photos', 'wppa_page_moderate' );
	add_submenu_page( 'wppa_admin_menu',  __('Export Photos', 'wp-photo-album-plus'),           __('Export Photos', 'wp-photo-album-plus'),          'wppa_export',     	'wppa_export_photos',   'wppa_page_export' );
    add_submenu_page( 'wppa_admin_menu',  __('Settings', 'wp-photo-album-plus'),                __('Settings', 'wp-photo-album-plus'),               'wppa_settings',      	'wppa_options',         'wppa_page_options' );
	add_submenu_page( 'wppa_admin_menu',  __('Photo of the day Widget', 'wp-photo-album-plus'), __('Photo of the day', 'wp-photo-album-plus'),       'wppa_potd', 		 	'wppa_photo_of_the_day', 'wppa_sidebar_page_options' );
	add_submenu_page( 'wppa_admin_menu',  __('Manage comments', 'wp-photo-album-plus'),         __('Comments', 'wp-photo-album-plus').$com_pending,  'wppa_comments',      	'wppa_manage_comments', 'wppa_comment_admin' );
    add_submenu_page( 'wppa_admin_menu',  __('Help &amp; Info', 'wp-photo-album-plus'),         __('Documentation', 'wp-photo-album-plus'),        	'wppa_help',          	'wppa_help',            'wppa_page_help' );
	if ( wppa_get_option( 'wppa_logfile_on_menu' ) == 'yes' ) {
		add_submenu_page( 'wppa_admin_menu',  __('Logfile', 'wp-photo-album-plus'), 			__('Logfile', 'wp-photo-album-plus'), 				'administrator',  	 	'wppa_log', 			'wppa_log_page' );
	}
	add_submenu_page( 'wppa_admin_menu', __('Clear cache', 'wp-photo-album-plus'), 				__('Clear cache', 'wp-photo-album-plus'), 			'wppa_admin',			'wppa_clear_cache', 	'wppa_clear_wppa_cache' );
}

/* ADMIN STYLES */
add_action( 'admin_init', 'wppa_admin_styles' );

function wppa_admin_styles() {
global $wppa_api_version;
	wp_register_style( 'wppa_admin_style', WPPA_URL.'/wppa-admin-styles.css', '', $wppa_api_version );
	wp_enqueue_style( 'wppa_admin_style' );

	if ( wppa_can_magick() ) {
		wp_register_style( 'wppa_cropper_style', WPPA_URL.'/vendor/cropperjs/dist/cropper.min.css' );
		wp_enqueue_style( 'wppa_cropper_style' );
	}

	// Standard frontend styles
	wp_register_style('wppa_style', WPPA_URL.'/theme/wppa-style.css', array(), $wppa_api_version);
	wp_enqueue_style('wppa_style');

}

/* ADMIN SCRIPTS */
add_action( 'admin_init', 'wppa_admin_scripts' );

function wppa_admin_scripts() {
global $wppa_api_version;
	wp_register_script( 'wppa_upload_script', WPPA_URL.'/js/wppa-multifile-compressed.js', '', $wppa_api_version );
	wp_enqueue_script( 'wppa_upload_script' );
	if ( is_file( WPPA_PATH.'/js/wppa-admin-scripts.min.js' ) ) {
		wp_register_script( 'wppa_admin_script', WPPA_URL.'/js/wppa-admin-scripts.min.js', '', $wppa_api_version );
	}
	else {
		wp_register_script( 'wppa_admin_script', WPPA_URL.'/js/wppa-admin-scripts.js', '', $wppa_api_version );
	}
	wp_enqueue_script( 'wppa_admin_script' );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui-sortable' );
	wp_enqueue_script( 'jquery-ui-dialog' );
	wp_enqueue_script( 'jquery-form' );
	wp_enqueue_script( 'jquery-masonry' );
 	wp_enqueue_script( 'wppa-utils', WPPA_URL . '/js/wppa-utils.js', array(), $wppa_api_version );
 	wp_enqueue_script( 'wppa', WPPA_URL . '/js/wppa.js', array(), $wppa_api_version );
	wp_enqueue_script( 'wppa-slideshow', WPPA_URL . '/js/wppa-slideshow.js', array(), $wppa_api_version );
 	wp_enqueue_script( 'wppa-ajax-front', WPPA_URL . '/js/wppa-ajax-front.js', array(), $wppa_api_version );
	wp_enqueue_script( 'wppa-zoom', WPPA_URL . '/js/wppa-zoom.js', array(), $wppa_api_version );
	wp_enqueue_script( 'wppa-spheric', WPPA_URL . '/js/wppa-spheric.js', array(), $wppa_api_version );
	wp_enqueue_script( 'wppa-three', WPPA_URL . '/vendor/three/three.min.js', array(), $wppa_api_version );
	if ( wppa_can_magick() ) {
		wp_enqueue_script( 'cropperjs', WPPA_URL . '/vendor/cropperjs/dist/cropper.min.js', array(), $wppa_api_version );
	}
}

/* ADMIN PAGE PHP's */

// Album admin page
function wppa_admin() {
	wppa_grant_albums();
	require_once 'wppa-album-admin-autosave.php';
	require_once 'wppa-photo-admin-autosave.php';
	require_once 'wppa-album-covers.php';
	wppa_publish_scheduled();
	_wppa_admin();
}
// Upload admin page
function wppa_page_upload() {
	if ( wppa_is_user_blacklisted() ) wp_die(__( 'Uploading is temporary disabled for you' , 'wp-photo-album-plus') );
	wppa_grant_albums();
	require_once 'wppa-upload.php';
	_wppa_page_upload();
}
// Edit photo(s)
function wppa_edit_photo() {
	if ( wppa_is_user_blacklisted() ) wp_die(__( 'Editing is temporary disabled for you' , 'wp-photo-album-plus') );
	require_once 'wppa-photo-admin-autosave.php';
	wppa_publish_scheduled();
	_wppa_edit_photo();
}
// Import admin page
function wppa_page_import() {

	// check the user depot directory
	$dir = WPPA_DEPOT_PATH;
	if ( ! wppa_is_dir( $dir ) ) {
		wppa_mktree( $dir );
	}
	wppa_chmod( $dir, true );

	if ( wppa_is_user_blacklisted() ) wp_die(__( 'Importing is temporary disabled for you' , 'wp-photo-album-plus') );
	wppa_grant_albums();
	wppa_rename_files_sanitized( WPPA_DEPOT_PATH );
	require_once 'wppa-import.php';
	echo '<script type="text/javascript" >/* <![CDATA[ */wppa_import = "'.__('Import', 'wp-photo-album-plus').'"; wppa_update = "'.__('Update', 'wp-photo-album-plus').'";/* ]]> */</script>';
	_wppa_page_import();
}
// Moderate admin page
function wppa_page_moderate() {
	require_once 'wppa-photo-admin-autosave.php';
	wppa_publish_scheduled();
	_wppa_moderate_photos();
}
// Export admin page
function wppa_page_export() {
	require_once 'wppa-export.php';
	_wppa_page_export();
}
// Settings admin page
function wppa_page_options() {
	require_once 'wppa-settings-autosave.php';

	// jQuery Easing for Nicescroller
	$easing_url = WPPA_URL . '/vendor/jquery-easing/jquery.easing.min.js';
	wp_enqueue_script( 'nicescrollr-easing-min-js', $easing_url, array( 'jquery' ), 'all', true );

	// Nicescroll Library
	$nice_url = WPPA_URL . '/vendor/nicescroll/jquery.nicescroll.min.js';
	wp_enqueue_script( 'nicescrollr-inc-nicescroll-min-js', $nice_url, array( 'jquery', 'nicescrollr-easing-min-js' ), 'all', true );

	_wppa_page_options();
}
// Photo of the day admin page
function wppa_sidebar_page_options() {
	require_once 'wppa-potd-admin.php';
	wppa_publish_scheduled();
	_wppa_sidebar_page_options();
}
// Comments admin page
function wppa_comment_admin() {
	require_once 'wppa-comment-admin.php';
	_wppa_comment_admin();
}
// Help admin page
function wppa_page_help() {
	require_once 'wppa-help.php';
	_wppa_page_help();
}
// Clear cache
function wppa_clear_wppa_cache() {

	if ( ! current_user_can( 'administrator' ) && ! current_user_can( 'edit_posts' ) ) {
		wp_die( 'You have no rights to do this' );
	}

	echo '<h1>' .	__( 'Clear WPPA Cache', 'wp-photo-album-plus' ) . '</h1>';

	$sc_files = array();
	$root = WPPA_CONTENT_PATH . '/' . wppa_opt( 'cache_root' ) . '/wppa-shortcode';
	if ( wppa_is_dir( $root ) ) {
		$sc_files = wppa_glob( $root . '/*' );
	}

	$wg_files = array();
	$root = WPPA_CONTENT_PATH . '/' . wppa_opt( 'cache_root' ) . '/wppa-widget';
	if ( wppa_is_dir( $root ) ) {
		$wg_files = wppa_glob( $root . '/*' );
	}

	$files = array_merge( $sc_files, $wg_files );
	$count = count( $files );
	if ( $count ) {
		echo '
		<div class="wrap" >
			<table class="wppa-table widefat wppa-setting-table" style="margin-top:12px;width: 800px;" >
				<thead style="font-weight:bold;" >
					<tr>
						<td style="width:600px;" >' . __( 'Name', 'wp-photo-album-plus' ) . '</td>
						<td style="width:75px; text-align:center;" >' . __( 'Size', 'wp-photo-album-plus' ) . '</td>
						<td style="width:100px; text-align:center;" >' . __( 'Age', 'wp-photo-album-plus' ) . '</td>
					</tr>
				</thead>
				<tbody>';
				foreach( $files as $file ) {
					$pfile 	= '...' . str_replace( dirname( dirname( dirname( dirname( $file ) ) ) ), '', $file );
					$size 	= wppa_filesize( $file );
					$a = time() - wppa_filetime( $file );
					$d = floor( $a / ( 24 * 3600 ) ) ;
					$a -= $d * 24 * 3600;
					$h = floor( $a / 3600 );
					$a -= $h * 3600;
					$m = floor( $a / 60 );
					$s = $a - $m * 60;
					$age 	= sprintf( '%2dd %2dh %2dm %2ds', $d, $h, $m, $s );
					if ( isset( $_REQUEST['delete'] ) ) {
						wppa_unlink( $file, false );
					}
					echo '
					<tr>
						<td style="width:600px;" >' . $pfile . '</td>
						<td style="width:75px;" ><span style="float:right;" >' . $size . '</span></td>
						<td style="width:100px;" >' . $age . '</td>
					</tr>';
				}
				echo '
				</tbody>
			</table>';

			if ( isset( $_REQUEST['delete'] ) ) {
				echo '<br><b>' . sprintf( __( '%d cachefiles deleted', 'wp-photo-album-plus' ), $count ) . '</br>';
			}
			else {
				echo '
					<p>' .
					__( 'Caching is \'smart\'. This means that cache files are cleared when the display of a wppa widget or shortcode will change due to adding albums, photos comments or ratings.', 'wp-photo-album-plus' ) . '
					<br />' .
					__( 'You will need to clear the cachefiles only when you change the layout outside the WPPA settings, i.e. change theme or custom CSS.', 'wp-photo-album-plus' ) . '
					</p>

					<input
						type="button"
						class="button-primary"
						onclick="document.location.href=\'' . admin_url( 'admin.php?page=wppa_clear_cache&delete' ) . '\'"
						value="' . __( 'Delete', 'wp-photo-album-plus' ) . '"
					/>';
			}
		echo '
		</div>';
	}
	else {
		echo '
		<div class="wrap" >' .
			__( 'No cachefiles to remove.', 'wp-photo-album-plus' ) . '
		</div>';
	}
}

/* GENERAL ADMIN */

// General purpose admin functions
require_once 'wppa-admin-functions.php';
require_once 'wppa-tinymce-shortcodes.php';
require_once 'wppa-tinymce-photo.php';
require_once 'wppa-privacy-policy.php';

require_once 'wppa-gutenberg-photo.php';
require_once 'wppa-gutenberg-wppa.php';


/* This is for the changelog text when an update is available */
global $pagenow;
if ( 'plugins.php' === $pagenow )
{
    // Changelog update message
    $file   = basename( __FILE__ );
    $folder = basename( dirname( __FILE__ ) );
    $hook = "in_plugin_update_message-{$folder}/{$file}";
    add_action( $hook, 'wppa_update_message_cb', 20, 2 ); // hook for function below
}
function wppa_update_message_cb( $plugin_data, $r )
{
    $output = '<span style="margin-left:10px;color:#FF0000;">Please Read the ' .
		'<a href="http://wppa.opajaap.nl/changelog/" target="_blank" >Changelog</a>' .
		' Details Before Upgrading.</span>';

    return print $output;
}


/* Add "donate" link to main plugins page */
add_filter('plugin_row_meta', 'wppa_donate_link', 10, 2);

/* Check multisite config */
add_action('admin_notices', 'wppa_verify_multisite_config');

/* Check for pending maintenance procs */
add_action('admin_notices', 'wppa_maintenance_messages');

// Check for configuration conflicts
add_action( 'admin_notices', 'wppa_check_config_conflicts' );

// Check if tags system needs conversion
add_action( 'admin_init', 'wppa_check_tag_system' );

// Check if cats system needs conversion
add_action( 'admin_init', 'wppa_check_cat_system' );

require_once 'wppa-dashboard-widgets.php';


// Load panoama js if needed at the backend
if ( wppa_get_option( 'wppa_enable_panorama' ) == 'yes' ) {
	add_action( 'admin_footer', 'wppa_load_panorama_js' );
}
function wppa_load_panorama_js() {
global $wppa_api_version;

	if ( wppa( 'has_panorama' ) ) {
		$three_url = WPPA_URL . '/vendor/three/three.min.js';
		$ver = '122';
		wp_enqueue_script( 'wppa-three-min-js', $three_url, array(), $ver, true );
	}
}

// Register category for Gutenberg
function wppa_block_categories( $categories, $post ) {
    if ( $post->post_type !== 'post' && $post->post_type !== 'page' ) {
        return $categories;
    }
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'wppa-shortcodes',
                'title' => __( 'WPPA shortcodes', 'wp-photo-album-plus' ),
            ),
        )
    );
}
add_filter( 'block_categories', 'wppa_block_categories', 10, 2 );

// Fix Gutenberg bug
function wppa_fix_gutenberg_shortcodes( $post ) {
global $wpdb;

	// Get the post
	$post_content = $wpdb->get_var( $wpdb->prepare( "SELECT post_content FROM $wpdb->posts WHERE ID = %d", $post ) );

	// Fix
	$new_content = str_replace( array( 'wp:wppa/gutenberg-photo', 'wp:wppa/gutenberg-wppa' ), 'wp:shortcode', $post_content );

	// Update if fixed
	if ( $post_content != $new_content ) {
		$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->posts SET post_content = %s WHERE ID = %d", $new_content, $post ) );
	}

	// If wppa content, clear wppa cache
	if ( strpos( $post_content, '[wppa' ) !== false ||
		 strpos( $post_content, '[photo' ) !== false ) {
			 wppa_clear_cache();
	}
}
add_action( 'save_post', 'wppa_fix_gutenberg_shortcodes' );

//add_action( 'save_post', 'wppa_clear_cache' );