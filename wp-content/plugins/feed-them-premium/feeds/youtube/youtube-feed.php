<?php
extract(shortcode_atts(array(
    'username' => '',
    'vid_count' => '1',
    'large_vid' => '',
    'large_vid_description' => 'yes',
    'large_vid_title' => 'yes',
    'vids_in_row' => '4',
    'channel_id' => '',
    'playlist_id' => '',
    'username_subscribe_btn' => '',
    'space_between_videos' => '',
    'force_columns' => 'no',
    'thumbs_wrap_color' => '',
    'thumbs_wrap_height' => '',
    'wrap' => '',
    'wrap_single' => '',
    'maxres_thumbnail_images' => '',
    'popup'  => '',
    // Loadmore Options
    'loadmore' => '',
    'loadmore_count' => '',
    'loadmore_btn_margin' => '',
    'loadmore_btn_maxwidth' => '',
    'video_wrap_display' => '',
    'video_wrap_display_single'  => '',
    'comments_in_popup' => '',
    'comments_sidebar' => '',
    'comments_count' => '0',
    'thumbs_play_in_iframe' => '',
    //for single videos
    'video_id_or_link' => '',
    'comments_visible' => '',
), $atts));


$video_wrap_display = $video_wrap_display_single !== '' ? $video_wrap_display_single : $video_wrap_display;
$wrap = $wrap_single !== '' ? $wrap_single : $wrap;

?>