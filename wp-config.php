<?php
/** Enable W3 Total Cache */
//define( 'WPCACHEHOME', 'C:\xampp\htdocs\aasteppingstone\wp-content\plugins\wp-super-cache/' );
define('WP_CACHE', true); // Added by W3 Total Cache

/** Enable W3 Total Cache */

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
 //define('WP_HOME','http://www.aasteppingstone.com');
 //define('WP_SITEURL','http://www.aasteppingstone.com');
 
 
define('WP_MEMORY_LIMIT', '96M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aasteppingstone');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '{!NT_-t>lQwG+}QbF(ASZ+id>-Yau|UF@x|fA-Tq!(mpl/fOh?Bcs!y%D@q<|L@y]CaugCX^yR|B<g$pNQ-[!jK]YHIGvZdg^[JjnrS*aLTYsW&lSiXd=EyqpbF_}WTG');
define('SECURE_AUTH_KEY', 'x$Um+LMM*oWzt@=Dqr(wtvGbXwmv*OzXtg[nJp)Lg;LtgD[%_aXolJ/A@p!g@Jo]g!F!&^il-OtO@ajJp|Wulr<{ORRZ=J[LQdmW}iOn^G-<)fKWFBN;=@bJrYKf|$XL');
define('LOGGED_IN_KEY', 'zTUH</d!AapKx<x?xCO_J)<WR?Q&>dpj>pT)v@!bK/;pejZ*hvv[P]U]|P(;BLxLZf}]nIda[u[i/>%QH&*XPCj=-zOSVU=ILS+GgIB|qFd>)+V@fBy=*pm/&$Zgs>Gz');
define('NONCE_KEY', 'U!GnAn|?(TIvXlV^{]>mJIMTUHNIwsPVvI;fzx$!;rih-vAt}YYatJzc-GC|bFOZnxQjqwL]WeUSGZZoGPakMauHCO>nXKZ$EbUn&fB>@[e?gnog[ClHk_OJb=ONd{&g');
define('AUTH_SALT', 'GzP/+uwgS$}C[Tv?;uF;lDRDPCnms(ahKuEZ;YL^{Yk>ja%YYh%JA!h<sD[s(^;ABStjG+$M}kx;J<%ljpZNMLAqW[B;poVV|+XY!_wkX?tuvYh(l=hInOezI?qzVXC[');
define('SECURE_AUTH_SALT', 'jlQ[K-Q}A&y+_ba-b|)Vq)q%>_o}timOqSo&|G<QR/uwG/+OsMXn;ANO}?O!jOcCY>bjm-X+T^j*mG)CqfHjAXG^DMd+T+&PRv$sMl_la!EYADa&?UT?ldgFy=D*xjZq');
define('LOGGED_IN_SALT', '@-s^GVgW]SV%|tG(t!PKCZ[sfzmU_WhsViT!WAn?M{[<qmeb|TA!xGKoBU^ZXhEz!-IYH[^qLRr-@/B@d;%nkZ[[g*W(=z<quhCHEI^qCIGmy=C=V]l@H[@A[_IIQYhc');
define('NONCE_SALT', '-[MxpgFiEfBDAp^T|UCCWY$e*jz(ypl;LAU?{xc$|ItqhpWri%m)P/;*L-vf*!Rf_ywaq*wf>JUj/;wBxao@}VoTB%lb|??XVe)HlC$*oD[Ud=UKUZH%aD*AbO<mD^ZG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_wgry_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Include tweaks requested by hosting providers.  You can safely
 * remove either the file or comment out the lines below to get
 * to a vanilla state.
 */
if (file_exists(ABSPATH . 'hosting_provider_filters.php')) {
	include('hosting_provider_filters.php');
}
